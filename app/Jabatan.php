<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
  protected $table = 'jabatan';
  protected $primaryKey = 'jab_id';
  public $timestamps = false;
  protected $fillable = [
      'jab_id','jab_nama','jab_desc','jab_update'
  ];
}
