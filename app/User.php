<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','username','name','nama_belakang','gelar_depan', 'gelar_belakang','tempat_lahir_provinsi','tempat_lahir_kota','tanggal_lahir','alamat','provinsi','kota','kode_pos','handphone','telephone','agama' ,'email', 'photo', 'password',
        'nik', 'department', 'jabatan', 'tanggal_kerja', 'gender',
        'status','hak_akses'
    ];
    /**++++++++++++++++++++++++
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function skill()
    {
       return $this->hasMany(Skill::class);
    }







}
