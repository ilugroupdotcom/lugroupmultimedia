<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pensiun extends Model
{
  protected $table = 'pensiun';
  protected $primaryKey = 'pensiun_id';
  public $timestamps = false;
  protected $fillable = [
      'pensiun_id','pensiun_nama', 'pensiun_created'
  ];
}
