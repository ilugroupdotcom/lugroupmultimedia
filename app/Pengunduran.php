<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengunduran extends Model
{
  protected $table = 'pengunduran_diri';
  protected $primaryKey = 'pd_id';
  public $timestamps = false;
  protected $fillable = [
      'pd_id','pd_nama', 'pd_created'
  ];
}
