<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
  protected $table = 'berita';
  protected $primaryKey = 'be_id';
  public $timestamps = false;
  protected $fillable = [
      'be_id','be_user_id','be_title', 'be_draft', 'be_time_create', 'be_time_update'
  ];

}
