<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikans extends Model
{
  protected $primaryKey = 'rpen_id';
  protected $fillable = [
      'user_id', 'sekolah', 'kota', 'tingkat_pen', 'date_keluar', 'jurusan', 'no_ijazah'
  ];
}
