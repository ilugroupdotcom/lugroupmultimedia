<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenisizin extends Model
{
  protected $table = 'jenis_izin';
  protected $primaryKey = 'ji_id';
  public $timestamps = false;
  protected $fillable = [
      'ji_id','ji_jenis','ji_kategori','ji_kode','ji_created'
  ];
}
