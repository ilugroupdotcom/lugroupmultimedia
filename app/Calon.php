<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calon extends Model
{
  protected $table = 'calon_pegawai';
  protected $primaryKey = 'cp_id';
  public $timestamps = false;
  protected $fillable = [
      'cp_id','cp_fullname','cp_telp',
      'cp_alamat', 'cp_gender', 'cp_filecv',
      'cp_foto', 'cp_skill', 'cp_pengalaman',
      'status', 'cp_penilaian', 'cp_hasil', 'cp_tgl_transfer', 'cp_transfer'
  ];
}
