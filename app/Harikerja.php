<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Harikerja extends Model
{
  protected $table = 'hari_kerja';
  protected $primaryKey = 'hari_id';
  public $timestamps = false;
  protected $fillable = [
      'hari_id','hari_jam_id','hari_nama','hari_jam_masuk', 'hari_jam_akhir_masuk', 'hari_jam_pulang', 'hari_jam_akhir_pulang'
      , 'hari_jam_lembur', 'hari_selesai_lembur', 'hari_datetime'
  ];
}
