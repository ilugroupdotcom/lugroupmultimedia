<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submenu1 extends Model
{
  protected $table = 'submenu1';
  protected $primaryKey = 'sub1_id';
  public $timestamps = false;
  protected $fillable = [
      'sub1_id','sub1_sub_id','sub1_label','sub1_link','sub1_date'
  ];
}
