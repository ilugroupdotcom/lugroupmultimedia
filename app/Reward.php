<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
  protected $table = 'reward';
  protected $primaryKey = 'rew_id';
  public $timestamps = false;
  protected $fillable = [
      'rew_id','rew_user_id','rew_title','rew_nosk', 'rew_tgl_keputusan',
      'rew_nama_pemberi', 'rew_update'
  ];
}
