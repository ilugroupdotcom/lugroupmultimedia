<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Klibur extends Model
{
  protected $table = 'kal_libur';
  protected $primaryKey = 'kl_id';
  public $timestamps = false;
  protected $fillable = [
      'kl_id','kl_nama', 'kl_tgl_start', 'kl_tgl_end', 'kel_created'
  ];
}
