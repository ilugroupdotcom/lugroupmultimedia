<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
  protected $table = 'agama';
  protected $primaryKey = 'agama_id';
  public $timestamps = false;
  protected $fillable = [
      'agama_id','agama_nama','agama_created'
  ];
}
