<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diklat extends Model
{
  protected $table = 'diklat';
  protected $primaryKey = 'diklat_id';
  public $timestamps = false;
  protected $fillable = [
      'diklat_id','diklat_nama','diklat_created'
  ];
}
