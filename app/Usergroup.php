<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usergroup extends Model
{
  protected $table = 'user_group';
  protected $primaryKey = 'ug_id';
  public $timestamps = false;
  protected $fillable = [
      'ug_id','ug_usergroup','ug_label','ug_date'
  ];
}
