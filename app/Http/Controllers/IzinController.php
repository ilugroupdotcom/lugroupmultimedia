<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usergroup;
use App\Menu;
use App\Modul;
use App\Hakakses;
use App\User;
use App\Department;
use App\Jabatan;
use App\Jamkerja;
use App\Harikerja;
use App\Lembur;
use App\Izin;
use App\Jenisizin;
use DB;


class IzinController extends Controller
{

  public function Izin()
  {
    $pegawai = User::All();
    $menu = Menu::All();
    $users = DB::table('users')->paginate(10);
    $department = Department::All();
    $jabatan = Jabatan::All();
    $izin = Izin::join('users','izin.izin_user_id','=','users.id')
              ->join('jenis_izin','izin.izin_jenis_id','=','jenis_izin.ji_id')
              ->orderBy('izin.izin_id','ASC')
              ->paginate(10);

    return view('izin.izin', compact('users', 'department', 'jabatan', 'menu', 'pegawai', 'izin'));
  }

  public function createIzin()
  {
      $users = User::All();
      $pegawai = User::All();
      $menu = Menu::All();
      $jenisizin = Jenisizin::All();
      return view('izin.createizin', compact('users', 'jenisizin', 'menu', 'pegawai'));
  }

  public function insertIzin()
  {
    $date = date('Y-m-d H:i:s');
    DB::table('izin')
          ->insert([
                'izin_user_id' => request('user'),
                'izin_jenis_id' => request('jenis'),
                'izin_date_start' => request('tglawal'),
                'izin_date_end' => request('tglakhir'),
                'izin_keterangan' => request('keterangan', false),
                'izin_personalia_ket' => request('personalia', false),
                'izin_gm_ket' => request('general', false),
                'izin_status' => "Pending",
                'izin_created' => $date
        ]);

    //dd($input);

    return redirect()->back();

  }

  public function editizin($id)
  {
      $menu = Menu::All();
      $izin = Izin::join('users','izin.izin_user_id','=','users.id')
                ->join('jenis_izin','izin.izin_jenis_id','=','jenis_izin.ji_id')
                ->where('izin.izin_id', $id)
                ->get();
      $jenisizin = Jenisizin::All();
      $users = User::All();
      // dd($izin);
      return view('izin.updateizin', compact('izin','menu','jenisizin','users'));
  }

  public function updateizin($id)
  {
      $date = date('Y-m-d H:i:s');
      $izin = DB::table('izin')->where('izin_id', $id);
      $izin->update([
        'izin_id' => request('id'),
        'izin_user_id' => request('user'),
        'izin_jenis_id' => request('jenis'),
        'izin_date_start' => request('tglawal'),
        'izin_date_end' => request('tglakhir'),
        'izin_keterangan' => request('keterangan', false),
        'izin_personalia_ket' => request('personalia', false),
        'izin_gm_ket' => request('general', false),
        'izin_status' => request('status', false),
        'izin_created' => $date
      ]);

      return redirect()->route('izin');
  }

  public function detailizin($id)
  {
      $menu = Menu::All();
      $izin = Izin::join('users','izin.izin_user_id','=','users.id')
                ->join('jenis_izin','izin.izin_jenis_id','=','jenis_izin.ji_id')
                ->where('izin.izin_id', $id)
                ->get();
      $jenisizin = Jenisizin::All();
      $users = User::All();
      // dd($izin);
      return view('izin.detailizin', compact('izin','menu','jenisizin','users'));
  }

  public function destroyIzin($id)
  {
      $jizin = Jenisizin::find($id);
      $jizin->delete();
      return redirect()->route('izin');
  }

  public function jenisIzin()
  {
      $pegawai = User::All();
      $menu = Menu::All();
      $users = DB::table('users')->paginate(10);
      $department = Department::All();
      $jabatan = Jabatan::All();
      $jenisizin = Jenisizin::paginate(10);

      return view('izin.jenisizin', compact('users', 'department', 'jabatan', 'menu', 'pegawai', 'jenisizin'));
  }
  public function createjenisIzin()
  {
      $pegawai = User::All();
      $menu = Menu::All();
      $users = DB::table('users')->paginate(10);
      $department = Department::All();
      $jabatan = Jabatan::All();
      $jenisizin = Jenisizin::paginate(10);

      return view('izin.createjenisizin', compact('users', 'department', 'jabatan', 'menu', 'pegawai', 'jenisizin'));
  }

  public function insertjenisIzin()
  {
    $date = date('Y-m-d H:i:s');
    DB::table('jenis_izin')
          ->insert([
                'ji_jenis' => request('jenis'),
                'ji_kategori' => request('kategori'),
                'ji_kode' => request('kode'),
                'ji_created' => $date
        ]);

    //dd($input);

    return redirect()->back();

  }

  public function editjenisizin($id)
  {
      $menu = Menu::All();
      $jenisizin = Jenisizin::All()->where('ji_id', $id);
      return view('izin.updatejenisizin', compact('jenisizin','menu'));
  }

  public function updatejenisizin($id)
  {
      $date = date('Y-m-d H:i:s');
      $jenisizin = DB::table('jenis_izin')->where('ji_id', $id);
      $jenisizin->update([
        'ji_id' => request('id'),
        'ji_jenis' => request('jenis'),
        'ji_kategori' => request('kategori'),
        'ji_kode' => request('kode'),
        'ji_created' => $date
      ]);

      return redirect()->route('jenisizin');
  }

  public function destroyjenisIzin($id)
  {
      $jizin = Jenisizin::find($id);
      $jizin->delete();
      return redirect()->route('jenisizin');
  }

}
