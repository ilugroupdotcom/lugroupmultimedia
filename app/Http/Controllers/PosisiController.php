<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Posisi;
use App\Department;
use App\Jabatan;
use App\Menu;
use DB;

class PosisiController extends Controller
{
  public function create()
  {
      $menu = Menu::All();
      $users = User::All();
      $selectedUser = User::first()->id;
      $department = Department::All();
      $jabatan = Jabatan::All();
      return view('posisi.posisi', compact('users','selectedUser','department','jabatan','menu'));
  }

  public function detail($id)
  {
      $menu = Menu::All();
      $posisi = DB::table("posisi")->where('pos_id', '=', $id)->get();
      return view('posisi.detailposisi', compact('posisi','menu'));
  }

  public function insert()
  {
      $tgl = date('Y-m-d');
      Posisi::create([
          'user_id' => request('id'),
          'pos_department' => request('department'),
          'pos_jabatan' => request('jabatan'),
          'pos_tipe' => request('tipe'),
          'pos_mulaikerja' => request('tanggal'),
          'pos_cabang' => request('cabang'),
          'pos_date_update' => $tgl


      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function edit($id)
  {
      $menu = Menu::All();
      $posisi = DB::table("posisi")->where('pos_id', '=', $id)->get();
      return view('posisi.updateposisi', compact('posisi','menu'));
  }

  public function update($id)
  {
      $tgl = date('Y-m-d');
      $posisi = DB::table("posisi")->where('Pos_id', '=', $id);
      $posisi->update([
        'user_id' => request('id'),
        'pos_department' => request('department'),
        'pos_jabatan' => request('jabatan'),
        'pos_tipe' => request('tipe'),
        'pos_mulaikerja' => request('tanggal'),
        'pos_cabang' => request('cabang'),
        'pos_date_update' => $tgl
      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function destroy($id)
  {
      $user = Posisi::find($id);
      $user->delete();
      return redirect()->route('pegawai.detail', request('userid') );
  }

  public function deleteAll(Request $request)
  {
      $ids = $request->ids;
      DB::table("posisi")->whereIn('pos_id',explode(",",$ids))->delete();
      return response()->json(['success'=>"Products Deleted successfully."]);
  }
}
