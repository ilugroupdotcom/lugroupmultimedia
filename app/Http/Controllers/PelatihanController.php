<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Pelatihan;
use \App\User;
use \App\Menu;
use DB;

class PelatihanController extends Controller
{
  public function create()
  {
      $menu = Menu::All();
      $users = User::All();
      $selectedUser = User::first()->id;
      return view('pelatihan.pelatihan', compact('users','selectedUser','menu'));
  }

  public function detail($id)
  {
      $menu = Menu::All();
      $pelatihan = DB::table("pelatihan")->where('pelatihan_id', '=', $id)->get();
      return view('pelatihan.detailpelatihan', compact('pelatihan','menu'));
  }

  public function insert()
  {
      Pelatihan::create([
          'user_id' => request('id'),
          'title' => request('title'),
          'lokasi' => request('lokasi'),
          'penyelenggara' => request('penyelenggara'),
          'date_mulai' => request('mulai'),
          'date_selesai' => request('selesai'),
          'no_sertifikat' => request('no_set'),
          'tgl_sertifikat' => request('tgl_set'),


      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function edit($id)
  {
      $menu = Menu::All();
      $pelatihan = DB::table("pelatihan")->where('pelatihan_id', '=', $id)->get();
      return view('pelatihan.updatepelatihan', compact('pelatihan','menu'));
  }

  public function update($id)
  {
      $pelatihan = DB::table("pelatihan")->where('Pelatihan_id', '=', $id);
      $pelatihan->update([
        'user_id' => request('id'),
        'title' => request('title'),
        'lokasi' => request('lokasi'),
        'penyelenggara' => request('penyelenggara'),
        'date_mulai' => request('mulai'),
        'date_selesai' => request('selesai'),
        'no_sertifikat' => request('no_set'),
        'tgl_sertifikat' => request('tgl_set'),
      ]);

      return redirect()->route('pegawai.detail', request('id') );
  }

  public function destroy($id)
  {
      $user = Pelatihan::find($id);
      $user->delete();
      return redirect()->route('pegawai.detail', request('userid') );
  }

  public function deleteAll(Request $request)
  {
      $ids = $request->ids;
      DB::table("pelatihan")->whereIn('pelatihan_id',explode(",",$ids))->delete();
      return response()->json(['success'=>"Products Deleted successfully."]);
  }
}
