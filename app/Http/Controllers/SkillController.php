<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Skill;
use App\User;
use App\Menu;
use DB;

class SkillController extends Controller
{

  public function index()
      {
          $menu = Menu::All();
          $skills = DB::table("skills")->get();
          $skills = \App\Skill::paginate(5);
          return view('skill/dataskill',compact('skills','menu'));
      }

  public function detail($id)
  {
      $menu = Menu::All();
      $skills = DB::table("skills")->where('skill_id', '=', $id)->get();
      return view('skill.detailskill', compact('skills','menu'));
  }

  public function create()
  {
      $menu = Menu::All();
      $users = User::All();
      $selectedUser = User::first()->id;
      return view('skill.skill', compact('users','selectedUser','menu'));
  }

  public function insert()
  {
      Skill::create([
          'user_id' => request('id'),
          'skill' => request('skill'),
          'deskripsi' => request('deskripsi'),
      ]);
      $user = request('id');
      return redirect()->route('pegawai.detail', $user);
  }

  public function edit($id)
  {
      $menu = Menu::All();
      $skills = DB::table("skills")->where('skill_id', '=', $id)->get();
      return view('skill.updateskill', compact('skills','menu'));
  }

  public function update($id)
  {
      $skills = DB::table("skills")->where('skill_id', '=', $id);
      $skills->update([
        'skill' => request('skill'),
        'deskripsi' => request('deskripsi'),
      ]);

      return redirect()->route('pegawai.detail');
  }

  public function destroy($id)
    {
    	DB::table("skills")->delete($id);
    	return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("skills")->whereIn('skill_id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
}
