<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Menu;
use App\User;
use App\Usergroup;
use DB;

class UserController extends Controller
{
  public function index()
  {
      $menu = Menu::orderBy('m_urutan')->get();
      return view('user.profile', compact('menu'));
  }

  public function user()
  {
      $menu = Menu::orderBy('m_urutan')->get();
      $user = User::All();
      return view('user.user', compact('menu','user'));
  }

  public function createuser()
  {
      $menu = Menu::orderBy('m_urutan')->get();
      $usergroup = Usergroup::All();
      return view('user.tambahuser', compact('menu','usergroup'));
  }

  public function insertuser()
  {
    $date = date('Y-m-d H:i:s');
    DB::table('users')
          ->insert([
                'username' => request('username'),
                'name' => request('name'),
                'nama_belakang' => request('last_name'),
                'gelar_depan' => " ",
                'gelar_belakang' => " ",
                'tempat_lahir_provinsi' => " ",
                'tempat_lahir_kota' => " ",
                'tanggal_lahir' => request('tanggallahir'),
                'alamat' => request('alamat'),
                'provinsi' => request('provinsi'),
                'kota' => request('kota'),
                'kode_pos' => request('kodepos'),
                'handphone' => request('hp'),
                'telephone' => " ",
                'agama' => " ",
                'email' => request('email'),
                'nik' => request('no_identitas'),
                'department' => " ",
                'jabatan' => " ",
                'tanggal_kerja' => $date,
                'gender' => request('jeniskelamin'),
                'status' => "Kontrak",
                'password' => request('password'),
                'hak_akses' => request('usergroup'),
                'photo' => " "

        ]);

    //dd($input);

    return redirect()->back();

  }
}
