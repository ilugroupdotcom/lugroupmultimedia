<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Menu;
use App\User;
use App\Submenu;
use App\Submenu1;
use DB;

class MenuController extends Controller
{
  public function index()
  {
    $menu = Menu::All();
    $mmenu = DB::table('menu')->paginate(10);
    return view('menu.menu', compact('menu','mmenu'));
  }

  public function createmenu()
  {
    $menu = Menu::All();
    return view('menu.createmenu', compact('menu'));
  }

  public function insertmenu()
  {
      $date = date('Y-m-d H:i:s');
      Menu::create([
          'm_menu' => request('label'),
          'm_icon' => request('icon'),
          'm_link' => request('link'),
          'm_date' => $date

      ]);

      return redirect()->route('menu');
  }

  public function editmenu($id)
  {
      $menu = Menu::All();
      $mmenu = DB::table("menu")->where('m_id', '=', $id)->get();
      return view('menu.updatemenu', compact('menu','mmenu'));
  }

  public function updatemenu($id)
  {
      $date = date('Y-m-d H:i:s');
      $menu = DB::table("menu")->where('m_id', '=', $id);
      $menu->update([
        'm_menu' => request('label'),
        'm_icon' => request('icon'),
        'm_link' => request('link'),
        'm_date' => $date
      ]);

      return redirect()->route('menu');
  }

  public function destroymenu($id)
  {
      $menu = Menu::find($id);
      $menu->delete();
      return redirect()->route('menu');
  }

  public function submenu()
  {
    $menu = Menu::All();
    $submenu = DB::table('submenu')
            ->join('menu','submenu.sub_m_id','=','menu.m_id')
            ->paginate(10);
    return view('menu.submenu', compact('submenu','menu'));
  }

  public function createsub()
  {
    $menu = Menu::All();
    $users = User::All();
    $selectedMenu = Menu::first()->id;
    return view('menu.createsub', compact('users','menu','selectedMenu'));
  }

  public function insertsub()
  {
      $date = date('Y-m-d H:i:s');
      Submenu::create([
          'sub_m_id' => request('menu'),
          'sub_label' => request('label'),
          'sub_link' => request('link'),
          'sub_date' => $date

      ]);

      return redirect()->route('submenu');
  }

  public function editsub($id)
  {
      $menu = Menu::All();
      $selectedMenu = Menu::first()->id;
      $submenu = DB::table("submenu")->where('sub_id', '=', $id)->get();
      return view('menu.updatesubmenu', compact('submenu','menu','selectedMenu'));
  }

  public function updatesub($id)
  {
      $date = date('Y-m-d H:i:s');
      $submenu = DB::table("submenu")->where('sub_id', '=', $id);
      $submenu->update([
        'sub_m_id' => request('menu'),
        'sub_label' => request('label'),
        'sub_link' => request('link'),
        'sub_date' => $date
      ]);

      return redirect()->route('submenu');
  }

  public function destroy($id)
  {
      $submenu = Submenu::find($id);
      $submenu->delete();
      return redirect()->route('submenu');
  }

  public function detail($id)
  {
      $menu = Menu::All();
      $submenu = DB::table("submenu")->where('sub_id', '=', $id)->get();
      return view('menu.detailsub', compact('submenu','menu'));
  }

  public function submenu1()
  {
    $menu = Menu::All();
    $submenu1 = DB::table('submenu1')
            ->join('submenu','submenu1.sub1_sub_id','=','submenu.sub_id')
            ->paginate(10);
    return view('menu.submenu1', compact('submenu1','menu'));
  }

  public function createsub1()
  {
    $menu = Menu::All();
    $submenu = Submenu::join('menu','submenu.sub_m_id','=','menu.m_id')->get();
    $users = User::All();
    $selectedMenu = Submenu::first()->id;
    return view('menu.createsub1', compact('users','menu','selectedMenu','submenu'));
  }

  public function insertsub1()
  {
      $date = date('Y-m-d H:i:s');
      Submenu1::create([
          'sub1_sub_id' => request('submenu'),
          'sub1_label' => request('label1'),
          'sub1_link' => request('link1'),
          'sub1_date' => $date

      ]);

      return redirect()->route('submenu1');
  }

  public function editsub1($id)
  {
      $menu = Menu::All();
      $submenu = Submenu::join('menu','submenu.sub_m_id','=','menu.m_id')->get();
      $selectedMenu = Submenu::first()->id;
      $submenu1 = DB::table("submenu1")->where('sub1_id', '=', $id)->get();
      return view('menu.updatesubmenu1', compact('submenu','menu','selectedMenu','submenu1'));
  }

  public function updatesub1($id)
  {
      $date = date('Y-m-d H:i:s');
      $submenu1 = DB::table("submenu1")->where('sub1_id', '=', $id);
      $submenu1->update([
        'sub1_sub_id' => request('submenu'),
        'sub1_label' => request('label1'),
        'sub1_link' => request('link1'),
        'sub1_date' => $date
      ]);

      return redirect()->route('submenu1');
  }

  public function detailsub1($id)
  {
      $menu = Menu::All();
      $submenu = Submenu::join('menu','submenu.sub_m_id','=','menu.m_id')->get();
      $selectedMenu = Submenu::first()->id;
      $submenu1 = DB::table("submenu1")->where('sub1_id', '=', $id)->get();
      return view('menu.updatesubmenu1', compact('submenu','menu','selectedMenu','submenu1'));
  }

  public function destroysub1($id)
  {
      $submenu1 = Submenu1::find($id);
      $submenu1->delete();
      return redirect()->route('submenu1');
  }

}
