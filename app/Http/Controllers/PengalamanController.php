<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\User;
use \App\Pengalaman;
use App\Menu;
use DB;


class PengalamanController extends Controller
{

  public function detail($id)
  {
      $menu = Menu::All();
      $pengalaman = DB::table("pengalaman")->where('pengalaman_id', '=', $id)->get();
      return view('pengalaman.detailpengalaman', compact('pengalaman','menu'));
  }


  public function create()
  {
      $users = User::All();
      $selectedUser = User::first()->id;
      return view('pengalaman.pengalaman', compact('users','selectedUser'));
  }

  public function insert()
  {
      Pengalaman::create([
          'user_id' => request('id'),
          'nama_perusahaan' => request('perusahaan'),
          'jabatan' => request('jabatan'),
          'unit_kerja' => request('unit'),
          'no_sk' => request('nosk'),
          'tgl_sk' => request('tglsk'),

      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function edit($id)
  {
      $menu = Menu::All();
      $pengalaman = DB::table("pengalaman")->where('pengalaman_id', '=', $id)->get();
      return view('pengalaman.updatepengalaman', compact('pengalaman','menu'));
  }

  public function update($id)
  {
      $pengalaman = DB::table("pengalaman")->where('Pengalaman_id', '=', $id);
      $pengalaman->update([
        'user_id' => request('id'),
        'nama_perusahaan' => request('perusahaan'),
        'jabatan' => request('jabatan'),
        'unit_kerja' => request('unit'),
        'no_sk' => request('nosk'),
        'tgl_sk' => request('tglsk'),
      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function destroy($id)
  {
      $user = Pengalaman::find($id);
      $user->delete();
      return redirect()->route('pegawai.detail', request('userid') );
  }

  public function deleteAll(Request $request)
  {
      $ids = $request->ids;
      DB::table("pengalaman")->whereIn('pengalaman_id',explode(",",$ids))->delete();
      return response()->json(['success'=>"Products Deleted successfully."]);
  }
}
