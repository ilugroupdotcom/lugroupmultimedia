<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\User;
use \App\Reward;
use \App\Menu;

class RewardController extends Controller
{
  public function create()
  {
      $menu = Menu::All();
      $users = User::All();
      $selectedUser = User::first()->id;
      return view('reward.reward', compact('users','selectedUser','menu'));
  }

  public function insert()
  {
      Reward::create([
          'rew_user_id' => request('id'),
          'rew_title' => request('title'),
          'rew_nama_pemberi' => request('pemberi'),
          'rew_nosk' => request('nosk'),
          'rew_tgl_keputusan' => request('task'),
          'rew_update' => date('Y-m-d')

      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function edit($id)
  {
      $menu = Menu::All();
      $reward = Reward::where('rew_id', '=', $id)->get();
      return view('reward.updatereward', compact('reward','menu'));
  }

  public function update($id)
  {
      $reward = Reward::where('rew_id', '=', $id);
      $reward->update([
        'rew_user_id' => request('id'),
        'rew_title' => request('title'),
        'rew_nama_pemberi' => request('pemberi'),
        'rew_nosk' => request('nosk'),
        'rew_tgl_keputusan' => request('task'),
        'rew_update' => date('Y-m-d')
      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function detail($id)
  {
      $menu = Menu::All();
      $reward = Reward::where('rew_id', '=', $id)->get();
      return view('reward.detailreward', compact('reward','menu'));
  }

  public function destroy($id)
  {
      $user = Reward::find($id);
      $user->delete();
      return redirect()->route('pegawai.detail', request('userid') );
  }

  public function deleteAll(Request $request)
  {
      $ids = $request->ids;
      Reward::whereIn('rew_id',explode(",",$ids))->delete();
      return response()->json(['success'=>"Products Deleted successfully."]);
  }

}
