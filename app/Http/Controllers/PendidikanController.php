<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\User;
use \App\Pendidikans;
use \App\Menu;
use DB;

class PendidikanController extends Controller
{
  public function create()
  {
      $menu = Menu::All();
      $users = User::All();
      $selectedUser = User::first()->id;
      return view('pendidikan.pendidikan', compact('users','selectedUser','menu'));
  }

  public function destroy($id)
  {
      $user = pendidikans::find($id);
      $user->delete();
      return redirect()->route('pegawai.detail', request('userid') );
  }

  public function detail($id)
  {
      $menu = Menu::All();
      $riwayat = DB::table("pendidikans")->where('rpen_id', '=', $id)->get();
      return view('pendidikan.detailpen', compact('riwayat','menu'));
  }

  public function insert()
  {
      Pendidikans::create([
          'user_id' => request('id'),
          'sekolah' => request('sekolah'),
          'kota' => request('kota'),
          'tingkat_pen' => request('tingkat'),
          'jurusan' => request('jurusan'),
          'no_ijazah' => request('ijazah'),
          'date_keluar' => request('lulus'),

      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function edit($id)
  {
      $menu = Menu::All();
      $riwayat = DB::table("pendidikans")->where('rpen_id', '=', $id)->get();
      return view('pendidikan.updatependidikan', compact('riwayat','menu'));
  }

  public function update($id)
  {
      $riwayat = DB::table("pendidikans")->where('rpen_id', '=', $id);
      $riwayat->update([
        'user_id' => request('id'),
        'sekolah' => request('sekolah'),
        'kota' => request('kota'),
        'tingkat_pen' => request('tingkat'),
        'jurusan' => request('jurusan'),
        'no_ijazah' => request('ijazah'),
        'date_keluar' => request('lulus'),
      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function deleteAll(Request $request)
  {
      $ids = $request->ids;
      DB::table("pendidikans")->whereIn('rpen_id',explode(",",$ids))->delete();
      return response()->json(['success'=>"Products Deleted successfully."]);
  }
}
