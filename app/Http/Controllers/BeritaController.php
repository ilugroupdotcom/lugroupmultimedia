<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Berita;
use App\User;
use App\Menu;
use DB;

class BeritaController extends Controller
{

  public function index()
  {
      $menu = Menu::All();
      $berita = Berita::join('users','berita.be_user_id','=','users.id')
                ->paginate(5);
      return view('berita.beritaperusahaan', compact('berita','menu'));
  }

  public function create()
  {
      $menu = Menu::All();
      $users = User::All();
      return view('berita.createbp', compact('users','menu'));
  }

  public function insert()
  {
      Berita::create([
          'be_user_id' => request('id'),
          'be_title' => request('title'),
          'be_draft' => request('draft'),
          'be_time_create' => date("Y-m-d h:i:s"),
          'be_time_update' => date("Y-m-d h:i:s"),

      ]);

      return redirect()->route('berita');
  }

  public function edit($id)
  {
      $menu = Menu::All();
      $berita = Berita::where('be_id', '=', $id)->get();
      return view('berita.updateberitaperusahaan', compact('berita','menu'));
  }

  public function update($id)
  {
      $berita = Berita::where('be_id', '=', $id);
      $berita->update([
        'be_user_id' => request('id'),
        'be_title' => request('title'),
        'be_draft' => request('draft'),
        'be_time_create' => request('create'),
        'be_time_update' => date("Y-m-d h:i:s"),
      ]);

      return redirect()->route('berita');
  }

  public function detail($id)
  {
      $menu = Menu::All();
      $berita = Berita::where('be_id', '=', $id)->get();
      return view('berita.detailberita_perusahaan', compact('berita','menu'));
  }

  public function destroy($id)
  {
      $berita = Berita::find($id);
      $berita->delete();
      return redirect()->route('berita', request('berita') );
  }
}
