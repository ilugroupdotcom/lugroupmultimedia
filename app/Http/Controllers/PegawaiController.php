<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use App\User;
use App\Skill;
use App\Pendidikans;
use App\Pengalaman;
use App\Pelatihan;
use App\Posisi;
use App\Family;
use App\Reward;
use App\Department;
use App\Jabatan;
use App\Menu;
use App\Agama;
use App\Diklat;
use App\Jpendidikan;
use App\Jenisjabatan;
use App\Keluarga;
use App\Kpangkat;
use App\Pengunduran;
use App\Pensiun;
use App\Klibur;
use PDF;
use Excel;
use DB;
use Response;

class PegawaiController extends Controller
{
    public function index()
    {
        $menu = Menu::orderBy('m_urutan')->get();
        $users = User::join('posisi','users.id','=','posisi.user_id')
                    ->where('users.status','Tetap')->paginate(10);
        $department = Department::All();
        $jabatan = Jabatan::All();
        return view('pegawai.index', compact('users', 'department', 'jabatan','menu'));
    }

    public function getpdf()
    {
        $users = User::join('posisi','users.id','=','posisi.user_id')->get();
        $pdf=PDF::loadView('absensi.pdf',['users'=>$users]);
        return $pdf->stream('pegawai.pdf');

    }

    public function getexcel()
    {
      $users = User::join('posisi','users.id','=','posisi.user_id')->get();
      Excel::create('sheet', function($users) {
          $users->sheet('sheet', function($sheet) {
            $sheet->cells('A1:A5', function($cells) {
              // Set all borders (top, right, bottom, left)
                $cells->setBorder('solid', 'none', 'none', 'solid');
                // Set borders with array
                $cells->setBorder(array(
                    'top'   => array(
                        'style' => 'solid'
                    ),
                ));
            });
            $sheet->loadView('absensi.excel', compact('users'));
          });

        })->export('xlsx');
    }

    public function profile()
    {
        $menu = Menu::All();
        return view('pegawai.profile', compact('menu'));
    }


    public function autocomplete(Request $request){
      $term = request('term');
      $result = array();
      $queries = User::Where('name','LIKE','%'.$term.'%')
              ->where('status','=','Tetap')
              ->take(5)->get();
      foreach ($queries as $query) {
          $results[] = ['id' => $query->id, 'value' => $query->name];
      }
      return Response::json($results);
    }

    public function autocompletekontrak(Request $request){
      $term = request('term');
      $result = array();
      $queries = User::Where('name','LIKE','%'.$term.'%')
              ->where('status','=','Kontrak')
              ->take(5)->get();
      foreach ($queries as $query) {
          $results[] = ['id' => $query->id, 'value' => $query->name];
      }
      return Response::json($results);
    }

    public function search(Request $request)
    {
        $menu = Menu::orderBy('m_urutan')->get();
        $name = request('name');
        $department = request('department');
        $jabatan = request('jabatan');
        $gender = request('gender');
        if($name){
          $users = User::join('posisi','users.id','=','posisi.user_id')
                      ->where('users.name','LIKE', '%'.$name.'%')
                      ->paginate(10);
        }
        elseif (($department) && ($jabatan) && ($status)) {
          $users = User::join('posisi','users.id','=','posisi.user_id')
                      ->Where('posisi.pos_department','=', $department)
                      ->where('posisi.pos_jabatan','=', $jabatan)
                      ->where('users.gender','=', $status)
                      ->where('users.status','=','Tetap')
                      ->paginate(10);
        }
        elseif (($department) && ($jabatan)) {
          $users = User::join('posisi','users.id','=','posisi.user_id')
                      ->Where('posisi.pos_department','=', $department)
                      ->where('posisi.pos_jabatan','=', $jabatan)
                      ->where('users.status','=','Tetap')
                      ->paginate(10);
        }
        elseif($gender){
          $users = User::join('posisi','users.id','=','posisi.user_id')
                      ->where('users.gender','=', $gender)
                      ->where('users.status','=','Tetap')
                      ->paginate(10);
        }
        elseif ($department) {
          $users = User::join('posisi','users.id','=','posisi.user_id')
                      ->where('posisi.pos_department','=', $department)
                      ->where('users.status','=','Tetap')
                      ->paginate(10);
        }
        elseif ($jabatan) {
          $users = User::join('posisi','users.id','=','posisi.user_id')
                      ->where('posisi.pos_jabatan','=', $jabatan)
                      ->where('users.status','=','Tetap')
                      ->paginate(10);
        }
        else{
          $users = User::join('posisi','users.id','=','posisi.user_id')
                      ->where('users.status','Tetap')->paginate(10);
        }
          $department = Department::All();
          $jabatan = Jabatan::All();
        return view('pegawai.index', compact('users','name','department','jabatan','gender','department','jabatan','menu'));
    }

    public function kontrak()
    {
        $menu = Menu::orderBy('m_urutan')->get();
        $users = User::where('status','Kontrak')->paginate(10);
        return view('pegawai.kontrak', compact('users','menu'));
    }

    public function searchkontrak(Request $request)
    {
        $menu = Menu::orderBy('m_urutan')->get();
        $name = request('name');
        $gender = request('gender');
        if($name){
          $users = User::where('name','LIKE', '%'.$name.'%')
                      ->where('status','=', 'Kontrak')
                      ->paginate(10);
        }
        elseif($gender){
          $users = User::where('gender','=', $gender)
                      ->where('status','=','Kontrak')
                      ->paginate(10);
        }
        else{
          $users = User::where('users.status','Kontrak')->paginate(10);
        }
          return view('pegawai.kontrak', compact('users','name','gender','menu'));
    }

    public function loginuser()
    {

        return view('login');
    }

    public function create()
    {
        $menu = Menu::orderBy('m_urutan')->get();
        $users = User::All();
        return view('pegawai.create', compact('users','menu'));
    }

    public function insert(Request $request)
    {

      $file       = $request->file('gambar');
      $fileName   = $file->getClientOriginalName();
      $request->file('gambar')->move("image/", $fileName);
        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'nik' => request('nik'),
            'department' => request('department'),
            'jabatan' => request('jabatan'),
            'tanggal_kerja' => request('tanggal'),
            'gender' => request('gender'),
            'status' => request('status'),
            'photo' => $fileName,
            'password' => bcrypt(request('password')),
            'hak_akses' => 'Staff'
        ]);

        return redirect()->route('pegawai.index');
    }

    public function edit($id)
    {
        $menu = Menu::orderBy('m_urutan')->get();
        $user = User::find($id);
        return view('pegawai.update', compact('user','menu'));
    }

    public function update(Request $request, $id)
    {
      $file       = $request->file('gambar');
      $fileName   = $file->getClientOriginalName();
      $request->file('gambar')->move("image/", $fileName);

        $user = User::find($id);
        $user->update([
          'name' => request('name'),
          'email' => request('email'),
          'nik' => request('nik'),
          'department' => request('department'),
          'jabatan' => request('jabatan'),
          'tanggal_kerja' => request('tanggal'),
          'gender' => request('gender'),
          'status' => request('status'),
          'photo' => $fileName,
          'password' => bcrypt(request('password'))
        ]);

        return redirect()->route('pegawai.index');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('pegawai.index');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("users")->whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }

    public function detail($id)
    {
        $menu = Menu::orderBy('m_urutan')->get();
        $users = User::where('id', $id)->get();
        $user = DB::table('users')->where('id',$id)->get();
        $skills = DB::table('skills')->where('user_id',$id)->get();
        $riwayat = DB::table("pendidikans")->where('user_id',$id)->get();
        $pengalaman = Pengalaman::where('user_id', $id)->get();
        $pelatihan = Pelatihan::where('user_id', $id)->get();
        $posisi = Posisi::where('user_id', $id)->get();
        $family = Family::where('fam_user_id', $id)->get();
        $reward = Reward::where('rew_user_id', $id)->get();
        return view('pegawai.detail', compact('user','skills','riwayat',
        'pengalaman','pelatihan','users','posisi','family','reward','menu'));
    }

    public function loadData(Request $request)
    {
    	if ($request->has('q')) {
    		$cari = $request->q;
    		$data = DB::table('users')->select('id', 'email')->where('email', 'LIKE', '%$cari%')->get();
    		return response()->json($data);
    	}
    }

    public function agama()
    {
      $menu = Menu::orderBy('m_urutan')->get();
      $agama = Agama::All();
      return view('agama.viewagama', compact('menu','agama'));
    }

    public function filteragama()
    {
      $menu = Menu::orderBy('m_urutan')->get();
      $agama = Agama::Where('agama_nama','LIKE', '%'.request('agama').'%')->get();
      return view('agama.filterviewagama', compact('menu','agama'));
    }

    public function createagama()
    {
      $menu = Menu::orderBy('m_urutan')->get();
      $agama = Agama::All();
      return view('agama.createagama', compact('menu','agama'));
    }

    public function insertagama()
    {
      $date = date('Y-m-d H:i:s');
      DB::table('agama')
            ->insert([
                  'agama_nama' => request('agama'),
                  'agama_created' => $date

          ]);

      //dd($input);

      return redirect()->back();

    }

    public function editagama($id)
    {
        $menu = Menu::orderBy('m_urutan')->get();
        $agama = Agama::where('agama_id', $id)->get();
        // dd($izin);
        return view('agama.editagama', compact('agama','menu'));
    }

    public function updateagama($id)
    {
        $date = date('Y-m-d H:i:s');
        $agama = DB::table('agama')->where('agama_id', $id);
        $agama->update([
          'agama_nama' => request('agama'),
          'agama_created' => $date
        ]);

        return redirect()->back();
    }

    public function destroyagama($id)
    {
        $agama =Agama::find($id);
        $agama->delete();
        return redirect()->route('agama');
    }

    public function diklat()
    {
      $menu = Menu::orderBy('m_urutan')->get();
      $diklat = Diklat::All();
      return view('diklat.viewdiklat', compact('menu','diklat'));
    }

    public function filterdiklat()
    {
      $menu = Menu::orderBy('m_urutan')->get();
      $diklat = Diklat::Where('diklat_nama','LIKE', '%'.request('diklat').'%')->get();
      return view('diklat.filterviewdiklat', compact('menu','diklat'));
    }

    public function creatediklat()
    {
      $menu = Menu::orderBy('m_urutan')->get();
      $diklat = Diklat::All();
      return view('diklat.creatediklat', compact('menu','diklat'));
    }

    public function insertdiklat()
    {
      $date = date('Y-m-d H:i:s');
      DB::table('diklat')
            ->insert([
                  'diklat_nama' => request('diklat'),
                  'diklat_created' => $date

          ]);

      //dd($input);

      return redirect()->back();

    }

    public function editdiklat($id)
    {
        $menu = Menu::All();
        $diklat = Diklat::where('diklat_id', $id)->get();
        // dd($izin);
        return view('diklat.editdiklat', compact('diklat','menu'));
    }

    public function updatediklat($id)
    {
        $date = date('Y-m-d H:i:s');
        $diklat = DB::table('diklat')->where('diklat_id', $id);
        $diklat->update([
          'diklat_nama' => request('diklat'),
          'diklat_created' => $date
        ]);

        return redirect()->back();
    }

    public function destroydiklat($id)
    {
        $diklat = Diklat::find($id);
        $diklat->delete();
        return redirect()->route('diklat');
    }

    public function jpendidikan()
    {
      $menu = Menu::All();
      $jpen = Jpendidikan::All();
      return view('jpendidikan.viewjpendidikan', compact('menu','jpen'));
    }

    public function createjpendidikan()
    {
      $menu = Menu::All();
      return view('jpendidikan.createjpendidikan', compact('menu','jpen'));
    }

    public function insertjpendidikan()
    {
      $date = date('Y-m-d H:i:s');
      DB::table('jenis_pendidikan')
            ->insert([
                  'jpen_tingkat' => request('tingkat'),
                  'jpen_urutan' => "0",
                  'jpen_created' => $date

          ]);

      //dd($input);

      return redirect()->back();

    }

    public function editjpendidikan($id)
    {
        $menu = Menu::All();
        $jpen = Jpendidikan::where('jpen_id', $id)->get();
        // dd($izin);
        return view('jpendidikan.editjpendidikan', compact('jpen','menu'));
    }

    public function updatejpendidikan($id)
    {
        $date = date('Y-m-d H:i:s');
        $diklat = DB::table('jenis_pendidikan')->where('jpen_id', $id);
        $diklat->update([
          'jpen_tingkat' => request('tingkat'),
          'jpen_urutan' => "0",
          'jpen_created' => $date
        ]);

        return redirect()->back();
    }

    public function destroyjpendidikan($id)
    {
        $jpen = Jpendidikan::find($id);
        $jpen->delete();
        return redirect()->route('jpendidikan');
    }

    public function jenisjabatan()
    {
      $menu = Menu::All();
      if (Request('jabatan') <> NULL) {
        $jjab = Jenisjabatan::join('department','jenis_jabatan.jab_department','=','department.dept_id')
                    ->where('jenis_jabatan.jab_nama','LIKE', '%'.Request('jabatan').'%')
                    ->get();
      }elseif (Request('kode') <> NULL) {
        $jjab = Jenisjabatan::join('department','jenis_jabatan.jab_department','=','department.dept_id')
                    ->where('jenis_jabatan.jab_kode','LIKE', '%'.Request('kode').'%')
                    ->get();
      }elseif (Request('atasan') <> NULL) {
        $jjab = Jenisjabatan::join('department','jenis_jabatan.jab_department','=','department.dept_id')
                    ->where('jenis_jabatan.jab_atasan','LIKE', '%'.Request('atasan').'%')
                    ->get();
      }elseif (Request('department') <> NULL) {
        $jjab = Jenisjabatan::join('department','jenis_jabatan.jab_department','=','department.dept_id')
                    ->where('jenis_jabatan.jab_department','LIKE', '%'.Request('department').'%')
                    ->get();
      }
      else {
        $jjab = Jenisjabatan::join('department','jenis_jabatan.jab_department','=','department.dept_id')->get();
      }
      $jabatan = Jenisjabatan::All();
      $department = Department::All();
      return view('jabatan.viewjenisjabatan', compact('menu','jjab','department','jabatan'));
    }

    public function createjenisjabatan()
    {
      $menu = Menu::All();
      $department = Department::All();
      $jjab = Jenisjabatan::All();
      return view('jabatan.createjenisjabatan', compact('menu','department','jjab'));
    }

    public function insertjenisjabatan()
    {
      $date = date('Y-m-d H:i:s');
      DB::table('jenis_jabatan')
            ->insert([
                  'jab_nama' => request('jabatan'),
                  'jab_atasan' => request('atasan', false),
                  'jab_kode' => request('kode'),
                  'jab_department' => request('department'),
                  'jab_created' => $date
          ]);

      //dd($input);

      return redirect()->back();

    }

    public function editjenisjabatan($id)
    {
        $menu = Menu::All();
        $jjab = Jenisjabatan::where('jab_id', $id)->get();
        $jj = Jenisjabatan::All();
        $department = Department::All();
        // dd($izin);
        return view('jabatan.editjenisjabatan', compact('jjab','menu','department','jj'));
    }

    public function updatejenisjabatan($id)
    {
        $date = date('Y-m-d H:i:s');
        $jab = DB::table('jenis_jabatan')->where('jab_id', $id);
        $jab->update([
          'jab_nama' => request('jabatan'),
          'jab_atasan' => request('atasan', false),
          'jab_kode' => request('kode'),
          'jab_department' => request('department', false),
          'jab_created' => $date
        ]);

        return redirect()->back();
    }

    public function destroyjenisjabatan($id)
    {
        $jab = Jenisjabatan::find($id);
        $jab->delete();
        return redirect()->route('jenisjabatan');
    }

    public function keluarga()
    {
      $menu = Menu::All();
      $keluarga = Keluarga::All();
      return view('keluarga.viewkeluarga', compact('menu','keluarga'));
    }

    public function insertkeluarga()
    {
      $date = date('Y-m-d H:i:s');
      DB::table('keluarga')
            ->insert([
                  'kel_nama' => Request('keluarga'),
                  'kel_created' => $date

          ]);

      //dd($input);

      return redirect()->route('keluarga');

    }

    public function updatekeluarga($id)
    {
        $date = date('Y-m-d H:i:s');
        $kel = DB::table('keluarga')->where('kel_id', $id);
        $kel->update([
          'kel_nama' => Request('keluarga'),
          'kel_created' => $date
        ]);

        return redirect()->back();
    }

    public function destroykeluarga($id)
    {
        $kel = Keluarga::find($id);
        $kel->delete();
        return redirect()->route('keluarga');
    }

    public function kenaikanpangkat()
    {
      $menu = Menu::All();
      $kpangkat = Kpangkat::All();
      return view('pangkat.kenaikanpangkat', compact('menu','kpangkat'));
    }

    public function insertkenaikanpangkat()
    {
      $date = date('Y-m-d H:i:s');
      DB::table('kenaikan_pangkat')
            ->insert([
                  'kp_nama' => Request('pangkat'),
                  'kp_created' => $date

          ]);

      //dd($input);

      return redirect()->route('kenaikanpangkat');

    }

    public function updatekenaikanpangkat($id)
    {
        $date = date('Y-m-d H:i:s');
        $kp = DB::table('kenaikan_pangkat')->where('kp_id', $id);
        $kp->update([
          'kp_nama' => Request('pangkat'),
          'kp_created' => $date
        ]);

        return redirect()->back();
    }

    public function destroykenaikanpangkat($id)
    {
        $kp = Kpangkat::find($id);
        $kp->delete();
        return redirect()->route('kenaikanpangkat');
    }

    public function resign()
    {
      $menu = Menu::All();
      if (Request('nama') <> NULL) {
        $resign = Pengunduran::Where('pd_nama','LIKE', '%'.Request('nama').'%')->get();
      }
      else {
        $resign = Pengunduran::All();
      }

      return view('pengunduran.jenisresign', compact('menu','resign'));
    }

    public function insertresign()
    {
      $date = date('Y-m-d H:i:s');
      DB::table('pengunduran_diri')
            ->insert([
                  'pd_nama' => Request('nama'),
                  'pd_created' => $date

          ]);

      //dd($input);

      return redirect()->route('resign');

    }

    public function updateresign($id)
    {
        $date = date('Y-m-d H:i:s');
        $resign = DB::table('pengunduran_diri')->where('pd_id', $id);
        $resign->update([
          'pd_nama' => Request('nama'),
          'pd_created' => $date
        ]);

        return redirect()->back();
    }

    public function destroyresign($id)
    {
        $resign = Pengunduran::find($id);
        $resign->delete();
        return redirect()->back();
    }

    public function pensiun()
    {
      if (Request('nama') <> NULL) {
        $pensiun = Pensiun::Where('pensiun_nama', 'LIKE' ,'%'.Request('nama').'%')->get();
      }
      else {
        $pensiun = Pensiun::All();
      }
      $menu = Menu::All();

      return view('pensiun.jenispensiun', compact('menu','pensiun'));
    }

    public function insertpensiun()
    {
      $date = date('Y-m-d H:i:s');
      DB::table('pensiun')
            ->insert([
                  'pensiun_nama' => Request('nama'),
                  'pensiun_created' => $date

          ]);
      return redirect()->back();

    }

    public function updatepensiun($id)
    {
        $date = date('Y-m-d H:i:s');
        $pensiun = DB::table('pensiun')->where('pensiun_id', $id);
        $pensiun->update([
          'pensiun_nama' => Request('nama'),
          'pensiun_created' => $date
        ]);

        return redirect()->back();
    }

    public function destroypensiun($id)
    {
        $pensiun = Pensiun::find($id);
        $pensiun->delete();
        return redirect()->back();
    }

    public function kallibur()
    {
      if (Request('nama') <> NULL) {
        $klibur = Klibur::Where('kl_title', 'LIKE' ,'%'.Request('nama').'%')->get();
      }elseif (Request('bulan')) {
        $klibur = DB::table('kal_libur')->whereMonth('kl_tgl_start', Request('bulan'))->get();
      }elseif (Request('tahun')) {
        $klibur = DB::table('kal_libur')->whereYear('kl_tgl_end', Request('tahun'))->get();
      }
      else {
        $klibur = Klibur::All();
      }
      $menu = Menu::All();
      $year = DB::table('kal_libur')->distinct()->get();
      return view('kallibur.viewkallibur', compact('menu','klibur', 'year'));
    }

    public function createkallibur()
    {
      $menu = Menu::All();
      return view('kallibur.createkallibur', compact('menu'));
    }

    public function insertkallibur()
    {
      $date = date('Y-m-d H:i:s');
      $tgl_start = Request('start');
      $start = date('Y-m-d', strtotime(Request('start')));
      $end = date('Y-m-d', strtotime(Request('end')));
      DB::table('kal_libur')
            ->insert([
                  'kl_title' => Request('title'),
                  'kl_tgl_start' => $start,
                  'kl_tgl_end' => $end,
                  'kl_created' => $date

          ]);
      return redirect()->back();

    }

    public function updatekallibur($id)
    {
        $date = date('Y-m-d H:i:s');
        $tgl_start = Request('start');
        $start = date('Y-m-d', strtotime(Request('start')));
        $end = date('Y-m-d', strtotime(Request('end')));
        $klibur = DB::table('kal_libur')->where('kl_id', $id);
        $klibur->update([
          'kl_title' => Request('title'),
          'kl_tgl_start' => $start,
          'kl_tgl_end' => $end,
          'kl_created' => $date
        ]);

        return redirect()->back();
    }

    public function destroykallibur($id)
    {
        $klibur = Klibur::find($id);
        $klibur->delete();
        return redirect()->back();
    }



}
