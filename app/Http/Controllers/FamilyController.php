<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\User;
use \App\Family;
use \App\Menu;

class FamilyController extends Controller
{
  public function create()
  {
     $menu = Menu::All();
      $users = User::All();
      $selectedUser = User::first()->id;
      return view('family.family', compact('users','selectedUser','menu'));
  }

  public function insert()
  {
      Family::create([
          'fam_user_id' => request('id'),
          'fam_nama' => request('keluarga'),
          'fam_tempat_lahir' => request('tempat'),
          'fam_tgl_lahir' => request('tanggal'),
          'fam_gender' => request('gender'),
          'fam_pendidikan' => request('pendidikan'),
          'fam_pekerjaan' => request('pekerjaan'),
          'fam_agama' => request('agama'),
          'fam_status_nikah' => request('nikah'),
          'fam_status_hidup' => request('hidup'),
          'fam_status_anak' => request('anak'),
          'fam_update' => date('Y-m-d')

      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function edit($id)
  {
      $menu = Menu::All();
      $family = Family::where('fam_id', '=', $id)->get();
      return view('family.updatefamily', compact('family','menu'));
  }

  public function update($id)
  {
      $family = Family::where('fam_id', '=', $id);
      $family->update([
        'fam_nama' => request('keluarga'),
        'fam_tempat_lahir' => request('tempat'),
        'fam_tgl_lahir' => request('tanggal'),
        'fam_gender' => request('gender'),
        'fam_pendidikan' => request('pendidikan'),
        'fam_pekerjaan' => request('pekerjaan'),
        'fam_agama' => request('agama'),
        'fam_status_nikah' => request('nikah'),
        'fam_status_hidup' => request('hidup'),
        'fam_status_anak' => request('anak'),
        'fam_update' => date('Y-m-d')
      ]);

      return redirect()->route('pegawai.detail', request('id'));
  }

  public function detail($id)
  {
      $family = Family::where('fam_id', '=', $id)->get();
      return view('family.detailfamily', compact('family'));
  }

  public function destroy($id)
  {
      $user = Family::find($id);
      $user->delete();
      return redirect()->route('pegawai.detail', request('userid') );
  }

  public function deleteAll(Request $request)
  {
      $ids = $request->ids;
      Family::whereIn('fam_id',explode(",",$ids))->delete();
      return response()->json(['success'=>"Products Deleted successfully."]);
  }
}
