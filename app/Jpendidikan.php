<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jpendidikan extends Model
{
  protected $table = 'jenis_pendidikan';
  protected $primaryKey = 'jpen_id';
  public $timestamps = false;
  protected $fillable = [
      'jpen_id','jpen_tingkat','jpen_urutan','jpen_created'
  ];
}
