<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
  protected $table = 'keluarga';
  protected $primaryKey = 'kel_id';
  public $timestamps = false;
  protected $fillable = [
      'kel_id','kel_nama','kel_created'
  ];
}
