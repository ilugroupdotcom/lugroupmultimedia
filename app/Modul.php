<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modul extends Model
{
  protected $table = 'modul';
  protected $primaryKey = 'mod_id';
  public $timestamps = false;
  protected $fillable = [
      'mod_id','mod_label','mod_alias','mod_date'
  ];
}
