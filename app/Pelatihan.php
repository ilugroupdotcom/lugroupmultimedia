<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelatihan extends Model
{
  protected $table = 'pelatihan';
  protected $primaryKey = 'pelatihan_id';
  public $timestamps = false;
  protected $fillable = [
      'pelatihan_id','user_id','title', 'lokasi', 'date_mulai', 'date_selesai', 'penyelenggara', 'no_sertifikat', 'tgl_sertifikat'
  ];
}
