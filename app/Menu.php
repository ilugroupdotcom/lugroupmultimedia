<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
  protected $table = 'menu';
  protected $primaryKey = 'm_id';
  public $timestamps = false;
  protected $fillable = [
      'm_id','m_menu','m_icon','m_link','m_urutan','m_date'
  ];
}
