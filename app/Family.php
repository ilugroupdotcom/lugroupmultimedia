<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
  protected $table = 'family';
  protected $primaryKey = 'fam_id';
  public $timestamps = false;
  protected $fillable = [
      'fam_id','fam_user_id','fam_nama', 'fam_tempat_lahir', 'fam_tgl_lahir',
      'fam_gender', 'fam_pendidikan','fam_pekerjaan', 'fam_agama', 'fam_status_nikah', 'fam_status_hidup',
      'fam_status_anak','fam_update'
  ];
}
