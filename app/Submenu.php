<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submenu extends Model
{
  protected $table = 'submenu';
  protected $primaryKey = 'sub_id';
  public $timestamps = false;
  protected $fillable = [
      'sub_id','sub_m_id','sub_label','sub_link','sub_date'
  ];
}
