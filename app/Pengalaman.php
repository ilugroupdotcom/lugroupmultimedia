<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengalaman extends Model
{
    protected $table = 'pengalaman';
    protected $primaryKey = 'pengalaman_id';
    public $timestamps = false;
    protected $fillable = [
        'pengalaman_id','user_id','nama_perusahaan', 'jabatan', 'unit_kerja', 'no_sk', 'tgl_sk'
    ];

    public function user()
    {
       return $this->belongsTo(User::class);
    }
}
