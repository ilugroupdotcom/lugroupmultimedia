<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('pegawai.index');
});



Auth::routes();

Route::get('/', 'PegawaiController@loginuser')->name('pegawai.login');
Route::get('/home', 'DefaultController@index')->name('home');
Route::get('pegawai/autocomplete', 'PegawaiController@autocomplete');
Route::get('pegawai/profile', 'PegawaiController@profile')->name('pegaai.profile');

Route::get('/loginuser', 'PegawaiController@loginuser')->name('pegawai.login');
Route::get('/pegawai/profile', 'PegawaiController@profile')->name('pegawai.profile');
Route::get('/pegawai', 'PegawaiController@index')->name('pegawai.index');
Route::post('/pegawai', 'PegawaiController@search')->name('pegawai.search');
Route::get('/pegawai/kontrak', 'PegawaiController@kontrak')->name('pegawai.kontrak');
Route::post('/pegawai/kontrak', 'PegawaiController@searchkontrak')->name('pegawai.searchkontrak');
Route::get('/pegawai/{id}/detail', 'PegawaiController@detail')->name('pegawai.detail');
Route::get('/pegawai/create', 'PegawaiController@create')->name('pegawai.create');
Route::post('/pegawai/create', 'PegawaiController@insert')->name('pegawai.insert');
Route::get('/pegawai/{id}/edit', 'PegawaiController@edit')->name('pegawai.edit');
Route::patch('/pegawai/{id}/update', 'PegawaiController@update')->name('pegawai.update');
Route::delete('pegawai/{id}/delete', 'PegawaiController@destroy')->name('pegawai.destroy');
Route::get('pegawai/getpdf', 'PegawaiController@getpdf')->name('pegawai.getpdf');
Route::get('pegawai/getexcel', 'PegawaiController@getexcel')->name('pegawai.getexcel');
Route::delete('pegawaiDeleteAll', 'PegawaiController@deleteAll');
Route::get('/cari', 'PegawaiController@loadData');

Route::get('/skill/skill', 'SkillController@create')->name('skill.skill');
Route::get('skill', 'SkillController@index')->name('skill.index');
Route::get('/skill/{id}/detail', 'SkillController@detail')->name('skill.detail');
Route::post('/skill/skill', 'SkillController@insert')->name('skill.insert');
Route::get('/skill/{id}/edit', 'SkillController@edit')->name('skill.edit');
Route::patch('/skill/{id}/update', 'SkillController@update')->name('skill.update');
Route::delete('skill/{id}', 'SkillController@destroy');
Route::delete('skillDeleteAll', 'SkillController@deleteAll');

Route::get('/pendidikan/create', 'PendidikanController@create')->name('pendidikan.create');
Route::post('/pendidikan/create', 'PendidikanController@insert')->name('pendidikan.insert');
Route::get('/pendidikan/{id}/detail', 'PendidikanController@detail')->name('pendidikan.detail');
Route::get('/pendidikan/{id}/edit', 'PendidikanController@edit')->name('pendidikan.edit');
Route::patch('/pendidikan/{id}/update', 'PendidikanController@update')->name('pendidikan.update');
Route::delete('pendidikan/{id}/delete', 'PendidikanController@destroy')->name('pendidikan.destroy');
Route::delete('pendidikanDeleteAll', 'PendidikanController@deleteAll');

Route::get('/pengalaman/create', 'PengalamanController@create')->name('pengalaman.create');
Route::post('/pengalaman/create', 'PengalamanController@insert')->name('pengalaman.insert');
Route::get('/pengalaman/{id}/detail', 'PengalamanController@detail')->name('pengalaman.detail');
Route::get('/pengalaman/{id}/edit', 'PengalamanController@edit')->name('pengalaman.edit');
Route::patch('/pengalaman/{id}/update', 'PengalamanController@update')->name('pengalaman.update');
Route::delete('pengalaman/{id}/delete', 'PengalamanController@destroy')->name('pengalaman.destroy');
Route::delete('pengalamanDeleteAll', 'PengalamanController@deleteAll');

Route::get('/pelatihan/create', 'PelatihanController@create')->name('pelatihan.create');
Route::post('/pelatihan/create', 'PelatihanController@insert')->name('pelatihan.insert');
Route::get('/pelatihan/{id}/detail', 'PelatihanController@detail')->name('pelatihan.detail');
Route::get('/pelatihan/{id}/edit', 'PelatihanController@edit')->name('pelatihan.edit');
Route::patch('/pelatihan/{id}/update', 'PelatihanController@update')->name('pelatihan.update');
Route::delete('pelatihan/{id}/delete', 'PelatihanController@destroy')->name('pelatihan.destroy');
Route::delete('pelatihanDeleteAll', 'PelatihanController@deleteAll');

Route::get('/calonpegawai', 'CalonController@index')->name('calon.index');
Route::get('/calon/seleksi', 'CalonController@seleksi')->name('calon.seleksi');
Route::get('/calon/hasil', 'CalonController@hasil')->name('calon.hasil');
Route::get('/calon/{id}/penilaian', 'CalonController@penilaian')->name('calon.nilai');
Route::get('/calon/create', 'CalonController@create')->name('calon.create');
Route::post('/calon/create', 'CalonController@insert')->name('calon.insert');
Route::post('/calon/{id}/employe', 'CalonController@employe')->name('calon.employe');
Route::post('/calon/{id}/approve', 'CalonController@approve')->name('calon.approve');
Route::post('/calon/{id}/nilai', 'CalonController@nilai')->name('calon.poinnilai');
Route::delete('calonDeleteAll', 'CalonController@deleteAll');

Route::get('/posisi/create', 'PosisiController@create')->name('posisi.create');
Route::post('/posisi/create', 'PosisiController@insert')->name('posisi.insert');
Route::get('/posisi/{id}/edit', 'PosisiController@edit')->name('posisi.edit');
Route::patch('/posisi/{id}/update', 'PosisiController@update')->name('posisi.update');
Route::delete('posisi/{id}/delete', 'PosisiController@destroy')->name('posisi.destroy');
Route::get('/posisi/{id}/detail', 'PosisiController@detail')->name('posisi.detail');
Route::delete('posisiDeleteAll', 'PosisiController@deleteAll');

Route::get('/family/create', 'FamilyController@create')->name('family.family');
Route::post('/family/create', 'FamilyController@insert')->name('family.insert');
Route::get('/family/{id}/edit', 'FamilyController@edit')->name('family.edit');
Route::patch('/family/{id}/update', 'familyController@update')->name('family.update');
Route::get('/family/{id}/detail', 'familyController@detail')->name('family.detail');
Route::delete('family/{id}/delete', 'familyController@destroy')->name('family.destroy');
Route::delete('familyDeleteAll', 'familyController@deleteAll');

Route::get('/reward/create', 'rewardController@create')->name('reward.reward');
Route::post('/reward/create', 'rewardController@insert')->name('reward.insert');
Route::get('/reward/{id}/edit', 'rewardController@edit')->name('reward.edit');
Route::patch('/reward/{id}/update', 'rewardController@update')->name('reward.update');
Route::get('/reward/{id}/detail', 'rewardController@detail')->name('reward.detail');
Route::delete('reward/{id}/delete', 'rewardController@destroy')->name('reward.destroy');
Route::delete('rewardDeleteAll', 'rewardController@deleteAll');

Route::get('/berita', 'BeritaController@index')->name('berita');
Route::get('/berita/create', 'BeritaController@create')->name('berita.create');
Route::post('/berita/create', 'BeritaController@insert')->name('berita.insert');
Route::get('/berita/{id}/edit', 'BeritaController@edit')->name('berita.edit');
Route::patch('/berita/{id}/update', 'BeritaController@update')->name('berita.update');
Route::get('/berita/{id}/detail', 'BeritaController@detail')->name('berita.detail');
Route::delete('berita/{id}/delete', 'BeritaController@destroy')->name('berita.destroy');

Route::get('/setting/modul', 'SettingController@modul')->name('setting.modul');
Route::get('/setting/tambahmodul', 'SettingController@create_modul')->name('setting.create_modul');
Route::post('/setting/insert', 'SettingController@insert_modul')->name('setting.insert_modul');
Route::get('/setting/{id}/editmodul', 'SettingController@edit_modul')->name('setting.edit_modul');
Route::patch('/setting/{id}/updatemodul', 'SettingController@update_modul')->name('setting.update_modul');
Route::get('setting/{id}/deletemodul', 'SettingController@destroy_modul')->name('setting.destroy_modul');

Route::get('/setting/usergroup', 'SettingController@index')->name('setting.usergroup');
Route::get('/setting/usergroup_insert', 'SettingController@create_usergroup')->name('usergroup.create');
Route::post('/setting/usergroup_insert', 'SettingController@insert_usergroup')->name('usergroup.insert');
Route::delete('setting/{id}/usergroup_delete', 'SettingController@delete_usergroup')->name('menu.destroy');

Route::get('/setting/viewhakakses', 'SettingController@viewhakakses')->name('setting.viewhakakses');
Route::get('/setting/hakakses', 'SettingController@hakakses')->name('setting.hakakses');
Route::post('/setting/hakakses', 'SettingController@hakinsert')->name('setting.hakinsert');
Route::get('/setting/{id}/updatehakakses', 'SettingController@edit_hakakses')->name('setting.edithakakses');
Route::patch('/setting/{id}/updatehakakses', 'SettingController@update_hakakses')->name('setting.updatehakakses');

Route::get('/menu', 'MenuController@index')->name('menu');
Route::get('/menu/create_menu', 'MenuController@createmenu')->name('menu.create');
Route::post('/menu/create_menu', 'MenuController@insertmenu')->name('menu.insert');
Route::get('/menu/{id}/edit_menu', 'MenuController@editmenu')->name('menu.edit');
Route::patch('/menu/{id}/update_menu', 'MenuController@updatemenu')->name('menu.update');
Route::delete('menu/{id}/delete_menu', 'MenuController@destroymenu')->name('menu.destroy');

Route::get('/submenu', 'MenuController@submenu')->name('submenu');
Route::get('/submenu/create', 'MenuController@createsub')->name('submenu.create');
Route::post('/submenu/create', 'MenuController@insertsub')->name('submenu.insert');
Route::get('/submenu/{id}/edit', 'MenuController@editsub')->name('submenu.edit');
Route::patch('/submenu/{id}/update', 'MenuController@updatesub')->name('submenu.update');
Route::delete('submenu/{id}/delete', 'MenuController@destroy')->name('submenu.destroy');
Route::get('/submenu/{id}/detail', 'MenuController@detail')->name('submenu.detail');

Route::get('/submenu1', 'MenuController@submenu1')->name('submenu1');
Route::get('/submenu1/create', 'MenuController@createsub1')->name('submenu1.create');
Route::post('/submenu1/create', 'MenuController@insertsub1')->name('submenu1.insert');
Route::get('/submenu1/{id}/edit', 'MenuController@editsub1')->name('submenu1.edit');
Route::patch('/submenu1/{id}/update', 'MenuController@updatesub1')->name('submenu1.update');
Route::get('/submenu1/{id}/detail', 'MenuController@detailsub1')->name('submenu1.detail');
Route::delete('submenu1/{id}/delete', 'MenuController@destroy')->name('submenu1.destroy');

Route::get('/absensi/dataabsensi', 'AbsensiController@index')->name('absensi.dataabsensi');
Route::get('/absensi/absensi', 'AbsensiController@view_uid')->name('absensi.viewuid');
Route::post('/absensi/create_dataabsensi', 'AbsensiController@insert_uid')->name('absensi.insert');
Route::patch('/absensi/update', 'AbsensiController@update_uid')->name('absensi.update');
Route::get('/absensi/rekap', 'AbsensiController@rekap')->name('absensi.rekap');
Route::post('/absensi/filterrekap', 'AbsensiController@filterrekap')->name('absensi.filter_rekap');
Route::get('/absensi/inoutlog', 'AbsensiController@inoutlog')->name('absensi.inout_log');
Route::get('/absensi/lembur', 'AbsensiController@lembur')->name('absensi.lembur');
Route::get('/absensi/createlembur', 'AbsensiController@createlembur')->name('absensi.create_lembur');
Route::post('/absensi/createlembur', 'AbsensiController@insertlembur')->name('absensi.insert_lembur');
Route::get('/absensi/{id}/editlembur', 'AbsensiController@editlembur')->name('absensi.edit_lembur');
Route::patch('/absensi/{id}/updatelembur', 'AbsensiController@updatelembur')->name('absensi.update_lembur');

Route::get('/departemen', 'DepartmentController@departemen')->name('departemen');
Route::get('/departemen/create', 'DepartmentController@createdepartemen')->name('departemen.create');
Route::post('/departemen/create', 'DepartmentController@insertdepartemen')->name('departemen.insert');
Route::delete('/departemen/{id}/delete', 'DepartmentController@destroydepartemen')->name('departemen.delete');

Route::get('/jamkerja/jenisjamkerja', 'AbsensiController@jenisjamkerja')->name('jamkerja');
Route::get('/jamkerja/tambahjenisjamkerja', 'AbsensiController@create_jamkerja')->name('jamkerja.create');
Route::post('/jamkerja/insertjenisjamkerja', 'AbsensiController@insert_jamkerja')->name('jamkerja.insert');
Route::delete('jamkerja/{id}/delete', 'AbsensiController@destroy')->name('jamkerja.destroy');

Route::get('/izin/izin', 'IzinController@Izin')->name('izin');
Route::get('/izin/createizin', 'IzinController@createIzin')->name('createizin');
Route::get('/izin/jenisizin', 'IzinController@jenisIzin')->name('jenisizin');
Route::post('/izin/insertizin', 'IzinController@insertIzin')->name('insertizin');
Route::get('/izin/{id}/editizin', 'IzinController@editizin')->name('editizin');
Route::patch('izin/{id}/updateizin', 'IzinController@updateizin')->name('updateizin');
Route::get('/izin/{id}/detailizin', 'IzinController@detailizin')->name('detailizin');
Route::delete('izin/{id}/deleteizin', 'IzinController@destroyIzin')->name('destroyizin');
Route::get('/izin/createjenisizin', 'IzinController@createjenisIzin')->name('createjenisizin');
Route::post('/izin/insertjenisizin', 'IzinController@insertjenisIzin')->name('insertjenisizin');
Route::get('/izin/{id}/editjenisizin', 'IzinController@editjenisizin')->name('editjenisizin');
Route::patch('izin/{id}/updatejenisizin', 'IzinController@updatejenisizin')->name('updatejenisizin');
Route::delete('izin/{id}/deletejenisi', 'IzinController@destroyjenisIzin')->name('destroyjenisizin');

Route::get('/agama', 'PegawaiController@agama')->name('agama');
Route::post('/agama/filteragama', 'PegawaiController@filteragama')->name('filteragama');
Route::get('/agama/createagama', 'PegawaiController@createagama')->name('createagama');
Route::post('/agama/insertagama', 'PegawaiController@insertagama')->name('insertagama');
Route::get('/agama/{id}/editagama', 'PegawaiController@editagama')->name('editagama');
Route::patch('/agama/{id}/updateagama', 'PegawaiController@updateagama')->name('updateagama');
Route::delete('agama/{id}/deleteagama', 'PegawaiController@destroyagama')->name('destroyagama');

Route::get('/diklat', 'PegawaiController@diklat')->name('diklat');
Route::get('/diklat/creatediklat', 'PegawaiController@creatediklat')->name('creatediklat');
Route::post('/diklat/insertdiklat', 'PegawaiController@insertdiklat')->name('insertdiklat');
Route::get('/diklat/{id}/editdiklat', 'PegawaiController@editdiklat')->name('editdiklat');
Route::patch('/diklat/{id}/updatediklat', 'PegawaiController@updatediklat')->name('updatediklat');
Route::post('/diklat/filterdiklat', 'PegawaiController@filterdiklat')->name('filterdiklat');
Route::delete('diklat/{id}/deletediklat', 'PegawaiController@destroydiklat')->name('destroydiklat');

Route::get('/jpendidikan', 'PegawaiController@jpendidikan')->name('jpendidikan');
Route::get('/jpendidikan/createjpendidikan', 'PegawaiController@createjpendidikan')->name('createjpendidikan');
Route::post('/jpendidikan/insertjpendidikan', 'PegawaiController@insertjpendidikan')->name('insertjpendidikan');
Route::get('/jpendidikan/{id}/editjpendidikan', 'PegawaiController@editjpendidikan')->name('editjpendidikan');
Route::patch('/jpendidikan/{id}/updatejpendidikan', 'PegawaiController@updatejpendidikan')->name('updatejpendidikan');
Route::delete('jpendidikan/{id}/deletejpendidikan', 'PegawaiController@destroyjpendidikan')->name('destroyjpendidikan');

Route::get('/jenisjabatan', 'PegawaiController@jenisjabatan')->name('jenisjabatan');
Route::get('/jenisjabatan/createjenisjabatan', 'PegawaiController@createjenisjabatan')->name('createjenisjabatan');
Route::post('/jenisjabatan/insertjenisjabatan', 'PegawaiController@insertjenisjabatan')->name('insertjenisjabatan');
Route::get('/jenisjabatan/{id}/editjenisjabatan', 'PegawaiController@editjenisjabatan')->name('editjenisjabatan');
Route::patch('/jenisjabatan/{id}/updatejenisjabatan', 'PegawaiController@updatejenisjabatan')->name('updatejenisjabatan');
Route::delete('jenisjabatan/{id}/deletejenisjabatan', 'PegawaiController@destroyjenisjabatan')->name('destroyjenisjabatan');

Route::get('/keluarga', 'PegawaiController@keluarga')->name('keluarga');
Route::post('/keluarga/insertkeluarga', 'PegawaiController@insertkeluarga')->name('insertkeluarga');
Route::patch('/keluarga/{id}/updatekeluarga', 'PegawaiController@updatekeluarga')->name('updatekeluarga');
Route::delete('keluarga/{id}/deletekeluarga', 'PegawaiController@destroykeluarga')->name('destroykeluarga');

Route::get('/kenaikanpangkat', 'PegawaiController@kenaikanpangkat')->name('kenaikanpangkat');
Route::post('/kenaikanpangkat/insertkenaikanpangkat', 'PegawaiController@insertkenaikanpangkat')->name('insertkenaikanpangkat');
Route::patch('/kenaikanpangkat/{id}/updatekenaikanpangkat', 'PegawaiController@updatekenaikanpangkat')->name('updatekenaikanpangkat');
Route::delete('kenaikanpangkat/{id}/deletekenaikanpangkat', 'PegawaiController@destroykenaikanpangkat')->name('destroykenaikanpangkat');

Route::get('/resign', 'PegawaiController@resign')->name('resign');
Route::post('/resign/insertresign', 'PegawaiController@insertresign')->name('insertresign');
Route::patch('/resign/{id}/updateresign', 'PegawaiController@updateresign')->name('updateresign');
Route::delete('resign/{id}/deleteresign', 'PegawaiController@destroyresign')->name('destroyresign');

Route::get('/pensiun', 'PegawaiController@pensiun')->name('pensiun');
Route::post('/pensiun/insertpensiun', 'PegawaiController@insertpensiun')->name('insertpensiun');
Route::patch('/pensiun/{id}/updatepensiun', 'PegawaiController@updatepensiun')->name('updatepensiun');
Route::delete('pensiun/{id}/deletepensiun', 'PegawaiController@destroypensiun')->name('destroypensiun');

Route::get('/kallibur', 'PegawaiController@kallibur')->name('kallibur');
Route::get('/kallibur/createkallibur', 'PegawaiController@createkallibur')->name('createkallibur');
Route::post('/kallibur/insertkallibur', 'PegawaiController@insertkallibur')->name('insertkallibur');
Route::patch('/kallibur/{id}/updatekallibur', 'PegawaiController@updatekallibur')->name('updatekallibur');
Route::delete('kallibur/{id}/deletekallibur', 'PegawaiController@destroykallibur')->name('destroykallibur');

Route::get('/user/profile', 'UserController@index')->name('user.profile');
Route::get('/user', 'UserController@user')->name('user');
Route::get('/user/createuser', 'UserController@createuser')->name('create.user');
Route::post('/user/insertuser', 'UserController@insertuser')->name('insert.user');
