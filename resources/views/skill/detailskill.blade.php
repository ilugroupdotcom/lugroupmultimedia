@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Separated form outside panel -->
@foreach ($skills as $skill)
					<form class="form-horizontal form-separate" action="{{ route('pegawai.detail') }}" role="form">
						<h6><i class="icon-page-break"></i> Detail Skill</h6>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Skill:</label>
				            <div class="col-md-10">
				            	<input type="text" value="{{ $skill->skill }}" class="form-control" disabled>
				            </div>
                </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Deskripsi Skill:</label>
				            <div class="col-md-10">
				            	<input type="text" value="{{ $skill->deskripsi }}" class="form-control" disabled>
				            </div>
				        </div>

                <div class="form-actions text-right">
                  <input type="submit" value="Kembali" class="btn btn-danger">
              </div>

					</form>
@endforeach
					<!-- /separated form outside panel -->

@endsection
