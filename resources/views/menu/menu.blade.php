@extends('master.master')

@section('body')

	<!-- Breadcrumbs line -->
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li class="active">Berita</li>
		</ul>

		<div class="visible-xs breadcrumb-toggle">
			<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
		</div>
	</div>
	<!-- /breadcrumbs line -->

  <!-- Pre-scrollable table -->
			            <div class="panel panel-default">
			                <div class="panel-heading">
												<div class="pull-right">
													<a href="{{ route('menu.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;"><i class="icon-file-plus"></i>Tambah Data</a>
												</div>

												<h6 class="panel-title"><i class="icon-newspaper"></i> Data Menu</h6>
											</div>
			            	<div class="table-responsive">
				                <table class="table table-striped">
				                    <thead>
				                        <tr>
				                            <th>#</th>
				                            <th>Menu</th>
				                            <th>Menu Icon</th>
				                            <th>
                                      <center>
                                        Aksi
                                      </center>
                                    </th>
				                        </tr>
				                    </thead>
				                    <tbody>
														@if($mmenu->count())
															@php
																$no = $mmenu->firstItem() - 1;
															@endphp
															@foreach ($mmenu as $men)
                                @php
                                  $no++;
                                @endphp
																<tr>
				                            <td>{{$no}}</td>
				                            <td>{{$men->m_menu}}</td>
				                            <td>{{$men->m_icon}}</td>
																		<td class="text-center">
																			<form class="" action="{{ route('menu.destroy', $men->m_id) }}" method="post">
																					{{ csrf_field() }}
																					{{ method_field('Delete') }}
																					<button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
																			</form>
																		</td>
				                        </tr>
															@endforeach
														@endif
				                    </tbody>

				                </table>
				            </div>
										<div class="well clearfix">
													<ul class="pagination pagination-right pull-right">
															<?php echo str_replace('/?', '?', $mmenu->render()); ?>
													</ul>
											</div>
				        </div>
								<!-- /pre-scrollable table -->

@endsection
