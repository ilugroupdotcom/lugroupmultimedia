@extends('master.master')

@section('body')

	<!-- Breadcrumbs line -->
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li class="active">Berita</li>
		</ul>

		<div class="visible-xs breadcrumb-toggle">
			<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
		</div>
	</div>
	<!-- /breadcrumbs line -->

  <!-- Pre-scrollable table -->
			            <div class="panel panel-default">
			                <div class="panel-heading">
												<div class="pull-right">
													<a href="{{ route('submenu.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;"><i class="icon-file-plus"></i>Tambah Data</a>
												</div>

												<h6 class="panel-title"><i class="icon-newspaper"></i> Data Submenu</h6>
											</div>
			            	<div class="table-responsive">
				                <table class="table table-striped">
				                    <thead>
				                        <tr>
				                            <th>#</th>
				                            <th>Menu</th>
				                            <th>Submenu</th>
				                            <th>
                                      <center>
                                        Aksi
                                      </center>
                                    </th>
				                        </tr>
				                    </thead>
				                    <tbody>
														@if($submenu->count())
															@php
																$no = $submenu->firstItem() - 1;
															@endphp
															@foreach ($submenu as $sub)
                                @php
                                  $no++;
                                @endphp
																<tr>
				                            <td>{{$no}}</td>
				                            <td>{{$sub->m_menu}}</td>
				                            <td>{{$sub->sub_label}}</td>
																		<td class="text-center">
																			<form class="" action="{{ route('submenu.destroy', $sub->sub_id) }}" method="post">
																					{{ csrf_field() }}
																					{{ method_field('Delete') }}
																					<button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
																					<a href="{{ route('submenu.detail', $sub->sub_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye7"></i></a>
																					<a href="{{ route('submenu.edit', $sub->sub_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
																			</form>
																		</td>
				                        </tr>
															@endforeach
														@endif
				                    </tbody>

				                </table>
				            </div>
										<div class="well clearfix">
													<ul class="pagination pagination-right pull-right">
															<?php echo str_replace('/?', '?', $submenu->render()); ?>
													</ul>
											</div>
				        </div>
								<!-- /pre-scrollable table -->

@endsection
