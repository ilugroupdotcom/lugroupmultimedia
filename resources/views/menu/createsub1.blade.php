@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Sub Menu</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form action="{{ route('submenu1.insert') }}" role="form" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Submenu </h6></div>
          <div class="panel-body">

            <div class="form-group">
              <input type="hidden" name="id" value="{{ Auth::user()->id }}">
              <div class="row">
                <div class="col-md-6" style="padding:10px;">
  									<label>SubMenu Parent:</label>
                      <select name="submenu" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                        <option value=""></option>
                        @foreach($submenu as $sub)
                          <option value="{{ $sub->sub_id }}" {{ $selectedMenu == $sub->sub_id ? 'selected="selected"' : '' }}>{{$sub->m_menu.' - '.$sub->sub_label }}</option>
                        @endforeach
                      </select>
  								</div>

                <div class="col-md-6" style="padding:10px;">
                  <label>Label Submenu1:</label>
                      <input type="text" name="label1" placeholder="Label Submenu" class="form-control">
                </div>

                <div class="col-md-6" style="padding:10px;">
                  <label>Link Submenu1:</label>
                      <input type="text" name="link1" placeholder="Link" class="form-control">
                </div>


              </div>
            </div>

          <div class="form-actions text-right">
            <a href="{{ route('submenu') }}" class="btn btn-danger">Cancel</a>
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
@endsection
