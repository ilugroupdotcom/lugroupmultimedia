

@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <form action="{{route('departemen.insert')}}" role="form" class="form-horizontal form-separate" method="post">
    {{ csrf_field() }}
  <div class="panel panel-default">
  <div class="panel-heading">
    <h6 class="panel-title">Tambah Data Departemen</h6>
    <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
  </div>
  <div class="panel-body">
    <div class="form-group">
      <div class="row">
          <div class="col-md-12">
              <div class="panel-heading" style="background:#2179cc; margin:10px;">
                  <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Departemen</h6>
              </div>

              <div class="col-md-6">
                <div class="form-group" style="margin:10px;">
    				       <label class="col-md-2 control-label">Nama Department:</label>
    				          <div class="col-md-10">
                        <input type="text" name="nama" class="form-control">
    				           </div>
    				      </div>
              </div>
              <div class="col-md-6">
                <div class="form-group" style="margin:10px;">
                   <label class="col-md-2 control-label">Kode Department:</label>
                      <div class="col-md-10">
                        <input type="text" name="kode" class="form-control">
                       </div>
                  </div>
              </div>
            </div>
    </div>
   </div>
  </div>

  <div class="panel-body">
    <div class="form-group">
    <div class="form-actions text-center">
        <a href="{{route('absensi.lembur')}}" type="submit" class="btn btn-success btn-filter-reset">Kembali</a>
        <input type="reset" value="Reset" class="btn btn-danger btn-filter-reset">
        <input type="submit" value="SImpan" class="btn btn-info btn-filter">
    </div>
  </div>
  </div>
</div>
</form>

@endsection
