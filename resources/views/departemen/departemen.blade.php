

@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>
  <script type="text/javascript">
      $(function() {
        $("#q").autocomplete({
          source: "{{ URL('pegawai/autocompletekontrak') }}",
          minlength: 2,
          select: function(event, ui) {
            $("q").html(ui.item.value);
            onchange= this.form.submit()
          }
        });
      });
  </script>
  <!-- /breadcrumbs line -->
  <!-- Alert -->
  <!-- <div class="alert alert-warning fade in block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
  </div> -->
  <!-- /alert -->
  <!-- Table with checkboxes -->
  <form action="{{route('absensi.filter_rekap')}}" role="form" class="panel-filter" method="post">
    {{ csrf_field() }}
  <div class="panel panel-default">
  <div class="panel-heading">
    <h6 class="panel-title">Filter Data</h6>
    <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
  </div>
  <div class="panel-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
              <label>Nama Departemen:</label>
                <input type="text" name="departemen" class="form-control" value="">
            </div>

        </div>
    </div>

    <div class="form-actions text-center">
        <input type="reset" value="Reset" class="btn btn-danger btn-filter-reset">
        <input type="submit" value="Cari" class="btn btn-info btn-filter">
    </div>
  </div>
  </div>
</form>
<div class="panel panel-default">
<div class="panel-body">
<div class="block-inner text-danger">
    <h6 class="heading-hr">DATA DEPARTEMEN
      <div class="pull-right">
            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#multipledelete" type="button" href="">
                <i class="icon-file-remove"></i>Hapus Data
            </button>&nbsp;
            <a href="{{route('departemen.create')}}">
              <button class="btn btn-xs btn-success" type="button">
                  <i class="icon-file-plus"></i>Tambah Data
              </button>
            </a>
            </div>
        <small class="display-block">PT Ilugroup Multimedia Indonesia -</small>
    </h6>
</div>
<div class="table-responsive pre-scrollable stn-table">
    <form id="checkboxForm" method="post" name="" action="">
        <table width="100%" class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="50">No</th>
                    <th class="text-center">Nama Departemen</th>
                    <th class="text-center">Kode Departemen</th>
                    <th class="text-center" width="100">Aksi</th>
                </tr>
            </thead>
            <tbody>
              @if($department->count())
              @php
                $no = $department->firstItem() - 1;
              @endphp
              @foreach ($department as $key => $dep)
                @php
                  $no++;
                @endphp
                <tr id="row-1" class="removeRow15669"><td class="text-center">{{$no}}</td>
                    <td class="text-center" style="width:100px;"> {{$dep->dept_nama}}</td>
                    <td class="text-center" style="width:100px;">{{$dep->dept_kode}}</td>
                    <td class="text-center" >
                      <form class="" action="{{ route('departemen.delete', $dep->dept_id) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('Delete') }}
                          <button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                          <a data-toggle="modal" role="button" href="#{{$dep->dept_id}}" class="btn btn-default btn-xs btn-icon tip"><i class="icon-file"></i></a>
                          <a href="{{route('absensi.create_lembur')}}"><button type="button" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-pencil"></i></button></a>
                      </form>

                    </td>
                  </tr>

                  <!-- Form modal -->
                  <div id="{{$dep->dept_id}}" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title"><i class="icon-paragraph-justify2"></i> Detail Departemen</h4>
                        </div>

                        <!-- Form inside modal -->
                        <form action="#" role="form">
                          <div class="modal-body with-padding">
                            <div class="form-group">
                              <div class="row">
                              <div class="col-sm-6">
                                <label>Nama Departemen</label>
                                <input type="text" placeholder="Eugene" class="form-control" value="{{$dep->dept_nama}}">
                              </div>

                              <div class="col-sm-6">
                                <label class="control-label">Kode Department</label>
                                <input type="text" placeholder="Eugene" class="form-control" value="{{$dep->dept_kode}}">
                              </div>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- /form modal -->
                @endforeach
              @endif
              </tbody>
        </table>
    </form>
</div>
</div>

<script>
$(document).ready(function () {
$("#pagination-limit").bind("change", function () {
    window.location.href = $("#pagination-limit option:selected").data("url");
})
$(".c-pagination").each(function () {
    var aEle = $(this).find("a");
    if (aEle.length == 0) {
        var html = $(this).html();
        console.log()
        $(this).html("").append("<a></a>").find("a").html(html);
    }
})
})
</script></div>


                 <!-- /table with checkboxes -->

                  <script type="text/javascript">
                      $(document).ready(function () {


                          $('#master').on('click', function(e) {
                           if($(this).is(':checked',true))
                           {
                              $(".sub_chk").prop('checked', true);
                           } else {
                              $(".sub_chk").prop('checked',false);
                           }
                          });


                          $('.delete_all').on('click', function(e) {


                              var allVals = [];
                              $(".sub_chk:checked").each(function() {
                                  allVals.push($(this).attr('data-id'));
                              });


                              if(allVals.length <=0)
                              {
                                  alert("Please select row.");
                              }  else {


                                  var check = confirm("Are you sure you want to delete this row?");
                                  if(check == true){


                                      var join_selected_values = allVals.join(",");


                                      $.ajax({
                                          url: $(this).data('url'),
                                          type: 'DELETE',
                                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                          data: 'ids='+join_selected_values,
                                          success: function (data) {
                                              if (data['success']) {
                                                  $(".sub_chk:checked").each(function() {
                                                      $(this).parents("tr").remove();
                                                  });
                                                  alert(data['success']);
                                              } else if (data['error']) {
                                                  alert(data['error']);
                                              } else {
                                                  alert('Whoops Something went wrong!!');
                                              }
                                          },
                                          error: function (data) {
                                              alert(data.responseText);
                                          }
                                      });


                                    $.each(allVals, function( index, value ) {
                                        $('table tr').filter("[data-row-id='" + value + "']").remove();
                                    });
                                  }
                              }
                          });


                          $('[data-toggle=confirmation]').confirmation({
                              rootSelector: '[data-toggle=confirmation]',
                              onConfirm: function (event, element) {
                                  element.trigger('confirm');
                              }
                          });


                          $(document).on('confirm', function (e) {
                              var ele = e.target;
                              e.preventDefault();


                              $.ajax({
                                  url: ele.href,
                                  type: 'DELETE',
                                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                  success: function (data) {
                                      if (data['success']) {
                                          $("#" + data['tr']).slideUp("slow");
                                          alert(data['success']);
                                      } else if (data['error']) {
                                          alert(data['error']);
                                      } else {
                                          alert('Whoops Something went wrong!!');
                                      }
                                  },
                                  error: function (data) {
                                      alert(data.responseText);
                                  }
                              });


                              return false;
                          });
                      });
                  </script>
@endsection
