@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
@foreach ($family as $fame)
    <form action="{{ route('family.update', $fame->fam_id) }}" role="form" method="post">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Data Keluarga</h6></div>
          <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
                <label>Nama Keluarga:</label>
                    <input type="text" name="keluarga" value="{{ $fame->fam_nama }}" class="form-control">
                    <input type="hidden" name="id" value="{{ $fame->fam_user_id }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tempat Lahir:</label>
                    <input type="text" name="tempat" value="{{ $fame->fam_tempat_lahir }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tanggal Lahir:</label>
                    <input type="date" name="tanggal" value="{{ $fame->fam_tgl_lahir }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Jenis Kelamin:</label>
                    <input type="text" name="gender" value="{{ $fame->fam_gender }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Pendidikan Terakhir:</label>
                    <input type="text" name="pendidikan" value="{{ $fame->fam_pendidikan }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Pekerjaan:</label>
                    <input type="text" name="pekerjaan" value="{{ $fame->fam_pekerjaan }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Agama:</label>
                    <input type="text" name="agama" value="{{ $fame->fam_agama }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Status Pernikahan:</label>
                    <input type="text" name="nikah" value="{{ $fame->fam_status_nikah }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Status Hidup:</label>
                    <input type="text" name="hidup" value="{{ $fame->fam_status_hidup }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Status Anak:</label>
                    <input type="text" name="anak" value="{{ $fame->fam_status_anak }}" class="form-control">
              </div>

            </div>
          </div>



          <div class="form-actions text-center">
            <input type="reset" value="Cancel" class="btn btn-danger">
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
@endforeach
    <!-- /simple contact form -->
@endsection
