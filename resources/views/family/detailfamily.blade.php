@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Separated form outside panel -->
@foreach ($family as $fame)
          <div class="panel-body">
            <form class="form-horizontal form-separate" action="{{ route('pegawai.detail', $fame->fam_user_id) }}" role="form">
  						<h6><i class="icon-page-break"></i> Detail Pengalaman</h6>

                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Nama Keluarga:</label>
                        <div class="col-md-8">
                          <input type="text" value="{{ $fame->fam_nama }}" class="form-control" disabled>
                        </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Tempat Lahir:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $fame->fam_tempat_lahir }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Tanggal Lahir:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $fame->fam_tgl_lahir }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Jenis Kelamin:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $fame->fam_gender }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Pendidikan Terakhir:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $fame->fam_pendidikan }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Pekerjaan:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $fame->fam_pekerjaan }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Agama:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $fame->fam_agama }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Status Pernikahan:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $fame->fam_status_nikah }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Status Hidup:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $fame->fam_status_hidup }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Status Anak:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $fame->fam_status_anak }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-actions text-center">
                      <input type="submit" value="Kembali" class="btn btn-danger">
                  </div>
                  </div>

  					</form>
          </div>
@endforeach
					<!-- /separated form outside panel -->

@endsection
