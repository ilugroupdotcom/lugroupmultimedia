@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Menu</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form action="{{route('usergroup.insert')}}" role="form" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian UserGroup </h6></div>
          <div class="panel-body">

            <div class="form-group">
              <div class="row">
                <div class="col-md-6" style="padding:10px;">
                  <label>Nama UserGroup:</label>
                      <input type="text" name="ug" placeholder="Nama UserGroup" class="form-control">
                </div>

                <div class="col-md-6" style="padding:10px;">
                  <label>Label UserGroup:</label>
                      <input type="text" name="uglabel" placeholder="Label UserGroup" class="form-control">
                </div>

              </div>
            </div>

          <div class="form-actions text-center">
            <a href="{{ route('setting.usergroup') }}" class="btn btn-danger">Cancel</a>
            <input type="submit" value="Insert" class="btn btn-success">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
@endsection
