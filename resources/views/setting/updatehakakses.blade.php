@extends('master.master')

@section('body')

  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Hak Akses</li>
    </ul>
  </div>
  <!-- /breadcrumbs line -->

@foreach ($hakakses as $hak)
  <!-- Table inside panel body -->
                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
  			                <div class="panel-heading">
                          <h4 class="panel-title"> Data Hak Akses
                            <small class="display-block">PT Ilugroup Multimeida Indonesia -</small>
                          </h4>

                        </div>
                        <form class="form-horizontal" action="{{ route('setting.updatehakakses', $hak->hak_id) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('PATCH') }}
  			                <div class="panel-body">
                          <table class="table table-bordered">
                            <tbody>
                              <tr>
                                <td colspan="3" style="width:200px">
                                    <div class="form-group">
                                        <label for="UserGroupName" class="col-sm-3 col-md-4 control-label label-required">User Group</label>
                                        <div class="col-sm-9 col-md-8 required">
                                          <select name="select" data-placeholder="Choose an option..." class="form-control" tabindex="2" disabled>
                                                  <option value=""></option>
                                                  @foreach($group as $g)
                                                    <option <?php if( $g->ug_id == $hak->hak_ug_id ){echo "selected"; } ?> value="{{ $g->ug_id }}" >{{ $g->ug_usergroup }}</option>
                                                  @endforeach
                                          </select>
                                          <input type="hidden" name="ugid" value="{{$hak->hak_ug_id}}">
                            						</div>
                                    </div>
                                </td>
                              </tr>
                            </tbody>
                          </table><br>
  			                	<div class="table-responsive">
  					                <table class="table table-bordered">
  					                    <thead>
  					                        <tr>
  					                            <th>#</th>
  					                            <th>Nama Modul</th>
  					                            <th>Akses</th>
                                        <th>Add</th>
  					                            <th>Edit</th>
                                        <th>Hapus</th>
  					                        </tr>
  					                    </thead>
  					                    <tbody>
                                  <input type="hidden" name="hak" value="{{$hak->hak_id}}" class="styled" >
                                  @php
                                  $modul = DB::table('hak_akses')->Join('user_group','hak_akses.hak_ug_id','=','user_group.ug_id')
                                            ->Join('modul','hak_akses.hak_mod_id','=','modul.mod_id')
                                            ->where('user_group.ug_id',$hak->hak_ug_id)
                                            ->get();
                                  @endphp
                                  @foreach ($modul as $key => $mod)
                                    <tr>
  					                            <td>{{++$key}}</td>
  					                            <td>
                                          {{$mod->mod_label}}
                                          <input type="hidden" name="modul[]" value="{{$mod->mod_id}}" class="styled" >
                                        </td>
  					                            <td class="text-center">

                                          <div class="checkbox">
                                            <label class="checkbox-inline checkbox-success toggledisabled styled">
                                              <input type="checkbox" class="id-check" @php
                                                if ($mod->hak_akses == "akses") {
                                                  echo "checked";
                                                }
                                                if ($mod->hak_akses != "akses") {
                                                  echo "unchecked";
                                                }
                                              @endphp >
                                              <input type="hidden" class="myinput" name="akses[]" value="{{$mod->hak_akses}}">
                                              <script type="text/javascript">
                                              $('.id-check').change(function() {
                                                if ($(this).is(':checked') == false) {
                                                  $(this).closest('div').find('.myinput').val('kosong').prop('disabled', false);
                                                } else {
                                                  $(this).closest('div').find('.myinput').val('akses').prop('disabled', false);
                                                }
                                                });
                                              </script>
                                            </label>
                                          </div>
                                        </td>
                                        <td class="text-center">
                                          <div class="checkbox">
                                            <label class="checkbox-inline checkbox-success toggledisabled">
                                              <input type="checkbox" class="add" @php
                                              if ($mod->hak_add == "add") {
                                                echo "checked";
                                              }
                                              @endphp >
                                              <input type="hidden" class="myinput" name="add[]" value="{{$mod->hak_add}}">
                                              <script type="text/javascript">
                                              $('.add').change(function() {
                                                if ($(this).is(':checked') == false) {
                                                  $(this).closest('div').find('.myinput').val('kosong').prop('disabled', false);
                                                } else {
                                                  $(this).closest('div').find('.myinput').val('add').prop('disabled', false);
                                                }
                                                });
                                              </script>

                                            </label>
                                          </div>
                                        </td>
  					                            <td class="text-center">
                                          <div class="checkbox">
                                            <label class="checkbox-inline checkbox-success toggledisabled">
                                              <input type="checkbox" class="edit" @php
                                                if ($mod->hak_edit == "edit") {
                                                  echo "checked";
                                                }
                                              @endphp>
                                              <input type="hidden" class="myinput" name="edit[]" value="{{$mod->hak_edit}}">
                                              <script type="text/javascript">
                                              $('.edit').change(function() {
                                                if ($(this).is(':checked') == false) {
                                                  $(this).closest('div').find('.myinput').val('kosong').prop('disabled', false);
                                                } else {
                                                  $(this).closest('div').find('.myinput').val('edit').prop('disabled', false);
                                                }
                                                });
                                              </script>
                                            </label>
                                          </div>
                                        </td>
                                        <td class="text-center">
                                          <div class="checkbox">
                                            <label class="checkbox-inline checkbox-success toggledisabled">
                                              <input type="checkbox" class="delete" @php
                                                if ($mod->hak_delete == "delete") {
                                                  echo "checked";
                                                }
                                              @endphp>
                                              <input type="hidden" class="myinput" name="delete[]" value="{{$mod->hak_delete}}">
                                              <script type="text/javascript">
                                              $('.delete').change(function() {
                                                if ($(this).is(':checked') == false) {
                                                  $(this).closest('div').find('.myinput').val('kosong').prop('disabled', false);
                                                } else {
                                                  $(this).closest('div').find('.myinput').val('delete').prop('disabled', false);
                                                }
                                                });
                                              </script>
                                            </label>
                                          </div>
                                        </td>
  					                        </tr>
                                  @endforeach

  					                    </tbody>
  					                </table>
  				                </div>
  			                </div>
                        <div class="panel-body">
                          <div class="block-inner text-danger">
                              <div class="form-actions text-center">
                                  <input name="Button" type="button" onclick="history.go(-1);" class="btn btn-success" value="Kembali">
                                  <input type="reset" value="Reset" class="btn btn-info">
                                  <button class="btn btn-danger" type="submit">
                                    Simpan
                                  </button>
                              </div>
                          </div>
                      </div>
                      </form>
  				        </div>
                  </center>
                </div>

				        <!-- /table inside panel body -->
@endforeach
@endsection
