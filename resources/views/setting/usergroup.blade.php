@extends('master.master')

@section('body')

  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>
  <!-- /breadcrumbs line -->


  <!-- Table inside panel body -->
			            <div class="panel panel-default">
			                <div class="panel-heading">
                        <div class="pull-right">
													<a href="{{ route('usergroup.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;"><i class="icon-file-plus"></i>Tambah Data</a>
												</div>
                        <h4 class="panel-title"> Data User Group
                          <small class="display-block">PT Ilugroup Multimeida Indonesia -</small>
                        </h4>

                      </div>
			                <div class="panel-body">
			                	<div class="table-responsive">
					                <table class="table table-bordered">
					                    <thead>
					                        <tr>
					                            <th>#</th>
					                            <th>User Group</th>
					                            <th>Label</th>
					                            <th class="text-center">Aksi</th>
					                        </tr>
					                    </thead>
					                    <tbody>
                                @foreach ($group as $key => $g)
                                  <tr>
					                            <td>{{++$key}}</td>
					                            <td>{{$g->ug_usergroup}}</td>
					                            <td>{{$g->ug_label}}</td>
					                            <td class="text-center">
                                        <form class="" action="{{ route('menu.destroy', $g->ug_id) }}" method="post">
  																					{{ csrf_field() }}
  																					{{ method_field('Delete') }}
  																					<button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                        </form>
                                      </td>
					                        </tr>
                                @endforeach

					                    </tbody>
					                </table>
				                </div>
			                </div>
				        </div>
				        <!-- /table inside panel body -->
@endsection
