@extends('master.master')

@section('body')

  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Modul</li>
    </ul>
  </div>
  <!-- /breadcrumbs line -->


  <!-- Table inside panel body -->
                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
  			                <div class="panel-heading">
                          <h4 class="panel-title"> Data Modul
                            <small class="display-block">PT Ilugroup Multimeida Indonesia -</small>
                          </h4>

                        </div>
                        <form class="form-horizontal" action="{{ route('setting.hakinsert') }}" method="post">
                          {{ csrf_field() }}
  			                <div class="panel-body">
                          <table class="table table-bordered">
                            <tbody>
                              <tr>
                                <td colspan="3" style="width:200px">
                                    <div class="form-group">
                                        <label for="UserGroupName" class="col-sm-3 col-md-4 control-label label-required">User Group</label>
                                        <div class="col-sm-9 col-md-8 required">
                                          <select name="ugid" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                                                  <option value=""></option>
                                                  @foreach($group as $g)
                                                    <option value="{{ $g->ug_id }}" {{ $selectedGroup == $g->ug_id ? 'selected="selected"' : '' }}>{{ $g->ug_usergroup }}</option>
                                                  @endforeach
                                          </select>
                            						</div>
                                    </div>
                                </td>
                              </tr>
                            </tbody>
                          </table><br>
  			                	<div class="table-responsive">
  					                <table class="table table-bordered">
  					                    <thead>
  					                        <tr>
  					                            <th>#</th>
  					                            <th>Nama Modul</th>
  					                            <th>Akses</th>
                                        <th>Add</th>
  					                            <th>Edit</th>
                                        <th>Hapus</th>
  					                        </tr>
  					                    </thead>
  					                    <tbody>

                                  @foreach ($modul as $key => $mod)
                                    <tr>
  					                            <td>{{++$key}}</td>
  					                            <td>
                                          {{$mod->mod_label}}
                                          <input type="hidden" name="modul[]" value="{{$mod->mod_id}}" class="styled" >
                                        </td>
  					                            <td class="text-center">

                                          <div class="checkbox">
                                            <label class="checkbox-inline checkbox-success toggledisabled styled">
                                              <input type="checkbox" class="id-check">
                                              <input type="hidden" class="myinput" name="akses[]" value="kosong">
                                              <script type="text/javascript">
                                              $('.id-check').change(function() {
                                                if ($(this).is(':checked') == false) {
                                                  $(this).closest('div').find('.myinput').val('kosong').prop('disabled', true);
                                                } else {
                                                  $(this).closest('div').find('.myinput').val('akses').prop('disabled', false);
                                                }
                                                });
                                              </script>
                                            </label>
                                          </div>
                                        </td>
                                        <td class="text-center">
                                          <div class="checkbox">
                                            <label class="checkbox-inline checkbox-success toggledisabled">
                                              <input type="checkbox" class="add">
                                              <input type="hidden" class="myinput" name="add[]" value="kosong">
                                              <script type="text/javascript">
                                              $('.add').change(function() {
                                                if ($(this).is(':checked') == false) {
                                                  $(this).closest('div').find('.myinput').val('kosong').prop('disabled', true);
                                                } else {
                                                  $(this).closest('div').find('.myinput').val('add').prop('disabled', false);
                                                }
                                                });
                                              </script>

                                            </label>
                                          </div>
                                        </td>
  					                            <td class="text-center">
                                          <div class="checkbox">
                                            <label class="checkbox-inline checkbox-success toggledisabled">
                                              <input type="checkbox" class="edit">
                                              <input type="hidden" class="myinput" name="edit[]" value="kosong">
                                              <script type="text/javascript">
                                              $('.edit').change(function() {
                                                if ($(this).is(':checked') == false) {
                                                  $(this).closest('div').find('.myinput').val('kosong').prop('disabled', true);
                                                } else {
                                                  $(this).closest('div').find('.myinput').val('edit').prop('disabled', false);
                                                }
                                                });
                                              </script>
                                            </label>
                                          </div>
                                        </td>
                                        <td class="text-center">
                                          <div class="checkbox">
                                            <label class="checkbox-inline checkbox-success toggledisabled">
                                              <input type="checkbox" class="delete">
                                              <input type="hidden" class="myinput" name="delete[]" value="kosong">
                                              <script type="text/javascript">
                                              $('.delete').change(function() {
                                                if ($(this).is(':checked') == false) {
                                                  $(this).closest('div').find('.myinput').val('kosong').prop('disabled', true);
                                                } else {
                                                  $(this).closest('div').find('.myinput').val('delete').prop('disabled', false);
                                                }
                                                });
                                              </script>
                                            </label>
                                          </div>
                                        </td>
  					                        </tr>
                                  @endforeach

  					                    </tbody>
  					                </table>
  				                </div>
  			                </div>
                        <div class="panel-body">
                          <div class="block-inner text-danger">
                              <div class="form-actions text-center">
                                  <input name="Button" type="button" onclick="history.go(-1);" class="btn btn-success" value="Kembali">
                                  <input type="reset" value="Reset" class="btn btn-info">
                                  <button class="btn btn-danger" type="submit">
                                    Simpan
                                  </button>
                              </div>
                          </div>
                      </div>
                      </form>
  				        </div>
                  </center>
                </div>

				        <!-- /table inside panel body -->
@endsection
