@extends('master.master')

@section('body')

  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Hak Akses</li>
    </ul>
  </div>
  <!-- /breadcrumbs line -->


  <!-- Table inside panel body -->
			            <div class="panel panel-default">
			                <div class="panel-heading">
                        <h4 class="panel-title"> Data Hak Akses
                          <small class="display-block">PT Ilugroup Multimeida Indonesia -</small>
                        </h4>

                      </div>
			                <div class="panel-body">
			                	<div class="table-responsive">
					                <table class="table table-bordered">
					                    <thead>
					                        <tr>
					                            <th>#</th>
					                            <th>User Group</th>
					                            <th>Menu</th>
                                      <th>Aksi</th>
					                        </tr>
					                    </thead>
					                    <tbody>
                                @foreach ($hakakses as $key => $hak)
                                  <tr>
					                            <td>{{++$key}}</td>
                                      @php
                                        $ug = $hak->hak_ug_id;
                                        $ug = DB::table('user_group')
                                              ->where('ug_id',$ug)
                                              ->get();
                                      @endphp
                                      @foreach ($ug as $u)
                                        <td>{{$u->ug_label}}</td>
                                      @endforeach
                                      <td>
                                        @php
                                          $ug = $hak->hak_ug_id;
                                          $mod = DB::table('hak_akses')
                                                ->join('modul','hak_akses.hak_mod_id','=','modul.mod_id')
                                                ->where('hak_akses.hak_ug_id',$ug)
                                                ->get();
                                        @endphp
                                        @if ($mod->count()!=0)
                                          <ul>
                                            @foreach ($mod as $mo)
                                              @if ($mo->hak_akses == 'akses' || $mo->hak_add == 'add' || $mo->hak_edit == 'edit' || $mo->hak_delete == 'delete' )
                                                <li>
                                                  {{$mo->mod_label}}
                                                  @if ($mo->hak_akses == 'akses')
                                                    [{{$mo->hak_akses}}]
                                                  @endif
                                                  @if ($mo->hak_add == 'add')
                                                    [{{$mo->hak_add}}]
                                                  @endif
                                                  @if ($mo->hak_edit == 'edit')
                                                    [{{$mo->hak_edit}}]
                                                  @endif
                                                  @if ($mo->hak_delete == 'delete')
                                                    [{{$mo->hak_delete}}]
                                                  @endif

                                                </li>
                                              @endif
                                            @endforeach
                                          </ul>
                                        @endif
                                      </td>
                                      <td>
                                        <center>
                                          <a href="{{ route('setting.edithakakses', $mo->hak_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
                                        </center>

                                      </td>
					                        </tr>
                                @endforeach

					                    </tbody>
					                </table>
				                </div>
			                </div>
				        </div>
				        <!-- /table inside panel body -->
@endsection
