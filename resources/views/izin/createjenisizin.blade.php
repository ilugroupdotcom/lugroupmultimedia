

@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <!-- /breadcrumbs line -->
  <!-- Alert -->
  <!-- <div class="alert alert-warning fade in block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
  </div> -->
  <!-- /alert -->

  <form action="{{route('insertjenisizin')}}" class="form-horizontal form-separate" id="formSubmit" method="post" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="POST"></div><div class="row">
      {{ csrf_field() }}
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="block-inner text-danger">
                    <h6 class="heading-hr">Tambah Jenis Izin</h6>
                </div>
                <div class="table-responsive">
                    <div class="panel-heading" style="background:#2179cc">
                            <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Tambah Data Jenis Izin</h6>
                        </div><table width="100%" class="table">

                        <tbody><tr>
                            <td>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <label for="PermitTypeName" class="col-sm-3 col-md-4 control-label">Nama</label>
                                              <div class="col-sm-9 col-md-8">
                                                <input name="jenis" class="form-control" maxlength="255" type="text">
                                              </div>
                                            </div>
                                        <div class="col-md-6 ">
                                            <label for="PermitTypeUniqName" class="col-sm-3 col-md-4 control-label">Kode</label>
                                              <div class="col-sm-9 col-md-8">
                                                <input name="kode" class="form-control" maxlength="255" type="text">
                                              </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <label class="col-md-4 control-label">Kategori</label>
                                                 <div class="col-md-8">
                                                    <select data-placeholder="Pilih Kategori..." class="select-full" tabindex="2" name="kategori">
                                                        <option value=""></option>
                                                        <option value="Izin">Izin</option>
                                                        <option value="Cuti">Cuti</option>
                                                        <option value="Dinas">Dinas</option>
                                                        <option value="Libur">Libur</option>
                                                    </select>
                                                 </div>
                                             </div>
                                          </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-actions text-center">
                                    <a href="{{route('jenisizin')}}" class="btn btn-success">Kembali</a>
                                    <input type="reset" value="Reset" class="btn btn-info">
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#add" type="submit">
                                        Simpan
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
