@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form action="{{ route('pegawai.insert') }}" role="form" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Data </h6></div>
          <div class="panel-body">

          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
                <label>Nama Pegawai:</label>
                    <input type="text" name="name" placeholder="Pegawai" class="form-control">
                </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Email:</label>
                    <input type="email" name="email" placeholder="your@email.com" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>NIK:</label>
                    <input type="text" name="nik" placeholder="NIK" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                <label>DEPARTMENT:</label>
                    <input type="text" name="department" placeholder="department" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                <label>JABATAN:</label>
                    <input type="text" name="jabatan" placeholder="jabatan" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                <label>TANGGAL MULAI KERJA:</label>
                    <input type="date" name="tanggal" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                  <label>JENIS KELAMIN:</label>
                    <select name="gender" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                      <option value="Laki-Laki">Laki-Laki</option>
                      <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
              <div class="col-md-6" style="padding:10px;">
                  <label>STATUS PEGAWAI:</label>
                    <select name="status" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                      <option value="Tetap">Tetap</option>
                      <option value="Kontrak">Kontrak</option>
                    </select>
                </div>

              <div class="col-md-6" style="padding:10px;">
                <label>photo:</label>
                    <input type="file" name="gambar" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Password:</label>
                    <input type="password" name="password" placeholder="password" class="form-control">
              </div>

            </div>
          </div>



          <div class="form-actions text-right">
            <input type="reset" value="Cancel" class="btn btn-danger">
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
@endsection
