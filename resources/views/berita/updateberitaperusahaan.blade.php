@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
@foreach ($berita as $ber)
    <form action="{{ route('berita.update', $ber->be_id) }}" role="form" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Data </h6></div>
          <div class="panel-body">

          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
                <label>Title Berita:</label>
                    <input type="text" name="title" value="{{$ber->be_title}}" class="form-control">
                    <input type="hidden" name="id" value="{{ Auth::user()->id }}">
										<input type="hidden" name="create" value="{{ $ber->be_time_create }}">
              </div>

            </div>
          </div>

          <div class="form-group">
            <h6><i class="icon-pencil"></i> Content Berita</h6>
            <div class="block-inner">
              <textarea name="draft" class="editor">{{$ber->be_draft}}</textarea>
            </div>
          </div>

          <div class="form-actions text-right">
            <input type="reset" value="Cancel" class="btn btn-danger">
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
@endforeach
@endsection
