@extends('master.master')

@section('body')

  <!-- Breadcrumbs line -->
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li class="active">Berita</li>
		</ul>

		<div class="visible-xs breadcrumb-toggle">
			<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
		</div>
	</div>
	<!-- /breadcrumbs line -->

  <!-- Static control -->
  @foreach ($berita as $ber)
			<form class="form-horizontal" action="#" role="form">
	            <div class="panel panel-default">
	                <div class="panel-heading"><h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Detail Berita</h6></div>
	                <div class="panel-body">

				        <div class="form-group">
				            <label class="col-sm-2 control-label text-right">Title:</label>
				            <div class="col-sm-10">
				            	<p class="form-control-static">{{$ber->be_title}}</p>
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-sm-2 control-label text-right">Berita:</label>
				            <div class="col-sm-6">
				            	<p class="form-control-static">{!!($ber->be_draft)!!}</p>
				            </div>
				        </div>

                <div class="form-group">
				            <label class="col-sm-2 control-label text-right">Waktu Posting:</label>
				            <div class="col-sm-10">
				            	<p class="form-control-static">{{$ber->be_time_create}}</p>
				            </div>
				        </div>

                @if ($ber->be_time_create != $ber->be_time_update)
                  <div class="form-group">
                      <label class="col-sm-2 control-label text-right">Waktu Update Post:</label>
                      <div class="col-sm-10">
                        <p class="form-control-static">{{$ber->be_time_update}}</p>
                      </div>
                  </div>
                @endif

                <div class="form-actions text-right">
                  <input type="submit" value="Submit form" class="btn btn-primary">
                </div>

				    </div>
				</div>
			</form>
@endforeach
			<!-- /static control -->
@endsection
