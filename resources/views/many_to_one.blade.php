@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <center>
         <h1>{{ $title }}</h1>
         <p>{{ $content }}</p>
         <table>
             <thead>
                 <tr>
                     <th>Kendaraan</th>
                     <th>Buatan</th>
                     <th>Pemilik</th>
                 </tr>
             </thead>
             <tbody>
                 @foreach ( $pengalaman as $p )
                     <tr>
                         <td>{{ $p->nama_perusahaan }}</td>
                         <td>{{ $p->jabatan }}</td>
                         <td>{{ $p->user->name }}</td>
                     </tr>
                 @endforeach
             </tbody>
         </table>
     </center>
@endsection
