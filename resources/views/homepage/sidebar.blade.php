<div class="sidebar">
  <div class="sidebar-content">
  <!-- User dropdown -->
  <div class="user-menu dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ asset('image/'.Auth::user()->photo) }}" alt="">
    <div class="user-info">{{ Auth::user()->name }}<span>{{ Auth::user()->hak_akses }}</span></div>
    </a>
    <div class="popup dropdown-menu dropdown-menu-right">
      <div class="thumbnail">
        <div class="thumb"><img alt="" src="{{ URL::asset('images/demo/users/ilu.jpg') }}">
          <div class="thumb-options"><span><a href="#" class="btn btn-icon btn-success"><i class="icon-pencil"></i></a><a href="#" class="btn btn-icon btn-success"><i class="icon-remove"></i></a></span></div>
        </div>
        <div class="caption text-center">
          <h6>{{ Auth::user()->name }} <small>{{ Auth::user()->hak_akses }}</small></h6>
        </div>
      </div>
      <ul class="list-group">
        <li class="list-group-item"><i class="icon-pencil3 text-muted"></i> Ticket Answer <span class="label label-success">289</span></li>
        </li>
      </ul>
    </div>
  </div>
  <!-- /user dropdown -->
    <!-- Main navigation -->
    <ul class="navigation">
      @foreach ($menu as $men)
        @php
          $id = $men->m_id;
          $submenu = DB::table('submenu')->where('sub_m_id',$id)->get();
        @endphp
        @if($submenu->count() == 0 )
            @if ($men->m_id == '17')
              <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    <span>{{$men->m_menu}}</span> <i class="icon-enter2"></i></a>
                </a>

                <form id="logout-form" action="{{ route("$men->m_link") }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </li>
              @else
                @php
                  $menu = $men->m_menu;
                  $modul = DB::table('modul')
                                ->join('menu','modul.mod_label','=','menu.m_menu')
                                ->join('hak_akses','modul.mod_id','=','hak_akses.hak_mod_id')
                                ->join('user_group','hak_akses.hak_ug_id','=','user_group.ug_id')
                                ->join('users','user_group.ug_usergroup','=','users.hak_akses')
                                ->where('menu.m_menu',$men->m_menu)
                                ->where('users.hak_akses',Auth::user()->hak_akses)
                                ->where('users.id',Auth::user()->id)
                                ->where('hak_akses.hak_akses','akses')
                                ->orderBy('menu.m_urutan', 'ASC')
                                ->get();
                @endphp
                @if ($modul->count() > 0)
                    @foreach ($modul as $mod)
                      <li><a href="{{ route("$mod->mod_alias") }}"><span>{{$men->m_menu}}</span> <i class="{{$men->m_icon}}"></i></a></li>
                    @endforeach
                @else
                    <li><a href="{{ route('home') }}"><span>{{$men->m_menu}}</span> <i class="{{$men->m_icon}}"></i></a></li>
                @endif
            @endif

        @else
            <li><a href="{{ route("$men->m_link") }}"><span>{{$men->m_menu}}</span> <i class="{{$men->m_icon}}"></i></a>
            <ul>
              @foreach ($submenu as $sub)
                @php
                  $ids = $sub->sub_id;
                  $sub1 = DB::table('submenu1')->where('sub1_sub_id',$ids)->get();
                @endphp
                @if($sub1->count() == 0 )
                  @php
                    $submenu = $sub->sub_label;
                    $modul = DB::table('modul')
                                  ->join('submenu','modul.mod_label','=','submenu.sub_label')
                                  ->join('hak_akses','modul.mod_id','=','hak_akses.hak_mod_id')
                                  ->join('user_group','hak_akses.hak_ug_id','=','user_group.ug_id')
                                  ->join('users','user_group.ug_usergroup','=','users.hak_akses')
                                  ->where('submenu.sub_label',$submenu)
                                  ->where('users.hak_akses',Auth::user()->hak_akses)
                                  ->where('users.id',Auth::user()->id)
                                  ->where('hak_akses.hak_akses','akses')
                                  ->get();
                  @endphp
                  @if ($modul->count() > 0)
                      @foreach ($modul as $mod)
                        <li class="{{ isActiveRoute($mod->mod_alias) }}"><a href="{{route("$mod->mod_alias")}}">{{$sub->sub_label}}</a></li>
                      @endforeach

                  @else
                      <li><a href="{{route('home')}}">{{$sub->sub_label}}</a></li>
                  @endif
              @else
                <li><a href="{{route("$sub->sub_link")}}">{{$sub->sub_label}}</a>
                <ul>
                  @foreach ($sub1 as $s1)
                    @php
                      $submenu1 = $s1->sub1_label;
                      $modul1 = DB::table('modul')
                                    ->join('submenu1','modul.mod_label','=','submenu1.sub1_label')
                                    ->join('hak_akses','modul.mod_id','=','hak_akses.hak_mod_id')
                                    ->join('user_group','hak_akses.hak_ug_id','=','user_group.ug_id')
                                    ->join('users','user_group.ug_usergroup','=','users.hak_akses')
                                    ->where('submenu1.sub1_label',$submenu1)
                                    ->where('users.hak_akses',Auth::user()->hak_akses)
                                    ->where('users.id',Auth::user()->id)
                                    ->where('hak_akses.hak_akses','akses')
                                    ->get();
                    @endphp
                    @if ($modul1->count() > 0)
                      @foreach ($modul1 as $mod1)
                        <li class="{{ isActiveRoute($mod1->mod_alias) }}"><a href="{{route("$mod1->mod_alias")}}">{{$s1->sub1_label}}</a>
                      @endforeach
                    @else
                      <li><a href="{{route("home")}}">{{$s1->sub1_label}}</a>
                    @endif

                  @endforeach
                </ul>
                </li>
              @endif
              @endforeach
            </ul>
            </li>
        @endif
      @endforeach

    </ul>
    <!-- /main navigation -->

  </div>
</div>
