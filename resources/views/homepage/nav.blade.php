<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="navbar-header">
    <a class="navbar-brand" href="#"><img src="{{ URL::asset('images/logoim.png') }}" alt="Londinium"></a>

    <button type="button" class="navbar-toggle offcanvas">
      <span class="sr-only">Toggle navigation</span>
      <i class="icon-paragraph-justify2"></i>
    </button>
  </div>
  <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">

    <li class="dropdown">
      <a class="sidebar-toggle" data-toggle="dropdown">
        <i class="icon-paragraph-justify2"></i>
      </a>
    </li>

  </ul>
</div>
