@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>
  <script type="text/javascript">
      $(function() {
        $("#q").autocomplete({
          source: "{{ URL('pegawai/autocompletekontrak') }}",
          minlength: 2,
          select: function(event, ui) {
            $("q").html(ui.item.value);
            onchange= this.form.submit()
          }
        });
      });
  </script>

    <form action="#" role="form" class="panel-filter">
      <div class="panel panel-default">
          <div class="panel-heading">
              <h6 class="panel-title">Filter Data</h6>
              <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
          </div>
          <div class="panel-body">
              <div class="form-group">
                  <div class="row">
                      <div class="col-md-6">
                          <label>Nama Akun</label>
                          <input name="User.username" class="form-control tip" type="text" value="" id="" data-original-title="" title="">
                       </div>
                      <div class="col-md-6">
                          <label>Email</label>
                          <input name="User.email" class="form-control tip" type="text" value="" id="" data-original-title="" title="">
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="row">
                      <div class="col-md-6">
                          <label>Nama Depan</label>
                          <input name="Biodata.first_name" class="form-control tip" type="text" value="" id="" data-original-title="" title="">
                        </div>
                      <div class="col-md-6">
                          <label>Nama Belakang</label>
                          <input name="Biodata.last_name" class="form-control tip" type="text" value="" id="" data-original-title="" title="">
                        </div>
                  </div>
              </div>

                  <div class="form-actions text-center">
                      <input type="button" value="Reset" class="btn btn-danger btn-filter-reset">
                      <input type="button" value="Cari" class="btn btn-info btn-filter">
                  </div>
              </div>
          </div>
      </form>

      <div class="panel panel-default">
    <div class="panel-body">
        <div class="block-inner text-danger">
            <h6 class="heading-hr">
              DATA PENGGUNA APLIKASI
              <div class="pull-right">
                  <a href="{{url('user/createuser')}}">
                    <button class="btn btn-xs btn-success" type="button"><i class="icon-file-plus"></i>Tambah Data</button>
                  </a>
              </div>
                <small class="display-block">PT Ilugroup Multimedia Indonesia</small>
            </h6>
        </div>
        <div class="table-responsive pre-scrollable stn-table">
            <form id="checkboxForm" method="post" name="checkboxForm" action="">
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">No</th>
                            <th>Username</th>
                            <th>Nama Depan</th>
                            <th>Nama Belakang</th>
                            <th>Email</th>
                            <th>User Group</th>
                            <th>Alamat</th>
                            <th>No. Handphone</th>
                            <th>Status Pengguna</th>
                            <th width="100">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                      @php
                        $no= 0;
                      @endphp
                      @foreach ($user as $u)
                        @php
                          $no++;
                        @endphp
                        <tr id="row-1" class="removeRow253">
                            <td class="text-center">{{$no}}</td>
                              <td>{{$u->username}}</td>
                              <td>{{$u->name}} </td>
                              <td>{{$u->nama_belakang}}</td>
                              <td>{{$u->email}}</td>
                              <td>{{$u->hak_akses}}</td>
                              <td>{{$u->alamat}}</td>
                              <td>{{$u->handphone}}</td>
                              <td style="width:150px;">{{$u->status}}</td>
                              <td style="width:150px;" class="text-center">
                                <form class="" action="{{ route('pegawai.destroy', $u->id) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('Delete') }}
                                    <button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                    <a href="{{ route('pegawai.detail', $u->id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye7"></i></a>
                                    <a href="{{ route('pegawai.edit', $u->id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
                                </form>
                              </td>
                            </tr>
                    @endforeach
                          </tbody>
                </table>
            </form>
        </div>
    </div>

</div>


@endsection
