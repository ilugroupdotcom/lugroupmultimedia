<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Data Pegawai</title>
  </head>
  <body>
      <table>
         <thead>
           <tr>
             <th>No</th>
             <th>Nama</th>
             <th>Departemen</th>
             <th>Jabatan</th>
             <th>Tanggal Kerja</th>
             <th>Jenis Kelamin</th>
             <th>Status Pegawai</th>
            </tr>
         </thead>
         <tbody>
        @php
          $no = 0;
          $users = DB::table('users')->join('posisi','users.id','=','posisi.user_id')->get();
        @endphp
         @foreach($users as $key => $user)
           @php
             $no++;
           @endphp
           <tr>
             <td>{{ $no }}</td>
             <td>{{ $user->name }}</td>
             <td>{{ $user->pos_department }}</td>
             <td>{{ $user->pos_jabatan }}</td>
             <td>{{ $user->tanggal_kerja }}</td>
             <td>{{ $user->gender }}</td>
             <td>{{ $user->status }}</td>
            </tr>
         @endforeach
       </tbody>
       </table>
  </body>
</html>
