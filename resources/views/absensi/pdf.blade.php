<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Data Pegawai</title>
    <style media="screen">
      table{
        width: 100%;
        border: 1px solid;
        text-align: center;
        font-size: 8pt;
      }
      th{
        border: 1px solid;
      }
      tr{
        border: 1px solid;
      }
      td{
        border: 1px solid;
      }
      #container{
          min-height:100%;
          position:relative;
      }
      #footer{
          height:50px;
          line-height:50px;
          position:absolute;
          bottom:0px;
      }

    </style>
  </head>
  <body>
    <div id="container">
    <img src="images/logoim.png" alt="" height="40px" width="150px;">
    <p style="text-align:center; margin0px; padding:0px; font-size:8pt;"><b style="font-size:12pt;">PT. ILUGROUP MULTIMEDIA INDONESIA</b><br />
      JL. Toddopuli Raya Timur Kompleks Villa Surya Mas J/9 <br>
      Makassar - Indonesia <br>
      Phone : 62-411- 421567      Email : office@ilugroupmultimedia.co.id
    </p>
    <hr></hr>
    <p style="text-align:center; margin0px; padding:0px; font-size:8pt;">
      Data Pegawai<br>
    </p>
      <table>
         <thead>
           <tr>
             <th>No</th>
             <th>Photo</th>
             <th>Nama</th>
             <th>Departemen</th>
             <th>Jabatan</th>
             <th>Tanggal Kerja</th>
             <th>Jenis Kelamin</th>
             <th>Status Pegawai</th>
            </tr>
         </thead>
         <tbody>
        @php
          $no = 0;
        @endphp
         @foreach($users as $key => $user)
           @php
             $no++;
           @endphp
           <tr>
             <td>{{ $no }}</td>
             <td><img src="{{'image/'.$user->photo}}" alt="" style="height:30px; width:20px; padding:10px;" ></td>
             <td>{{ $user->name }}</td>
             <td>{{ $user->pos_department }}</td>
             <td>{{ $user->pos_jabatan }}</td>
             <td>{{ $user->tanggal_kerja }}</td>
             <td>{{ $user->gender }}</td>
             <td>{{ $user->status }}</td>
            </tr>
         @endforeach
       </tbody>
       </table>
     </div>
     <div id="footer">
       <h5 style="font-size:8pt;">
         @php
           echo date('l, d-m-Y H:i:s');
         @endphp
       </h5>
     </div>
  </body>
</html>
