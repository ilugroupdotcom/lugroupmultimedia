@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <!-- Table with checkboxes -->
  <form action="{{route('absensi.insert_lembur')}}" role="form" class="form-horizontal form-separate" method="post">
    {{ csrf_field() }}
  <div class="panel panel-default">
  <div class="panel-heading">
    <h6 class="panel-title">Tambah Data lembur</h6>
    <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
  </div>
  <div class="panel-body">
    <div class="form-group">
      <div class="row">
          <div class="col-md-6">
              <div class="panel-heading" style="background:#2179cc; margin:10px;">
                  <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Pegawai</h6>
              </div>
              <div class="form-group" style="margin:10px;">
				         <label class="col-md-2 control-label">Tujuan:</label>
				            <div class="col-md-10">
                      <select data-placeholder="Tujuan" class="select-full" tabindex="2" name="tujuan">
                        <option value=""></option>
                        <option value="Individu">-Individu</option>
                        <option value="Semua">-Semua</option>
                        <option value="" disabled>Departemen</option>
                        @foreach ($department as $dep)
                            <option value="{{$dep->dept_nama}}">--{{ $dep->dept_nama }}</option>
                        @endforeach
                      </select>
				            </div>
				       </div>

              <div class="form-group" style="margin:10px;">
  				       <label class="col-md-2 control-label">Pegawai:</label>
  				          <div class="col-md-10">
                      <select data-placeholder="Nama Pegawai" class="select-full" tabindex="2" name="user">
                        <option value=""></option>
                          @foreach ($pegawai as $p)
                              <option value="{{ $p->id }}">{{ $p->name }}</option>
                          @endforeach
                      </select>
  				           </div>
  				      </div>

                <div class="form-group" style="margin:10px;">
                   <label class="col-md-2 control-label">Keterangan Lembur:</label>
                   <div class="col-md-10">
                     <textarea rows="5" cols="5" placeholder="Keterangan..." class="elastic form-control" name="keterangan"></textarea>
                   </div>
               </div>
            </div>
            <div class="col-md-6">
              <div class="panel-heading" style="background:#2179cc; margin:10px;">
                  <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Lembur</h6>
              </div>
              <div class="form-group" style="margin:10px;">
				          <label class="col-md-4 control-label">Tanggal Lembur:</label>
				            <div class="col-md-8">
                      <input class="form-control" type="date" name="tgl_lembur" value="">
				            </div>
				       </div>
                <div class="form-group" style="margin:10px;">
  				        <label class="col-md-4 control-label">Waktu Mulai Lembur:</label>
  				          <div class="col-md-8">
                      <input class="form-control" type="time" name="start" >
  				          </div>
  				       </div>
                <div class="form-group" style="margin:10px;">
    				       <label class="col-md-4 control-label">Waktu Selesai Lembur:</label>
    				        <div class="col-md-8">
                      <input class="form-control" type="time" name="end" >
    				        </div>
    				   </div>
        </div>
    </div>
   </div>
  </div>

  <div class="panel-body">
    <div class="form-group">
    <div class="form-actions text-center">
        <a href="{{route('absensi.lembur')}}" type="submit" class="btn btn-success btn-filter-reset">Kembali</a>
        <input type="reset" value="Reset" class="btn btn-danger btn-filter-reset">
        <input type="submit" value="SImpan" class="btn btn-info btn-filter">
    </div>
  </div>
  </div>
</div>
</form>


@endsection
