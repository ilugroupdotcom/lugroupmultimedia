@extends('master.master')

@section('body')
<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form class="form-horizontal form-separate" action="{{ route('jamkerja.insert') }}" role="form" method="post">
      {{ csrf_field() }}
      <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="block-inner text-danger">
                    <h6 class="heading-hr">Tambah Jenis Jam Kerja<small class="display-block">PT Ilugroup Multimedia Indonesia</small>
                    </h6>
                </div>
                <div class="table-responsive">
                    <div class="panel-heading" style="background:#2179cc">
                            <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Jenis Jam Kerja</h6>
                        </div><table width="100%" class="table">

                        <tbody><tr>
                            <td colspan="3" style="width:200px">
                                <div class="form-group">
                                    <label for="WorkingHourTypeName" class="col-sm-3 col-md-4 control-label">Jenis Jam Kerja</label>
                                    <div class="col-sm-8 col-md-7">
                                      <input name="jenis" class="form-control" maxlength="255" type="text" id="">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                          <td colspan="3" style="width:200px">
                              <div class="form-group">
                                  <label for="WorkingHourTypeIsCustom" class="col-sm-3 col-md-4 control-label">Auto?</label>                                    <label class="radio-inline radio-success">
                                      <div class="choice">
                                        <span class="checked">
                                          <input id="yesCheck" type="radio" name="auto" value="Ya" class="styled" checked="" onclick="javascript:yesnoCheck();">
                                        </span>
                                      </div>
                                      <b>Ya</b> &nbsp; &nbsp; &nbsp; &nbsp;
                                      <div class="choice">
                                        <span class="">
                                          <input id="noCheck" type="radio" name="auto" value="Tidak" class="styled" onclick="javascript:yesnoCheck();">
                                        </span>
                                      </div>
                                      <b>Tidak</b>
                                  </label>
                              </div>
                          </td>
                        </tr>
                        <tr id="ifYes" class="toggleauto" style="display: table-row;">
                          <td colspan="3" style="width:200px">
                              <div  class="form-group">
                                  <label for="WorkingHourTypeIgnoreHoliday" class="col-sm-3 col-md-4 control-label">Abaikan Hari Libur?</label>                                    <label class="radio-inline radio-success">
                                      <div class="choice">
                                        <span class="checked">
                                          <input type="radio" name="libur" value="Ya" class="styled" checked="">
                                        </span>
                                      </div>
                                      <b>Ya</b> &nbsp; &nbsp; &nbsp; &nbsp;
                                      <div class="choice">
                                        <span>
                                          <input type="radio" name="libur" value="Tidak" class="styled">
                                        </span>
                                      </div>
                                      <b>Tidak</b>
                                  </label>
                              </div>
                          </td>
                        </tr>
                        <tr id="ifYes1" class="toggleauto" style="display: table-row;">
                            <td colspan="3" style="width:200px">
                                <div class="form-group">
                                    <label for="WorkingHourTypeDetailDayId" class="col-sm-3 col-md-4 control-label">Daftar Hari Kerja : </label>                                                                            <label class="checkbox-inline checkbox-success toggleable-fields " data-name="Senin">
                                        <div class="checker hover">
                                            <span class="">
                                              <input type="checkbox" class="styled" name="hari[]" value="senin" >
                                            </span>
                                        </div> <b>Senin</b> &nbsp;
                                    </label>
                                    <label class="checkbox-inline checkbox-success toggleable-fields " data-name="Selasa">
                                        <div class="checker">
                                          <span class="">
                                            <input type="checkbox" class="styled" name="hari[]" value="selasa">
                                          </span>
                                        </div> <b>Selasa</b> &nbsp;
                                    </label>
                                    <label class="checkbox-inline checkbox-success toggleable-fields " data-name="Rabu">
                                        <div class="checker">
                                          <span class="">
                                            <input type="checkbox" class="styled" name="hari[]" value="rabu">
                                          </span>
                                        </div> <b>Rabu</b> &nbsp;
                                    </label>
                                    <label class="checkbox-inline checkbox-success toggleable-fields " data-name="Kamis">
                                        <div class="checker">
                                          <span>
                                            <input type="checkbox" class="styled" name="hari[]" value="kamis">
                                          </span>
                                        </div> <b>Kamis</b> &nbsp;
                                    </label>
                                    <label class="checkbox-inline checkbox-success toggleable-fields " data-name="Jumat">
                                        <div class="checker">
                                          <span>
                                            <input type="checkbox" class="styled" name="hari[]" value="jumat">
                                          </span>
                                        </div> <b>Jumat</b> &nbsp;
                                    </label>
                                    <label class="checkbox-inline checkbox-success toggleable-fields " data-name="Sabtu">
                                        <div class="checker">
                                          <span>
                                            <input type="checkbox" class="styled" name="hari[]" value="sabtu">
                                          </span>
                                        </div> <b>Sabtu</b> &nbsp;
                                    </label>
                                    <label class="checkbox-inline checkbox-success toggleable-fields " data-name="Minggu">
                                        <div class="checker">
                                          <span>
                                            <input type="checkbox" class="styled" name="hari[]" value="ahad">
                                          </span>
                                        </div> <b>Minggu</b> &nbsp;
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </tbody></table>
                  </div>
            </div>

            <style type="text/css">
                .box{
                    color: #000;
                    display: none;
                    margin-top: 0px;
                    margin-bottom: 0px;
                }

                .boxradio{
                    color: #000;
                    padding: 20px;
                    display: none;
                    margin-top: 20px;
                    margin-bottom: 20px;
                }

            </style>

        <div id="ifYes2">
          <div class="senin box">
            <div class="panel-heading toggleauto" style="background:#2179cc" >
              <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Senin</h6>
            </div>
            <table width="100%" class="table toggle-disabled toggleauto" >
                <tbody>
                  <tr>
                    <td colspan="3" style="width:200px">
                      <div class="form-group">
                        <div class="col-sm-6" >
                        <label for="" class="col-sm-3 col-md-4 control-label">Jam Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="masuk[]" class="form-control timepicker" type="text" data-time-format="H:i" data-step="15" data-min-time="10:00" data-max-time="18:00" data-show-2400="true" >
                            <input name="day[]" class="form-control timepicker" maxlength="5" type="hidden" value="senin" >


                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndWork" class="col-sm-3 col-md-4 control-label">Jam Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="pulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartIn" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndIn" class="col-sm-3 col-md-4 control-label">Jam Terakhir Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartHome" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndHome" class="col-sm-2 col-md-4 control-label">Jam Terakhir Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirpulang[]" class="form-control timepicker" maxlength="5" type="text"  >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartOut" class="col-sm-3 col-md-4 control-label">Jam Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="break[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="lastbreak[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeIn" class="col-sm-3 col-md-4 control-label">Jam Mulai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="mulailembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirlembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>

                      </td>
                    </tr>
                  </tbody>
                </table>
          </div>

          <div class="selasa box">
            <div class="panel-heading toggleauto" style="background:#2179cc" >
              <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Selasa</h6>
            </div>
            <table width="100%" class="table toggle-disabled toggleauto" >
                <tbody>
                  <tr>
                    <td colspan="3" style="width:200px">
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartWork" class="col-sm-3 col-md-4 control-label">Jam Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="masuk[]" class="form-control timepicker" maxlength="5" type="text" >
                            <input name="day[]" class="form-control timepicker" maxlength="5" type="hidden" value="selasa" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndWork" class="col-sm-3 col-md-4 control-label">Jam Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="pulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartIn" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndIn" class="col-sm-3 col-md-4 control-label">Jam Terakhir Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartHome" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndHome" class="col-sm-2 col-md-4 control-label">Jam Terakhir Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartOut" class="col-sm-3 col-md-4 control-label">Jam Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="break[]" class="form-control timepicker" maxlength="5" type="text">
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="lastbreak[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeIn" class="col-sm-3 col-md-4 control-label">Jam Mulai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="mulailembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirlembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>

                      </td>
                    </tr>
                  </tbody>
                </table>
          </div>

          <div class="rabu box">
            <div class="panel-heading toggleauto" style="background:#2179cc" >
              <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Rabu</h6>
            </div>
            <table width="100%" class="table toggle-disabled toggleauto" >
                <tbody>
                  <tr>
                    <td colspan="3" style="width:200px">
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartWork" class="col-sm-3 col-md-4 control-label">Jam Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="masuk[]" class="form-control timepicker" maxlength="5" type="text" >
                            <input name="day[]" class="form-control timepicker" maxlength="5" type="hidden" value="rabu" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndWork" class="col-sm-3 col-md-4 control-label">Jam Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="pulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartIn" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndIn" class="col-sm-3 col-md-4 control-label">Jam Terakhir Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartHome" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndHome" class="col-sm-2 col-md-4 control-label">Jam Terakhir Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartOut" class="col-sm-3 col-md-4 control-label">Jam Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="break[]" class="form-control timepicker" maxlength="5" type="text">
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="lastbreak[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeIn" class="col-sm-3 col-md-4 control-label">Jam Mulai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="mulailembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirlembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>

                      </td>
                    </tr>
                  </tbody>
                </table>
          </div>

          <div class="kamis box">
            <div class="panel-heading toggleauto" style="background:#2179cc" >
              <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Kamis</h6>
            </div>
            <table width="100%" class="table toggle-disabled toggleauto" >
                <tbody>
                  <tr>
                    <td colspan="3" style="width:200px">
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartWork" class="col-sm-3 col-md-4 control-label">Jam Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="masuk[]" class="form-control timepicker" maxlength="5" type="text" >
                            <input name="day[]" class="form-control timepicker" maxlength="5" type="hidden" value="kamis" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndWork" class="col-sm-3 col-md-4 control-label">Jam Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="pulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartIn" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndIn" class="col-sm-3 col-md-4 control-label">Jam Terakhir Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartHome" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndHome" class="col-sm-2 col-md-4 control-label">Jam Terakhir Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartOut" class="col-sm-3 col-md-4 control-label">Jam Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="break[]" class="form-control timepicker" maxlength="5" type="text">
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="lastbreak[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeIn" class="col-sm-3 col-md-4 control-label">Jam Mulai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="mulailembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirlembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>

                      </td>
                    </tr>
                  </tbody>
                </table>
          </div>

          <div class="jumat box">
            <div class="panel-heading toggleauto" style="background:#2179cc" id="dataSeninField">
              <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Jumat</h6>
            </div>
            <table width="100%" class="table toggle-disabled toggleauto" id="dataSenin">
                <tbody>
                  <tr>
                    <td colspan="3" style="width:200px">
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartWork" class="col-sm-3 col-md-4 control-label">Jam Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="masuk[]" class="form-control timepicker" maxlength="5" type="text" >
                            <input name="day[]" class="form-control timepicker" maxlength="5" type="hidden" value="jumat" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndWork" class="col-sm-3 col-md-4 control-label">Jam Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="pulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartIn" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndIn" class="col-sm-3 col-md-4 control-label">Jam Terakhir Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartHome" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndHome" class="col-sm-2 col-md-4 control-label">Jam Terakhir Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartOut" class="col-sm-3 col-md-4 control-label">Jam Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="break[]" class="form-control timepicker" maxlength="5" type="text">
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="lastbreak[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeIn" class="col-sm-3 col-md-4 control-label">Jam Mulai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="mulailembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirlembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>

                      </td>
                    </tr>
                  </tbody>
                </table>
          </div>

          <div class="sabtu box">
            <div class="panel-heading toggleauto" style="background:#2179cc" id="dataSeninField">
              <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Sabtu</h6>
            </div>
            <table width="100%" class="table toggle-disabled toggleauto" id="dataSenin">
                <tbody>
                  <tr>
                    <td colspan="3" style="width:200px">
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartWork" class="col-sm-3 col-md-4 control-label">Jam Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="masuk[]" class="form-control timepicker" maxlength="5" type="text" >
                            <input name="day[]" class="form-control timepicker" maxlength="5" type="hidden" value="sabtu" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndWork" class="col-sm-3 col-md-4 control-label">Jam Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="pulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartIn" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndIn" class="col-sm-3 col-md-4 control-label">Jam Terakhir Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartHome" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndHome" class="col-sm-2 col-md-4 control-label">Jam Terakhir Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartOut" class="col-sm-3 col-md-4 control-label">Jam Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="break[]" class="form-control timepicker" maxlength="5" type="text">
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="lastbreak[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeIn" class="col-sm-3 col-md-4 control-label">Jam Mulai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="mulailembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirlembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>

                      </td>
                    </tr>
                  </tbody>
                </table>
          </div>

          <div class="ahad box">
            <div class="panel-heading toggleauto" style="background:#2179cc" id="dataSeninField">
              <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Minggu</h6>
            </div>
            <table width="100%" class="table toggle-disabled toggleauto" id="dataSenin">
                <tbody>
                  <tr>
                    <td colspan="3" style="width:200px">
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartWork" class="col-sm-3 col-md-4 control-label">Jam Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="masuk[]" class="form-control timepicker" maxlength="5" type="text" >
                            <input name="day[]" class="form-control timepicker" maxlength="5" type="hidden" value="minggu" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndWork" class="col-sm-3 col-md-4 control-label">Jam Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="pulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartIn" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndIn" class="col-sm-3 col-md-4 control-label">Jam Terakhir Absen Masuk Kerja</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirmasuk[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartHome" class="col-sm-3 col-md-4 control-label">Jam Awal Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="awalpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndHome" class="col-sm-2 col-md-4 control-label">Jam Terakhir Absen Pulang</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirpulang[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1StartOut" class="col-sm-3 col-md-4 control-label">Jam Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="break[]" class="form-control timepicker" maxlength="5" type="text">
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1EndOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Istirahat</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="lastbreak[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeIn" class="col-sm-3 col-md-4 control-label">Jam Mulai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="mulailembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                        <div class="col-sm-6">
                        <label for="WorkingHourTypeDetail1OvertimeOut" class="col-sm-3 col-md-4 control-label">Jam Selesai Lembur</label>
                          <div class="col-sm-8 col-md-7">
                            <input name="akhirlembur[]" class="form-control timepicker" maxlength="5" type="text" >
                          </div>
                        </div>
                      </div>

                      </td>
                    </tr>
                  </tbody>
                </table>
          </div>
        </div>



  </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="block-inner text-danger">
                    <div class="form-actions text-center">
                        <input  type="reset" value="Reset" class="btn btn-info">
                        <a class="btn btn-success" href="{{route('jamkerja')}}">Kembali</a>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#add" type="submit" >
                            Simpan
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </form>
    <!-- /simple contact form -->
    <script type="text/javascript">
    $(document).ready(function(){
      $('input[type="checkbox"]').click(function(){
          var inputValue = $(this).attr("value");
          $("." + inputValue).toggle();
      });
    });

    function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.display = 'table-row';
        document.getElementById('ifYes1').style.display = 'table-row';
        document.getElementById('ifYes2').style.display = 'block';
    } else {
        document.getElementById('ifYes').style.display = 'none';
        document.getElementById('ifYes1').style.display = 'none';
        document.getElementById('ifYes2').style.display = 'none';
    }
  }
    </script>
    <script type="text/javascript">
      $('.timepicker').datetimepicker({ datepicker:false, format:'H:i', step:'30' });
    </script>

@endsection
