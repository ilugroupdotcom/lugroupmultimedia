@extends('master.master')

@section('body')

  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Modul</li>
    </ul>
  </div>
  <!-- /breadcrumbs line -->


  <!-- Table inside panel body -->
                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
  			                <div class="panel-heading">
                          <h4 class="panel-title"> Data UID Pegawai
                            <small class="display-block">PT Ilugroup Multimeida Indonesia -</small>
                          </h4>

                        </div>
                        <form class="form-horizontal" action="{{ route('absensi.insert') }}" method="post">
                          {{ csrf_field() }}
  			                <div class="panel-body">
                        <div class="table-responsive">
  					                <table class="table table-bordered">
  					                    <thead class="text-center">
                                  <tr >
                                       <td rowspan="2"> #</td>
                                       <td rowspan="2"> Nama Pegawai</td>
                                       <td colspan="2"> Mesin</td>

                                   </tr>
                                   <tr>
                                       <td> Mesin 1</td>
                                       <td> Mesin 2</td>
                                   </tr>
  					                    </thead>
  					                    <tbody>

                                  @foreach ($users as $key => $user)
                                    <tr>
  					                            <td>{{++$key}}</td>
  					                            <td>
                                          {{$user->name}}
                                          <input type="hidden" name="user[]" value="{{$user->id}}" class="styled" >
                                        </td>
  					                            <td class="text-center">
                                          <label>
                                              <input type="number" class="form-control" name="uid1[]" placeholder="UID">
                                          </label>
                                        </td>
                                        <td class="text-center">
                                          <label>
                                              <input type="number" class="form-control" name="uid2[]" placeholder="UID">
                                          </label>
                                        </td>
                                    </tr>
                                  @endforeach

  					                    </tbody>
  					                </table>
  				                </div>
  			                </div>
                        <div class="panel-body">
                          <div class="block-inner text-danger">
                              <div class="form-actions text-center">
                                  <input name="Button" type="button" onclick="history.go(-1);" class="btn btn-success" value="Kembali">
                                  <input type="reset" value="Reset" class="btn btn-info">
                                  <button class="btn btn-danger" type="submit">
                                    Simpan
                                  </button>
                              </div>
                          </div>
                      </div>
                      </form>
  				        </div>
                  </center>
                </div>

				        <!-- /table inside panel body -->
@endsection
