

@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>
  <script type="text/javascript">
      $(function() {
        $("#q").autocomplete({
          source: "{{ URL('pegawai/autocompletekontrak') }}",
          minlength: 2,
          select: function(event, ui) {
            $("q").html(ui.item.value);
            onchange= this.form.submit()
          }
        });
      });
  </script>
  <!-- /breadcrumbs line -->
  <!-- Alert -->
  <!-- <div class="alert alert-warning fade in block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
  </div> -->
  <!-- /alert -->
  <!-- Table with checkboxes -->
  <form action="{{route('absensi.filter_rekap')}}" role="form" class="panel-filter" method="post">
    {{ csrf_field() }}
  <div class="panel panel-default">
  <div class="panel-heading">
    <h6 class="panel-title">Filter Data</h6>
    <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
  </div>
  <div class="panel-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
              <label>Nama Pegawai:</label>
                <select data-placeholder="Nama Pegawai" class="select-full" tabindex="2" name="name">
                  <option value=""></option>
                  @foreach ($pegawai as $p)
                      <option value="{{ $p->id }}">{{ $p->name }}</option>
                  @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <label>Per Bulan:</label>
                    <div class="row">
                        <div class="col-lg-8">
                            <select data-placeholder="Bulan" class="select-full" tabindex="2" name="bulan">
                                <option value=""></option>
                                <option <?php $mount = date("m"); if($mount == '1') echo "selected" ?>  value="1" >January</option>
                                <option <?php $mount = date("m"); if($mount == '2') echo "selected" ?>  value="2" >February</option>
                                <option <?php $mount = date("m"); if($mount == '3') echo "selected" ?>  value="3" >March</option>
                                <option <?php $mount = date("m"); if($mount == '4') echo "selected" ?>  value="4" >April</option>
                                <option <?php $mount = date("m"); if($mount == '5') echo "selected" ?>  value="5" >May</option>
                                <option <?php $mount = date("m"); if($mount == '6') echo "selected" ?>  value="6" >June</option>
                                <option <?php $mount = date("m"); if($mount == '7') echo "selected" ?>  value="7" >July</option>
                                <option <?php $mount = date("m"); if($mount == '8') echo "selected" ?>  value="8" >August</option>
                                <option <?php $mount = date("m"); if($mount == '9') echo "selected" ?>  value="9" >September</option>
                                <option <?php $mount = date("m"); if($mount == '10') echo "selected" ?> value="10">October</option>
                                <option <?php $mount = date("m"); if($mount == '11') echo "selected" ?> value="11">November</option>
                                <option <?php $mount = date("m"); if($mount == '12') echo "selected" ?> value="12">December</option>
                              </select>
                          </div>

                          <div class="col-lg-4">
                              <select data-placeholder="Tahun" class="select-full" tabindex="2" name="tahun">
                                <option value=""></option>
                                <option value="2018" <?php $tahun = date("Y"); if($tahun == '2018') echo "selected" ?>>2018</option>
                                <option value="2017" <?php $tahun = date("Y"); if($tahun == '2017') echo "selected" ?>>2017</option>
                                <option value="2016" <?php $tahun = date("Y"); if($tahun == '2016') echo "selected" ?>>2016</option>
                                <option value="2015" <?php $tahun = date("Y"); if($tahun == '2015') echo "selected" ?>>2015</option>
                                <option value="2014" <?php $tahun = date("Y"); if($tahun == '2014') echo "selected" ?>>2014</option>
                              </select>
                          </div>
                  </div>
              </div>
        </div>
    </div>

    <div class="form-actions text-center">
        <input type="reset" value="Reset" class="btn btn-danger btn-filter-reset">
        <input type="submit" value="Cari" class="btn btn-info btn-filter">
    </div>
  </div>
  </div>
</form>


               <div class="panel panel-default" style="height:600px;">
  			        <div class="panel-heading">
                  <div class="pull-right">
                    <a href="{{ route('pegawai.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;"><i class="icon-file-plus"></i>Tambah Data</a>
                    <button style="margin-bottom: 10px; margin-top:10px;" class="btn btn-xs btn-danger delete_all" data-url="{{ url('pegawaiDeleteAll') }}"><i class="icon-file-remove"></i>Hapus Data</button>
                  </div>

                  <h6 class="panel-title"><i class="icon-checkbox-partial"></i> Rekapitulasi Jam Kerja</h6>
                </div>
                 <div class="table-responsive " style="overflow-y:auto; overflow-x:scrol; height:500px;">
                  <table class="table table-bordered table-check text-center" CELLPADDING=4 >
  									<thead>
  										<tr class="text-center">
  											<th style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;"><input type="checkbox"  id="master"></th>
                        <th style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">#</th>
                        <th width="350" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Nama Pegawai</th>
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">KET</th>
                        @php
                        $sekarang=date("Y-m-d");
                        $kurang=1;
                        $bubu=strtotime(date("Y-m-d", strtotime($sekarang)));
                            $periode = date("Y-m-01", $bubu);
                            $tgllalu=date("Y-m-t", $bubu);
                            $tgl_skr = new DateTime($periode, new DateTimeZone("Asia/Makassar"));
                            $tgl_lalu = new DateTime($tgllalu, new DateTimeZone("Asia/Makassar"));
                          do {

                            $hari = array ( 1 =>    'Senin',
                            'Selasa',
                            'Rabu',
                            'Kamis',
                            'Jumat',
                            'Sabtu',
                            'Minggu'
                          );

                            $bulan = array (1 =>   'Januari',
                              'Februari',
                              'Maret',
                              'April',
                              'Mei',
                              'Juni',
                              'Juli',
                              'Agustus',
                              'September',
                              'Oktober',
                              'November',
                              'Desember'
                            );
                          $split = explode('-', $tgl_skr->format('y-m-d'));
                          $bulan = $bulan[(int)$split[1]];
                          $date = date('Y-m-d', strtotime($tgl_skr->format('y-m-d')));
                          $kemarin = date('Y-m-d', strtotime("-3 day", strtotime(date("Y-m-d"))));
                          $lusa = date('Y-m-d', strtotime("+3 day", strtotime(date("Y-m-d"))));
                          $num = date('N', strtotime($tgl_skr->format('y-m-d')));
                          if (($kemarin <= $date ) OR ($lusa == $date)) {

                            echo "<th class='text-center' width='100' style='background-color: rgb(255, 255, 204); position: relative; top: 0px;'>";
                            echo $hari[$num].', '.$tgl_skr->format('d').' '.$bulan.' '.$tgl_skr->format('Y');
                            echo "</th>";

                          }
                          $tgl_skr->modify("+1 day");
                          if ($date == $lusa){
                            break;
                          }
                        }

                        while ($tgl_lalu >= $tgl_skr);
                        @endphp
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Jumlah Hari Kerja</th>
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Jumlah Hari Lembur</th>
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Jumlah Absen</th>
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Ijin</th>
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Cuti</th>
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Dinas</th>
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Libur</th>
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Total Jam Kerja</th>
                        <th width="100" style="background-color: rgb(255, 255, 204); position: relative; z-index: 1; top: 0px; left: 0px;">Total Jam Lembur</th>
  										</tr>
  									</thead>
  									<tbody>
                    @if($users->count())
                      <?php $no = $users->firstItem() - 1 ; ?>
                    @foreach($users as $key => $user)
                      <?php
                      $no++ ;
                      ?>
                      <tr id="tr_{{$user->id}}">
                        <td rowspan="6"><input type="checkbox" name="checkRow" class="sub_chk" data-id="{{$user->id}}"/></td>
                        <td rowspan="6">{{ $no }}</td>
                        <td rowspan="6">{{ $user->name }}</td>
                        <td style="box-shadow: rgb(136, 136, 136) 5px 0px 5px -2px;">Masuk</td>
                        @php
                        $sekarang=date("Y-m-d");
                        $kurang=2;
                        $bubu=strtotime(date("Y-m-d", strtotime($sekarang)));
                            $periode = date("Y-m-01", $bubu);
                            $tgllalu=date("Y-m-t", $bubu);
                            $tgl_skr = new DateTime($periode, new DateTimeZone("Asia/Makassar"));
                            $tgl_lalu = new DateTime($tgllalu, new DateTimeZone("Asia/Makassar"));
                            $jumkerja = 0;
                            $jumabsen = 0;
                            $jumlembur = 0;
                            $cuti = 0;
                            $ijin = 0;
                            $dinas = 0;

                          do {

                            $hari = array ( 1 =>    'Senin',
                            'Selasa',
                            'Rabu',
                            'Kamis',
                            'Jumat',
                            'Sabtu',
                            'Minggu'
                          );

                            $bulan = array (1 =>   'Januari',
                              'Februari',
                              'Maret',
                              'April',
                              'Mei',
                              'Juni',
                              'Juli',
                              'Agustus',
                              'September',
                              'Oktober',
                              'November',
                              'Desember'
                            );
                          $split = explode('-', $sekarang);
                          $bulan = $bulan[(int)$split[1]];
                            $num = date('N', strtotime($tgl_skr->format('y-m-d')));
                            $date = date('Y-m-d', strtotime($tgl_skr->format('y-m-d')));
                            $datetime = date('Y-m-d H:i:s', strtotime($tgl_skr->format('y-m-d H:i:s')));
                            // echo "<td>";
                            // echo $hari[$num].', '.$tgl_skr->format('d').' '.$bulan.' '.$tgl_skr->format('Y');
                            // echo "</td>";
                            $absen = DB::table('data_absen')->get();
                            $uid = DB::table('data_absen')->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                ->join('users','data_absen.data_user_id','=','users.id')
                                ->where('data_absen.data_user_id', $user->id)
                                ->get();
                            $uidnum = count($uid);

                            $a = 0;

                            $kemarin = date('Y-m-d', strtotime("-3 day", strtotime(date("Y-m-d"))));
                            $lusa = date('Y-m-d', strtotime("+3 day", strtotime(date("Y-m-d"))));
                            $jamkerja = DB::table('jam_kerja')
                                        ->join('users','jam_kerja.jam_jenis_kerja','=','users.hak_akses')
                                        ->join('hari_kerja','jam_kerja.jam_id','=','hari_kerja.hari_jam_id')
                                        ->where('users.id', $user->id)
                                        ->where('hari_kerja.hari_nama', $hari[$num])
                                        ->get();
                            foreach ($uid as $ui) {

                              if (($kemarin <= $date ) OR ($lusa == $date)) {
                                $a++;
                                $tanggal = date('Y-m-d', strtotime($ui->date_time));
                                $time = date('H:i:s', strtotime($ui->date_time));
                                $izin = DB::table('izin')
                                        ->join('users','izin.izin_user_id','=','users.id')
                                        ->join('jenis_izin','izin.izin_jenis_id','=','jenis_izin.ji_id')

                                        ->where('izin.izin_date_start', $date)
                                        ->where('users.id', $user->id)
                                        ->orderBy('izin.izin_id', 'ASC')
                                        ->get();
                                if ($izin->count() > 0) {
                                  foreach ($izin as $iz) {
                                    echo "<td class='text-center' style='font-size:10pt;'><span class='label label-info' style='font-size:10pt;'>";
                                    echo $iz->ji_jenis;
                                    echo "</span></td>";
                                    if ($iz->ji_kategori == 'Cuti' ) {
                                      $cuti++;
                                    }elseif ($iz->ji_kategori == 'Izin' ) {
                                      $ijin++;
                                    }elseif ($iz->ji_kategori == 'Dinas' ) {
                                      $dinas++;
                                    }

                                  }
                                    break;
                                }else {
                                  if ($hari[$num] == 'Minggu') {
                                    echo "<td class='text-center' style='font-size:10pt;'><span class='label label-info' style='font-size:10pt;'>";
                                    echo "Libur";
                                    echo "</span></td>";
                                    break;
                                  }
                                  elseif ($tanggal == $date) {
                                    foreach ($jamkerja as $jam) {
                                      if ($time <= $jam->hari_jam_akhir_masuk) {
                                        $jumkerja++;
                                        echo "<td class='text-center' style='font-size:10pt;'><span class='label label-success' style='font-size:10pt;'>";
                                        echo $time;
                                        echo "</span></td>";
                                      }
                                      else {
                                        echo "<td>";
                                        echo "-";
                                        echo "</td>";
                                      }


                                    }
                                    break;
                                  }

                                  elseif ($tanggal > $date) {
                                    $jumabsen++;
                                    echo "<td>";
                                    echo '-';
                                    echo "</td>";

                                    break;
                                  }
                                  if ($a >= $uidnum ){
                                    $jumabsen++;
                                    echo "<td>-</td>";
                                  }

                                }

                              }

                            }
                            $uid = DB::table('data_absen')->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                ->join('users','data_absen.data_user_id','=','users.id')
                                ->where('data_absen.data_user_id', $user->id)
                                ->orderBy('data_mesinabsen.id','DESC')
                                ->get();
                            foreach ($uid as $ui) {
                              if (($kemarin <= $date ) OR ($lusa == $date)) {
                                $a++;
                                $tanggal = date('Y-m-d', strtotime($ui->date_time));
                                $time = date('H:i:s', strtotime($ui->date_time));
                                if ($tanggal == $date) {
                                  foreach ($jamkerja as $jam) {
                                    if ($time >= $jam->hari_jam_lembur) {
                                      $jumlembur++;
                                    }
                                  }
                                }
                              }
                            }

                            if ($date == $lusa){
                              break;
                            }

                            $tgl_skr->modify("+1 day");
                        }
                        while ($tgl_lalu >= $tgl_skr);
                        @endphp
                        <td rowspan="6">{{$jumkerja}}</td>
                        <td rowspan="6">{{$jumlembur}}</td>
                        <td rowspan="6">{{$jumabsen}}</td>
                        <td rowspan="6">{{$ijin}}</td>
                        <td rowspan="6">{{$cuti}}</td>
                        <td rowspan="6">{{$dinas}}</td>
                        <td rowspan="6">-</td>
                        <td rowspan="6">-</td>
                        <td rowspan="6">-</td>
                      </tr>

                      <tr>
                        <td style="box-shadow: rgb(136, 136, 136) 5px 0px 5px -2px;">Pulang</td>
                        @php
                        $sekarang=date("Y-m-d");
                        $kurang=2;
                        $bubu=strtotime(date("Y-m-d", strtotime($sekarang)));
                            $periode = date("Y-m-01", $bubu);
                            $tgllalu=date("Y-m-t", $bubu);
                            $tgl_skr = new DateTime($periode, new DateTimeZone("Asia/Makassar"));
                            $tgl_lalu = new DateTime($tgllalu, new DateTimeZone("Asia/Makassar"));
                          do {

                            $hari = array ( 1 =>    'Senin',
                            'Selasa',
                            'Rabu',
                            'Kamis',
                            'Jumat',
                            'Sabtu',
                            'Minggu'
                          );

                            $bulan = array (1 =>   'Januari',
                              'Februari',
                              'Maret',
                              'April',
                              'Mei',
                              'Juni',
                              'Juli',
                              'Agustus',
                              'September',
                              'Oktober',
                              'November',
                              'Desember'
                            );
                          $split = explode('-', $sekarang);
                          $bulan = $bulan[(int)$split[1]];
                            $num = date('N', strtotime($tgl_skr->format('y-m-d')));
                            $date = date('Y-m-d', strtotime($tgl_skr->format('y-m-d')));
                            $datetime = date('Y-m-d H:i:s', strtotime($tgl_skr->format('y-m-d H:i:s')));
                            // echo "<td>";
                            // echo $hari[$num].', '.$tgl_skr->format('d').' '.$bulan.' '.$tgl_skr->format('Y');
                            // echo "</td>";
                            $absen = DB::table('data_absen')->get();
                            $uid = DB::table('data_absen')->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                ->join('users','data_absen.data_user_id','=','users.id')
                                ->where('data_absen.data_user_id', $user->id)
                                ->get();
                            $uidnum = count($uid);
                            $a = 0;
                            $kemarin = date('Y-m-d', strtotime("-3 day", strtotime(date("Y-m-d"))));
                            $lusa = date('Y-m-d', strtotime("+3 day", strtotime(date("Y-m-d"))));
                            $jamkerja = DB::table('data_absen')
                                        ->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                        ->join('users','data_absen.data_user_id','=','users.id')
                                        ->join('jam_kerja','jam_kerja.jam_jenis_kerja','=','users.hak_akses')
                                        ->join('hari_kerja','jam_kerja.jam_id','=','hari_kerja.hari_jam_id')
                                        ->where('users.id', $user->id)
                                        ->where('data_mesinabsen.date', $date)
                                        ->where('hari_kerja.hari_nama', $hari[$num])
                                        ->orderBy('data_mesinabsen.id','DESC')
                                        ->get();
                            // $jamkerja = DB::table('jam_kerja')
                            //             ->join('users','jam_kerja.jam_jenis_kerja','=','users.hak_akses')
                            //             ->join('hari_kerja','jam_kerja.jam_id','=','hari_kerja.hari_jam_id')
                            //             ->where('users.id', $user->id)
                            //             ->where('hari_kerja.hari_nama', $hari[$num])
                            //             ->get();
                            foreach ($uid as $ui) {
                              if (($kemarin <= $date ) OR ($lusa == $date)) {
                              $a++;
                              $tanggal = date('Y-m-d', strtotime($ui->date_time));
                              $time = date('H:i:s', strtotime($ui->date_time));
                              if ($hari[$num] == 'Minggu') {
                                echo "<td>";
                                echo "-";
                                echo "</td>";
                                break;
                              }
                              elseif ($tanggal == $date) {

                                foreach ($jamkerja as $jam) {
                                    if (($jam->time >= $jam->hari_jam_pulang) && ($jam->time <= $jam->hari_jam_akhir_pulang)) {
                                      echo "<td class='text-center' style='font-size:10pt;'><span class='label label-success' style='font-size:10pt;'>";
                                      echo $jam->time;
                                      echo "</span></td>";
                                      break;
                                    }
                                    else {
                                      echo "<td>";
                                      echo "-";
                                      echo "</span></td>";
                                      break;
                                    }

                                  }
                                break;
                              }
                              elseif ($tanggal > $date) {
                                echo "<td>";
                                echo '-';
                                echo "</td>";
                                break;
                              }
                              if ($a >= $uidnum ){
                                echo "<td>-</td>";
                              }

                            }
                          }

                            if ($date == $lusa){
                              break;
                            }

                            $tgl_skr->modify("+1 day");
                        }
                        while ($tgl_lalu >= $tgl_skr);
                        @endphp
                      </tr>
                      <tr style="background-color: #D9D9D9; font-size:10pt;">
                        <td style="box-shadow: rgb(136, 136, 136) 5px 0px 5px -2px; background-color: rgb(217, 217, 217); position: relative; left: 0px;">Jam Kerja</td>
                        @php
                        $sekarang=date("Y-m-d");
                        $kurang=2;
                        $bubu=strtotime(date("Y-m-d", strtotime($sekarang)));
                            $periode = date("Y-m-01", $bubu);
                            $tgllalu=date("Y-m-t", $bubu);
                            $tgl_skr = new DateTime($periode, new DateTimeZone("Asia/Makassar"));
                            $tgl_lalu = new DateTime($tgllalu, new DateTimeZone("Asia/Makassar"));
                          do {

                            $hari = array ( 1 =>    'senin',
                            'selasa',
                            'rabu',
                            'kamis',
                            'jumat',
                            'sabtu',
                            'minggu'
                          );

                            $bulan = array (1 =>   'Januari',
                              'Februari',
                              'Maret',
                              'April',
                              'Mei',
                              'Juni',
                              'Juli',
                              'Agustus',
                              'September',
                              'Oktober',
                              'November',
                              'Desember'
                            );
                          $split = explode('-', $sekarang);
                          $bulan = $bulan[(int)$split[1]];
                            $num = date('N', strtotime($tgl_skr->format('y-m-d')));
                            $date = date('Y-m-d', strtotime($tgl_skr->format('y-m-d')));
                            $datetime = date('Y-m-d H:i:s', strtotime($tgl_skr->format('y-m-d H:i:s')));
                            // echo "<td>";
                            // echo $hari[$num].', '.$tgl_skr->format('d').' '.$bulan.' '.$tgl_skr->format('Y');
                            // echo "</td>";
                            $absen = DB::table('data_absen')->get();
                            $uid = DB::table('data_absen')->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                ->join('users','data_absen.data_user_id','=','users.id')
                                ->where('data_absen.data_user_id', $user->id)
                                ->get();
                            $uidnum = count($uid);
                            $a = 0;
                            $kemarin = date('Y-m-d', strtotime("-3 day", strtotime(date("Y-m-d"))));
                            $lusa = date('Y-m-d', strtotime("+3 day", strtotime(date("Y-m-d"))));
                            $jamkerja = DB::table('jam_kerja')
                                        ->join('users','jam_kerja.jam_jenis_kerja','=','users.hak_akses')
                                        ->join('hari_kerja','jam_kerja.jam_id','=','hari_kerja.hari_jam_id')
                                        ->where('users.id', $user->id)
                                        ->where('hari_kerja.hari_nama', $hari[$num])
                                        ->get();
                            foreach ($uid as $ui) {
                              if (($kemarin <= $date ) OR ($lusa == $date)) {
                              $a++;
                              $tanggal = date('Y-m-d', strtotime($ui->date_time));

                              $time = date('H:i', strtotime($ui->date_time));
                              if ($hari[$num] == 'minggu') {
                                echo "<td>";
                                echo "-";
                                echo "</span></td>";
                                break;
                              }
                              else {
                                foreach ($jamkerja as $jam) {
                                  echo "<td >";
                                  echo $jam->hari_jam_masuk;
                                  echo "</td>";

                                }
                                break;

                              }

                              if ($a >= $uidnum ){
                                echo "<td>-</td>";
                              }

                            }

                          }

                            if ($date == $lusa){
                              break;
                            }

                            $tgl_skr->modify("+1 day");
                        }
                        while ($tgl_lalu >= $tgl_skr);
                        @endphp
                      </tr>
                      <tr style='font-size:10pt;'>
                        <td style="box-shadow: rgb(136, 136, 136) 5px 0px 5px -2px;">Masuk Lembur</td>
                        @php
                        $sekarang=date("Y-m-d");
                        $kurang=2;
                        $bubu=strtotime(date("Y-m-d", strtotime($sekarang)));
                            $periode = date("Y-m-01", $bubu);
                            $tgllalu=date("Y-m-t", $bubu);
                            $tgl_skr = new DateTime($periode, new DateTimeZone("Asia/Makassar"));
                            $tgl_lalu = new DateTime($tgllalu, new DateTimeZone("Asia/Makassar"));
                          do {

                            $hari = array ( 1 =>    'Senin',
                            'Selasa',
                            'Rabu',
                            'Kamis',
                            'Jumat',
                            'Sabtu',
                            'Minggu'
                          );

                            $bulan = array (1 =>   'Januari',
                              'Februari',
                              'Maret',
                              'April',
                              'Mei',
                              'Juni',
                              'Juli',
                              'Agustus',
                              'September',
                              'Oktober',
                              'November',
                              'Desember'
                            );
                          $split = explode('-', $sekarang);
                          $bulan = $bulan[(int)$split[1]];
                            $num = date('N', strtotime($tgl_skr->format('y-m-d')));
                            $date = date('Y-m-d', strtotime($tgl_skr->format('y-m-d')));
                            $datetime = date('Y-m-d H:i:s', strtotime($tgl_skr->format('y-m-d H:i:s')));
                            // echo "<td>";
                            // echo $hari[$num].', '.$tgl_skr->format('d').' '.$bulan.' '.$tgl_skr->format('Y');
                            // echo "</td>";
                            $absen = DB::table('data_absen')->get();
                            $uid = DB::table('data_absen')->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                ->join('users','data_absen.data_user_id','=','users.id')
                                ->where('data_absen.data_user_id', $user->id)
                                ->get();
                            $uidnum = count($uid);
                            $a = 0;
                            $kemarin = date('Y-m-d', strtotime("-3 day", strtotime(date("Y-m-d"))));
                            $lusa = date('Y-m-d', strtotime("+3 day", strtotime(date("Y-m-d"))));
                            $jamkerja = DB::table('data_absen')
                                        ->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                        ->join('users','data_absen.data_user_id','=','users.id')
                                        ->join('jam_kerja','jam_kerja.jam_jenis_kerja','=','users.hak_akses')
                                        ->join('hari_kerja','jam_kerja.jam_id','=','hari_kerja.hari_jam_id')
                                        ->where('users.id', $user->id)
                                        ->where('data_mesinabsen.date', $date)
                                        ->where('hari_kerja.hari_nama', $hari[$num])
                                        ->orderBy('data_mesinabsen.id','DESC')
                                        ->get();
                            foreach ($uid as $ui) {
                              if (($kemarin <= $date ) OR ($lusa == $date)) {
                              $a++;
                              $tanggal = date('Y-m-d', strtotime($ui->date_time));
                              $time = date('H:i:s', strtotime($ui->date_time));
                              if ($hari[$num] == 'Minggu') {
                                echo "<td>";
                                echo "-";
                                echo "</td>";
                                break;
                              }
                              elseif ($tanggal == $date) {
                                foreach ($jamkerja as $jam) {
                                  if ($jam->time >= $jam->hari_jam_lembur) {
                                    echo "<td class='text-center' style='font-size:10pt;'><span class='label label-success' style='font-size:10pt;'>";
                                    echo $jam->hari_jam_lembur;
                                    echo "</span></td>";
                                    break;
                                  }
                                  else {
                                    echo "<td>";
                                    echo "-";
                                    echo "</span></td>";
                                    break;
                                  }

                                }
                                break;
                              }

                              elseif ($tanggal > $date) {
                                echo "<td>";
                                echo '-';
                                echo "</td>";
                                break;
                              }
                              if ($a >= $uidnum ){
                                echo "<td>-</td>";
                              }

                            }
                          }

                            if ($date == $lusa){
                              break;
                            }

                            $tgl_skr->modify("+1 day");
                        }
                        while ($tgl_lalu >= $tgl_skr);
                        @endphp
                      </tr>
                      <tr>
                        <td style="box-shadow: rgb(136, 136, 136) 5px 0px 5px -2px;">Pulang Lembur</td>
                        @php
                        $sekarang=date("Y-m-d");
                        $kurang=2;
                        $bubu=strtotime(date("Y-m-d", strtotime($sekarang)));
                            $periode = date("Y-m-01", $bubu);
                            $tgllalu=date("Y-m-t", $bubu);
                            $tgl_skr = new DateTime($periode, new DateTimeZone("Asia/Makassar"));
                            $tgl_lalu = new DateTime($tgllalu, new DateTimeZone("Asia/Makassar"));
                            $jumlembur = 0;
                          do {

                            $hari = array ( 1 =>    'Senin',
                            'Selasa',
                            'Rabu',
                            'Kamis',
                            'Jumat',
                            'Sabtu',
                            'Minggu'
                          );

                            $bulan = array (1 =>   'Januari',
                              'Februari',
                              'Maret',
                              'April',
                              'Mei',
                              'Juni',
                              'Juli',
                              'Agustus',
                              'September',
                              'Oktober',
                              'November',
                              'Desember'
                            );
                          $split = explode('-', $sekarang);
                          $bulan = $bulan[(int)$split[1]];
                            $num = date('N', strtotime($tgl_skr->format('y-m-d')));
                            $date = date('Y-m-d', strtotime($tgl_skr->format('y-m-d')));
                            $datetime = date('Y-m-d H:i:s', strtotime($tgl_skr->format('y-m-d H:i:s')));
                            // echo "<td>";
                            // echo $hari[$num].', '.$tgl_skr->format('d').' '.$bulan.' '.$tgl_skr->format('Y');
                            // echo "</td>";
                            $absen = DB::table('data_absen')->get();
                            $uid = DB::table('data_absen')->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                ->join('users','data_absen.data_user_id','=','users.id')
                                ->where('data_absen.data_user_id', $user->id)

                                ->get();
                            $uidnum = count($uid);
                            $a = 0;
                            $kemarin = date('Y-m-d', strtotime("-3 day", strtotime(date("Y-m-d"))));
                            $lusa = date('Y-m-d', strtotime("+3 day", strtotime(date("Y-m-d"))));
                            $jamkerja = DB::table('data_absen')
                                        ->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                        ->join('users','data_absen.data_user_id','=','users.id')
                                        ->join('jam_kerja','jam_kerja.jam_jenis_kerja','=','users.hak_akses')
                                        ->join('hari_kerja','jam_kerja.jam_id','=','hari_kerja.hari_jam_id')
                                        ->where('users.id', $user->id)
                                        ->where('data_mesinabsen.date', $date)
                                        ->where('hari_kerja.hari_nama', $hari[$num])
                                        ->orderBy('data_mesinabsen.id','DESC')
                                        ->get();
                            foreach ($uid as $ui) {
                              if (($kemarin <= $date ) OR ($lusa == $date)) {
                              $a++;
                              $tanggal = date('Y-m-d', strtotime($ui->date_time));
                              $time = date('H:i:s', strtotime($ui->date_time));
                              if ($hari[$num] == 'Minggu') {
                                echo "<td>";
                                echo "-";
                                echo "</td>";
                                break;
                              }
                              elseif ($tanggal == $date) {
                                foreach ($jamkerja as $jam) {
                                  if ($jam->time >= $jam->hari_jam_lembur) {
                                    $jumlembur++;
                                    echo "<td class='text-center' style='font-size:10pt;'><span class='label label-success' style='font-size:10pt;'>";
                                    echo $jam->time;
                                    echo "</span></td>";
                                    break;
                                  }
                                  else {
                                    echo "<td>";
                                    echo "-";
                                    echo "</span></td>";
                                    break;
                                  }
                                }
                                break;
                              }

                              elseif ($tanggal > $date) {
                                echo "<td>";
                                echo '-';
                                echo "</td>";
                                break;
                              }
                              if ($a >= $uidnum ){
                                echo "<td>-</td>";
                              }

                            }
                          }

                            if ($date == $lusa){
                              break;
                            }

                            $tgl_skr->modify("+1 day");
                        }
                        while ($tgl_lalu >= $tgl_skr);
                        @endphp
                      </tr>
                      <tr style="background-color: #D9D9D9">
                        <td style="box-shadow: rgb(136, 136, 136) 5px 0px 5px -2px; background-color: rgb(217, 217, 217); position: relative; left: 0px;">Jumlah Lembur</td>
                        @php
                        $sekarang=date("Y-m-d");
                        $kurang=2;
                        $bubu=strtotime(date("Y-m-d", strtotime($sekarang)));
                            $periode = date("Y-m-01", $bubu);
                            $tgllalu=date("Y-m-t", $bubu);
                            $tgl_skr = new DateTime($periode, new DateTimeZone("Asia/Makassar"));
                            $tgl_lalu = new DateTime($tgllalu, new DateTimeZone("Asia/Makassar"));
                          do {

                            $hari = array ( 1 =>    'senin',
                            'selasa',
                            'rabu',
                            'kamis',
                            'jumat',
                            'sabtu',
                            'minggu'
                          );

                            $bulan = array (1 =>   'Januari',
                              'Februari',
                              'Maret',
                              'April',
                              'Mei',
                              'Juni',
                              'Juli',
                              'Agustus',
                              'September',
                              'Oktober',
                              'November',
                              'Desember'
                            );
                          $split = explode('-', $sekarang);
                          $bulan = $bulan[(int)$split[1]];
                            $num = date('N', strtotime($tgl_skr->format('y-m-d')));
                            $date = date('Y-m-d', strtotime($tgl_skr->format('y-m-d')));
                            $datetime = date('Y-m-d H:i:s', strtotime($tgl_skr->format('y-m-d H:i:s')));
                            // echo "<td>";
                            // echo $hari[$num].', '.$tgl_skr->format('d').' '.$bulan.' '.$tgl_skr->format('Y');
                            // echo "</td>";
                            $absen = DB::table('data_absen')->get();
                            $uid = DB::table('data_absen')->join('data_mesinabsen','data_absen.data_uid_mesin','=','data_mesinabsen.pin')
                                ->join('users','data_absen.data_user_id','=','users.id')
                                ->where('data_absen.data_user_id', $user->id)
                                ->orderBy('data_mesinabsen.id','DESC')
                                ->get();
                            $uidnum = count($uid);
                            $a = 0;
                            $kemarin = date('Y-m-d', strtotime("-3 day", strtotime(date("Y-m-d"))));
                            $lusa = date('Y-m-d', strtotime("+3 day", strtotime(date("Y-m-d"))));
                            $jamkerja = DB::table('jam_kerja')
                                        ->join('users','jam_kerja.jam_jenis_kerja','=','users.hak_akses')
                                        ->join('hari_kerja','jam_kerja.jam_id','=','hari_kerja.hari_jam_id')
                                        ->where('users.id', $user->id)
                                        ->where('hari_kerja.hari_nama', $hari[$num])
                                        ->get();
                            foreach ($uid as $ui) {
                              if (($kemarin <= $date ) OR ($lusa == $date)) {
                              $a++;
                              $tanggal = date('Y-m-d', strtotime($ui->date_time));

                              $time = date('H:i:s', strtotime($ui->date_time));
                              if ($hari[$num] == 'minggu') {
                                echo "<td>";
                                echo "-";
                                echo "</td>";
                                break;
                              }
                              elseif ($tanggal == $date) {
                                foreach ($jamkerja as $jam) {
                                  if ($time >= $jam->hari_jam_lembur) {
                                    $jam_time = date('H', strtotime($ui->date_time));
                                    $menit_time = date('i', strtotime($ui->date_time));
                                    $detik_time = date('s', strtotime($ui->date_time));
                                    $jam_lembur = date('H', strtotime($jam->hari_jam_lembur));
                                    $menit_lembur = date('i', strtotime($jam->hari_jam_lembur));
                                    $detik_lembur = date('s', strtotime($jam->hari_jam_lembur));

                                    $date_awal  = new DateTime($jam_lembur.":".$menit_lembur.":".$detik_lembur);
                                    $date_akhir = new DateTime($jam_time.":".$menit_time.":".$detik_time);
                                    $selisih = $date_akhir->diff($date_awal);
                                    $jam = $selisih->format('%h');
                                    $menit = $selisih->format('%i');
                                    $detik = $selisih->format('%s');
                                    if($jam >= 0 && $jam <= 9){
                                       $jam = "0".$jam;
                                     }
                                    if($menit >= 0 && $menit <= 9){
                                       $menit = "0".$menit;
                                     }
                                    $hasil = $jam.":".$menit.":".$detik;
                                    echo "<td>";
                                    echo $hasil;
                                    echo "</td>";
                                    break;
                                  }
                                  else {
                                    echo "<td>";
                                    echo "-";
                                    echo "</span></td>";
                                    break;
                                  }
                                }
                                break;
                              }

                              if ($a >= $uidnum ){
                                echo "<td>-</td>";
                              }

                            }

                          }

                            if ($date == $lusa){
                              break;
                            }

                            $tgl_skr->modify("+1 day");
                        }
                        while ($tgl_lalu >= $tgl_skr);
                        @endphp
                      </tr>
                    @endforeach

                  @endif
                    </tbody>
  								</table>
  							</div>
                  </div>
                  <?php echo str_replace('/?', '?', $users->render()); ?>
                 <!-- /table with checkboxes -->

                  <script type="text/javascript">
                      $(document).ready(function () {


                          $('#master').on('click', function(e) {
                           if($(this).is(':checked',true))
                           {
                              $(".sub_chk").prop('checked', true);
                           } else {
                              $(".sub_chk").prop('checked',false);
                           }
                          });


                          $('.delete_all').on('click', function(e) {


                              var allVals = [];
                              $(".sub_chk:checked").each(function() {
                                  allVals.push($(this).attr('data-id'));
                              });


                              if(allVals.length <=0)
                              {
                                  alert("Please select row.");
                              }  else {


                                  var check = confirm("Are you sure you want to delete this row?");
                                  if(check == true){


                                      var join_selected_values = allVals.join(",");


                                      $.ajax({
                                          url: $(this).data('url'),
                                          type: 'DELETE',
                                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                          data: 'ids='+join_selected_values,
                                          success: function (data) {
                                              if (data['success']) {
                                                  $(".sub_chk:checked").each(function() {
                                                      $(this).parents("tr").remove();
                                                  });
                                                  alert(data['success']);
                                              } else if (data['error']) {
                                                  alert(data['error']);
                                              } else {
                                                  alert('Whoops Something went wrong!!');
                                              }
                                          },
                                          error: function (data) {
                                              alert(data.responseText);
                                          }
                                      });


                                    $.each(allVals, function( index, value ) {
                                        $('table tr').filter("[data-row-id='" + value + "']").remove();
                                    });
                                  }
                              }
                          });


                          $('[data-toggle=confirmation]').confirmation({
                              rootSelector: '[data-toggle=confirmation]',
                              onConfirm: function (event, element) {
                                  element.trigger('confirm');
                              }
                          });


                          $(document).on('confirm', function (e) {
                              var ele = e.target;
                              e.preventDefault();


                              $.ajax({
                                  url: ele.href,
                                  type: 'DELETE',
                                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                  success: function (data) {
                                      if (data['success']) {
                                          $("#" + data['tr']).slideUp("slow");
                                          alert(data['success']);
                                      } else if (data['error']) {
                                          alert(data['error']);
                                      } else {
                                          alert('Whoops Something went wrong!!');
                                      }
                                  },
                                  error: function (data) {
                                      alert(data.responseText);
                                  }
                              });


                              return false;
                          });
                      });
                  </script>

                  <?php
                  $IP  = "192.168.98.11";
                  $Key = "0";

                  $Connect = fsockopen($IP, "80", $errno, $errstr, 1);
                  if ($Connect) {
                    $soap_request = "<GetAttLog>
                      <ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey>
                      <Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg>
                    </GetAttLog>";

                    $newLine = "\r\n";
                    fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
                    fputs($Connect, "Content-Type: text/xml".$newLine);
                    fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
                    fputs($Connect, $soap_request.$newLine);
                    $buffer = "";
                    while($Response = fgets($Connect, 1024)) {
                      $buffer = $buffer.$Response;
                    }
                  }
                  $buffer = Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
                  $buffer = explode("\r\n",$buffer);

                  for ($a=0; $a<count($buffer); $a++) {
                    $data=Parse_Data($buffer[$a],"<Row>","</Row>");

                    $export[$a]['pin'] = Parse_Data($data,"<PIN>","</PIN>");
                    $export[$a]['waktu'] = Parse_Data($data,"<DateTime>","</DateTime>");
                    $export[$a]['ver'] = Parse_Data($data,"<Verified>","</Verified>");
                    $export[$a]['status'] = Parse_Data($data,"<Status>","</Status>");
                    $last = count($buffer);
                    $batas = $last - 2;
                    if ($a <= $batas) {
                      if ($a >= 1) {
                        $data = DB::table('data_mesinabsen')->where([
                                    ['pin', '=', $export[$a]['pin']],
                                    ['date_time', '=', $export[$a]['waktu']],
                                ])->get();
                        $nilai[$a] = count($data);
                        if ($nilai[$a] == null) {
                          DB::table('data_mesinabsen')
                               ->insert([
                                     'pin' => $export[$a]['pin'],
                                     'date_time' => $export[$a]['waktu'],
                                     'date' => $export[$a]['waktu'],
                                     'time' => $export[$a]['waktu'],
                                     'ver' => $export[$a]['ver'],
                                     'status' => $export[$a]['status']
                             ]);
                        }
                      }

                      }

                    }
                  // dd(!(if_exist_check($export[$a]['pin'], $export[$a]['waktu']) && $export[$a]['pin'] && $export[$a]['waktu']));
                  function Parse_Data ($data,$p1,$p2) {
                    $data = " ".$data;
                    $hasil = "";
                    $awal = strpos($data,$p1);
                    if ($awal != "") {
                      $akhir = strpos(strstr($data,$p1),$p2);
                      if ($akhir != ""){
                        $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
                      }
                    }
                    return $hasil;
                  }

                  function if_exist_check($PIN, $DateTime){
                      $data = DB::table('data_mesinabsen')->where('pin', $PIN)
                              ->where('date_time', $DateTime);
                      return $data;
                  }

                  ?>
@endsection
