@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
@foreach ($posisi as $pos)
    <form action="{{ route('posisi.update', $pos->pos_id) }}" role="form" method="post">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Posisi & Jabatan</h6></div>
          <div class="panel-body">
          <div class="form-group">
            <div class="row">
            <div class="col-md-6" style="padding:10px;">
                <label>Nama Department:</label>
                    <input type="text" name="department" value="{{ $pos->pos_department }}" class="form-control" disabled>
                    <input type="hidden" name="id" value="{{ $pos->user_id }}" class="form-control" required>
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Jabatan:</label>
                    <input type="text" name="jabatan" value="{{ $pos->pos_jabatan }}" class="form-control" disabled>
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tipe Pegawai:</label>
                    <input type="text" name="tipe" value="{{ $pos->pos_tipe }}"  class="form-control" disabled>
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tanggal Mulai Kerja:</label>
                    <input type="date" name="tanggal" value="{{ $pos->pos_mulaikerja }}" class="form-control" disabled>
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Nama Cabang:</label>
                    <input type="text" name="cabang" value="{{ $pos->pos_cabang }}" class="form-control" disabled>
              </div>

            </div>
          </div>



          <div class="form-actions text-center">
            <a href="{{ route('pegawai.detail', $pos->user_id) }}" class="btn btn-danger">Cancel</a>
            <input type="submit" value="Insert" class="btn btn-success">
          </div>

        </div>
      </div>
    </form>
  @endforeach
    <!-- /simple contact form -->
@endsection
