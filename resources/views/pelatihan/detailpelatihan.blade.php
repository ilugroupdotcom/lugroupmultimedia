@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Separated form outside panel -->
@foreach ($pelatihan as $p)
					<form class="form-horizontal form-separate" action="{{ route('pegawai.detail', $p->user_id) }}" role="form">
						<h6><i class="icon-page-break"></i> Detail Pelatihan</h6>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Title:</label>
				            <div class="col-md-10">
				            	<input type="text" value="{{ $p->title }}" class="form-control" disabled>
				            </div>
                </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Lokasi:</label>
				            <div class="col-md-10">
				            	<input type="text" value="{{ $p->lokasi }}" class="form-control" disabled>
				            </div>
				        </div>

                <div class="form-group">
				            <label class="col-md-2 control-label">Penyelenggara:</label>
				            <div class="col-md-10">
				            	<input type="text" value="{{ $p->penyelenggara }}" class="form-control" disabled>
				            </div>
				        </div>

                <div class="form-group">
				            <label class="col-md-2 control-label">Tanggal Mulai:</label>
				            <div class="col-md-10">
				            	<input type="datetime" value="{{ $p->date_mulai }}" class="form-control" disabled>
				            </div>
				        </div>

                <div class="form-group">
				            <label class="col-md-2 control-label">Tanggal Selesai:</label>
				            <div class="col-md-10">
				            	<input type="datetime" value="{{ $p->date_selesai }}" class="form-control" disabled>
				            </div>
				        </div>

                <div class="form-group">
				            <label class="col-md-2 control-label">Nomor Sertifikat:</label>
				            <div class="col-md-10">
				            	<input type="text" value="{{ $p->no_sertifikat }}" class="form-control" disabled>
				            </div>
				        </div>

                <div class="form-group">
				            <label class="col-md-2 control-label">Tanggal Sertifikat:</label>
				            <div class="col-md-10">
				            	<input type="text" value="{{ $p->tgl_sertifikat }}" class="form-control" disabled>
				            </div>
				        </div>

                <div class="form-actions text-right">
                  <input type="submit" value="Kembali" class="btn btn-danger">
              </div>

					</form>
@endforeach
					<!-- /separated form outside panel -->

@endsection
