@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
@foreach ($pelatihan as $p)
    <form action="{{ route('pelatihan.update', $p->pelatihan_id) }}" role="form" method="post">
          {{ csrf_field() }}
            {{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Pelatihan</h6></div>
          <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
                <label>Title:</label>
                    <input type="text" name="title" value="{{ $p->title }}" class="form-control">
                    <input type="hidden" name="id" value="{{ $p->user_id }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Lokasi:</label>
                    <input type="text" name="lokasi" value="{{ $p->lokasi }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Penyelenggara:</label>
                    <input type="text" name="penyelenggara" value="{{ $p->penyelenggara }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tanggal Mulai:</label>
                    <input type="datetime" name="mulai" value="{{ $p->date_mulai }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tanggal Selesai:</label>
                    <input type="datetime" name="selesai" value="{{ $p->date_selesai }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Nomor Sertifikat:</label>
                    <input type="text" name="no_set" value="{{ $p->no_sertifikat }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tanggal Sertifikat:</label>
                    <input type="date" name="tgl_set" value="{{ $p->tgl_sertifikat }}" class="form-control">
              </div>

            </div>
          </div>



          <div class="form-actions text-right">
            <input type="reset" value="Cancel" class="btn btn-danger">
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
@endforeach
    <!-- /simple contact form -->
@endsection
