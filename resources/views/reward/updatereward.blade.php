@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
@foreach ($reward as $rew)
    <form action="{{ route('reward.update', $rew->rew_id) }}" role="form" method="post">
          {{ csrf_field() }}
            {{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Data Penghargaan</h6></div>
          <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
                <label>Title Penghargaan:</label>
                    <input type="text" name="title" value="{{ $rew->rew_title }}" class="form-control">
                    <input type="hidden" name="id" value="{{ $rew->rew_user_id }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Pemberi Penghargaan:</label>
                    <input type="text" name="pemberi" value="{{ $rew->rew_nama_pemberi }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Nomor SK:</label>
                    <input type="text" name="nosk" value="{{ $rew->rew_nosk }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tanggal SK:</label>
                    <input type="text" name="task" value="{{ $rew->rew_tgl_keputusan }}" class="form-control">
              </div>

            </div>
          </div>



          <div class="form-actions text-center">
            <a href="{{ route('pegawai.detail', $rew->rew_user_id) }}" class="btn btn-danger">Cancel</a>
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
@endforeach
    <!-- /simple contact form -->
@endsection
