@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form action="{{ route('reward.insert') }}" role="form" method="post">
          {{ csrf_field() }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Data Penghargaan</h6></div>
          <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
									<label>Nama:</label>
                    <select name="id" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                      <option value=""></option>
                      @foreach($users as $user)
                        <option value="{{ $user->id }}" {{ $selectedUser == $user->id ? 'selected="selected"' : '' }}>{{ $user->name }}</option>
                      @endforeach
                    </select>
								</div>

              <div class="col-md-6" style="padding:10px;">
                <label>Title Penghargaan:</label>
                    <input type="text" name="title" placeholder="Title Penghargaan" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Nama Pemberi Penghargaan:</label>
                    <input type="text" name="pemberi" placeholder="Nama Pemberi Penghargaan" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Nomor SK:</label>
                    <input type="text" name="nosk" placeholder="Nama Pemberi Penghargaan" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tanggal SK:</label>
                    <input type="date" name="task" placeholder="Tanggal SK" class="form-control">
              </div>

            </div>
          </div>



          <div class="form-actions text-right">
            <input type="reset" value="Cancel" class="btn btn-danger">
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
@endsection
