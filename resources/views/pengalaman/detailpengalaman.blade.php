@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Separated form outside panel -->
@foreach ($pengalaman as $p)
          <div class="panel-body">
            <form class="form-horizontal form-separate" action="{{ route('pegawai.detail', $p->user_id) }}" role="form">
  						<h6><i class="icon-page-break"></i> Detail Pengalaman</h6>

                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Perusahaan:</label>
                        <div class="col-md-8">
                          <input type="text" value="{{ $p->nama_perusahaan }}" class="form-control" disabled>
                        </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Jabatan:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $p->jabatan }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Unit Kerja:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $p->unit_kerja }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Nomor SK:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $p->no_sk }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Tanggal SK:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $p->tgl_sk }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-actions text-center">
                      <input type="submit" value="Kembali" class="btn btn-danger">
                  </div>
                  </div>

  					</form>
          </div>
@endforeach
					<!-- /separated form outside panel -->

@endsection
