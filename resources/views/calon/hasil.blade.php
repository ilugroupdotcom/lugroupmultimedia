@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <!-- /breadcrumbs line -->
  <!-- Alert -->
  <!-- <div class="alert alert-warning fade in block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
  </div> -->
  <!-- /alert -->
  <!-- Table with checkboxes -->
               <div class="panel panel-default">
  			        <div class="panel-heading">
                  <div class="pull-right">
                    <a href="{{ route('calon.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;"><i class="icon-file-plus"></i>Tambah Data</a>
                    <button style="margin-bottom: 10px; margin-top:10px;" class="btn btn-xs btn-danger delete_all" data-url="{{ url('calonDeleteAll') }}"><i class="icon-file-remove"></i>Hapus Data</button>
                  </div>

                  <h6 class="panel-title"><i class="icon-checkbox-partial"></i>Data Pegawai</h6>
                </div>
                 <div class="table-responsive">
                  <table class="table table-bordered table-check">
  									<thead>
  										<tr>
  											<th><input type="checkbox"  id="master"></th>
                        <th>#</th>
                        <th>Foto</th>
                        <th>Nama Lengkap</th>
                        <th>Telepon</th>
                        <th>Gender</th>
                        <th>CV</th>
                        <th>Penilaian</th>
                        <th>Aksi</th>
  										</tr>
  									</thead>
  									<tbody>
                    @if($calon->count())
                    <?php $no = $calon->firstItem() - 1 ; ?>
                    @foreach($calon as $key => $cal)
                      <?php $no++ ;?>
                      <tr id="tr_{{$cal->cp_id}}">
  											<td><input type="checkbox" name="checkRow" class="sub_chk" data-id="{{$cal->cp_id}}"/></td>
                        <td>{{ $no }}</td>
                        <td width="70px"><img src="{{ asset('image/'.$cal->cp_foto)  }}" style="height:50px; width:50px;"></td>
                        <td>{{ $cal->cp_fullname }}</td>
                        <td>{{ $cal->cp_telp }}</td>
                        <td>{{ $cal->cp_gender }}</td>
                        <td>
                          <a href="{{ asset('image/'.$cal->cp_filecv)  }}" target="_blank">{{ $cal->cp_filecv}}</a>
                        </td>
                        @php
                            $nilai = explode(',', $cal->cp_penilaian);
                        @endphp
                        <td>
                            @php
                              $n = 0;
                              for($n;$n<count($nilai);$n++)
                              {
                                echo $nilai[$n] . "<br />";
                              }
                            @endphp
                        </td>
                        <td class="text-center">
                          <form class="" action="{{ route('calon.employe', $cal->cp_id) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="name" value="{{ $cal->cp_fullname }}">
                            <input type="hidden" name="gender" value="{{ $cal->cp_gender }}">
                            <input type="hidden" name="gambar" value="{{ $cal->cp_foto }}">
                            <button type="submit" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;" type="button" name="button">To Employe </button>
                          </form>
                        </td>
  										</tr>
                    @endforeach
                      @endif
  									</tbody>
  								</table>
                    <?php echo str_replace('/?', '?', $calon->render()); ?>
  							</div>
  				        </div>
  				        <!-- /table with checkboxes -->

                  <script type="text/javascript">
                      $(document).ready(function () {


                          $('#master').on('click', function(e) {
                           if($(this).is(':checked',true))
                           {
                              $(".sub_chk").prop('checked', true);
                           } else {
                              $(".sub_chk").prop('checked',false);
                           }
                          });


                          $('.delete_all').on('click', function(e) {


                              var allVals = [];
                              $(".sub_chk:checked").each(function() {
                                  allVals.push($(this).attr('data-id'));
                              });


                              if(allVals.length <=0)
                              {
                                  alert("Please select row.");
                              }  else {


                                  var check = confirm("Are you sure you want to delete this row?");
                                  if(check == true){


                                      var join_selected_values = allVals.join(",");


                                      $.ajax({
                                          url: $(this).data('url'),
                                          type: 'DELETE',
                                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                          data: 'ids='+join_selected_values,
                                          success: function (data) {
                                              if (data['success']) {
                                                  $(".sub_chk:checked").each(function() {
                                                      $(this).parents("tr").remove();
                                                  });
                                                  alert(data['success']);
                                              } else if (data['error']) {
                                                  alert(data['error']);
                                              } else {
                                                  alert('Whoops Something went wrong!!');
                                              }
                                          },
                                          error: function (data) {
                                              alert(data.responseText);
                                          }
                                      });


                                    $.each(allVals, function( index, value ) {
                                        $('table tr').filter("[data-row-id='" + value + "']").remove();
                                    });
                                  }
                              }
                          });


                          $('[data-toggle=confirmation]').confirmation({
                              rootSelector: '[data-toggle=confirmation]',
                              onConfirm: function (event, element) {
                                  element.trigger('confirm');
                              }
                          });


                          $(document).on('confirm', function (e) {
                              var ele = e.target;
                              e.preventDefault();


                              $.ajax({
                                  url: ele.href,
                                  type: 'DELETE',
                                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                  success: function (data) {
                                      if (data['success']) {
                                          $("#" + data['tr']).slideUp("slow");
                                          alert(data['success']);
                                      } else if (data['error']) {
                                          alert(data['error']);
                                      } else {
                                          alert('Whoops Something went wrong!!');
                                      }
                                  },
                                  error: function (data) {
                                      alert(data.responseText);
                                  }
                              });


                              return false;
                          });
                      });
                  </script>

@endsection
