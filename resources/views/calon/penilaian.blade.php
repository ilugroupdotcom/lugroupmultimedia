@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <!-- /breadcrumbs line -->
  <!-- Alert -->
  <!-- <div class="alert alert-warning fade in block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
  </div> -->
  <!-- /alert -->
  <!-- Selects -->
  @foreach($penilaian as $nil)
    <form class="form-horizontal" action="{{ route('calon.poinnilai', $nil->cp_id) }}" method="post">
      @php
        $checked = explode(',', $nil->cp_penilaian);
      @endphp
      {{ csrf_field() }}
      <div class="row">
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading"><h6 class="panel-title"><i class="icon-checkbox-checked"></i> Penilaian</h6></div>
              <div class="panel-body">

                <div class="form-group">
                <label class="col-sm-2 control-label">Poin Poin Penilaian: </label>
                <div class="col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="nilai[]" value="psikologis" class="styled" <?php in_array('psikologis', $checked) ? print "checked" : "" ?> >
                      Psikologis <br>
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="nilai[]" value="kompetensi" class="styled" <?php in_array('kompetensi', $checked) ? print "checked" : "" ?>>
                      Kompetensi <br>
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="nilai[]" value="attitude" class="styled" <?php in_array('attitude', $checked) ? print "checked" : "" ?>>
                      Attitude
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="nilai[]" value="experience" class="styled" <?php in_array('experience', $checked) ? print "checked" : "" ?>>
                      Experience
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="nilai[]" value="karakter" class="styled" <?php in_array('karakter', $checked) ? print "checked" : "" ?>>
                      Karakter
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="nilai[]" value="komunikasi" class="styled" <?php in_array('komunikasi', $checked) ? print "checked" : "" ?>>
                      Komunikasi
                    </label>
                  </div>

                </div>
              </div>
              <div class="pull-right">
                <button type="submit" style="margin-bottom: 10px" class="btn btn-xs btn-success delete_all"><i class="icon-file-plus"></i>Approve</button>
              </div>
            </div>
          </div>
        </div>
      </form>
  @endforeach
        <form class="" action="index.html" method="post">
        <div class="col-md-6">
						<div class="panel panel-default">
		                    <div class="panel-heading"><h6 class="panel-title"><i class="icon-menu5"></i> Curiculum Vitae</h6></div>
							<div class="panel-body">
              @foreach($penilaian as $nil)
                  <embed type="application/pdf" src="{{ asset('image/'. $nil->cp_filecv)  }}" width="450" height="600">
              @endforeach

							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- /selects -->


@endsection
