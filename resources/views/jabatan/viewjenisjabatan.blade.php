@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <!-- Bug report form -->
			<form action="#" role="form">
	            <div class="panel panel-default">
	                <div class="panel-heading"><h6 class="panel-title">Filter Data</h6></div>
	                <div class="panel-body">

						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label>Jabatan:</label>
	                  <input type="text" class="form-control" placeholder="Jabatan" name="jabatan">
								</div>

								<div class="col-md-6">
                  <label>Atasan</label>
									<a href="www.google.com">
                    <select data-placeholder="Pilih Atasan" class="select-full" tabindex="2" name="atasan">
                      <option value=""></option>
                      @foreach ($jabatan as $jab)
                        <option value="{{$jab->jab_nama}}">{{$jab->jab_nama}}</option>
                      @endforeach
                    </select>
									</a>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
                <div class="col-md-6">
									<label>Kode</label>
	                  <input type="text" class="form-control" placeholder="Kode" name="kode">
								</div>
								<div class="col-md-6">
									<label>Department</label>
									<a href="www.google.com">
                    <select data-placeholder="Pilih Department" class="select-full" tabindex="2" name="department">
                      <option value=""></option>
                      @foreach ($department as $dep)
                        <option value="{{$dep->dept_id}}">{{$dep->dept_nama}}</option>
                      @endforeach
                    </select>
									</a>
								</div>
							</div>
						</div>
            <div class="form-actions text-center">
              <a href="{{route('jenisjabatan')}}" class="btn btn-danger">Reset</a>
              <input type="submit" value="Cari" class="btn btn-primary">
            </div>
					</div>
				</div>
			</form>
			<!-- /bug report form -->


  <div class="panel panel-default">
    <div class="panel-body">
        <div class="block-inner text-danger">
            <h6 class="heading-hr">DATA JABATAN
              <div class="pull-right">
                    <a href="{{url('jenisjabatan/createjenisjabatan')}}"><button class="btn btn-xs btn-success" type="button"><i class="icon-file-plus"></i>Tambah Data</button></a>
              </div>
                <small class="display-block">PT Ilugroup Multimedia</small>
            </h6>
        </div>
        <div class="table-responsive pre-scrollable stn-table">
            <form id="checkboxForm" method="post" name="checkboxForm" action="">
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">No</th>
                            <th class="text-center">Jabatan</th>
                            <th class="text-center">Atasan</th>
                            <th class="text-center">Kode Grup</th>
                            <th class="text-center">Department</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                      @php
                        $no = 0;
                      @endphp
                      @foreach ($jjab as $key => $jab)
                        @php
                          $no++
                        @endphp
                        <tr id="row-1" class="removeRow1">
                            <td class="text-center">{{$no}}</td>
                            <td class="text-center">{{$jab->jab_nama}}</td>
                            <td class="text-center">
                              @if ($jab->jab_atasan == '0')
                                {{'-'}}
                              @else
                              {{$jab->jab_atasan}}
                            @endif
                            </td>
                            <td class="text-center">{{$jab->jab_kode}}</td>
                            <td class="text-center">{{$jab->dept_nama}}</td>
                            <td class="text-center">
                              <form class="" action="{{ route('destroyjenisjabatan', $jab->jab_id) }}" method="post">
                                  {{ csrf_field() }}
                                  {{ method_field('Delete') }}
                                  <button type="submit" name="submit" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                  <a href="{{url('jenisjabatan/'.$jab->jab_id.'/editjenisjabatan')}}"><button type="button" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Ubah"><i class="icon-pencil"></i></button></a>
                              </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>


@endsection
